import React, { useEffect } from "react";
import useStorage from "../useStorage";

const useToken = () => {
  const [token, setToken] = React.useState(null);
  const { getValue, saveValue } = useStorage();

  useEffect(() => {
    onGetToken();
  }, []);

  const onGetToken = React.useCallback(() => {
    async function getToken() {
      const u = await getValue("token");
      setToken(u);
    }
    getToken();
  });

  return { token };
};

export default useToken;
