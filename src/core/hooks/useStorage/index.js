import React from "react";
const useStorage = () => {
  const remember = JSON.parse(localStorage.getItem("remember"));
  const saveValue = React.useCallback((key, value, type) => {
    try {
      const execute = async () => {
        const action = type ? localStorage : sessionStorage;
        const jsonValue = JSON.stringify(value);
        await action.setItem(key, jsonValue);
      };

      execute();
    } catch (error) {
      print("Error[useStorage-set]", error);
    }
  }, []);

  const getValue = React.useCallback(
    async (key) => {
      try {
        const action = remember ? localStorage : sessionStorage;
        const jsonValue = await action.getItem(key);
        const value = await JSON.parse(jsonValue);
        return value;
      } catch (error) {
        print("Error[useStorage-get]", error);
      }
    },
    [remember]
  );

  const reset = React.useCallback(async () => {
    try {
      await localStorage.clear();
    } catch (error) {
      print("Error[useStorage-reset]", error);
    }
  });

  return {
    saveValue,
    getValue,
    reset,
  };
};

export default useStorage;
