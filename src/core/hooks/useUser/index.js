import React, { useEffect } from "react";
import useStorage from "../useStorage";

const useUser = () => {
  const [user, setUser] = React.useState(null);
  const { getValue, saveValue } = useStorage();

  useEffect(() => {
    window.addEventListener("storage", () => {
      onGetUser();
    });
  }, []);

  const onGetUser = React.useCallback(() => {
    async function getUser() {
      const u = await getValue("user");
      setUser(u);
    }
    getUser();
  });

  const saveUser = React.useCallback((value) => {
    setUser(value);
    saveValue(process.env.SECRET_USER_KEY, value);
  }, []);

  React.useEffect(onGetUser, []);

  return { user, saveUser };
};

export default useUser;
