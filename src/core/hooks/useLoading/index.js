import React, { useCallback } from "react";
import { setLoading } from "reduxFolder/loadingSlice.js";
import { useSelector, useDispatch } from "react-redux";
const useLoading = () => {
  const loading = useSelector((data) => data.loading);
  const dispatch = useDispatch();
  return {
    loading,
  };
  const setLoading = useCallback(
    (value) => {
      console.log("value", value);
      if (value instanceof Boolean) {
        dispatch(setLoading(value));
      }
    },
    [loading]
  );
  return { loading, setLoading };
};

export default useLoading;
