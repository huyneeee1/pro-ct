import { DarkTheme, LightTheme } from 'config/theme';
import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import 'rsuite/dist/rsuite.min.css';
import { ThemeProvider } from 'styled-components';
import App from './App';
import store from './redux/store';
const AppWrapper = () => {
  const [theme, setTheme] = useState('');
  const handleChangeTheme = (_theme) => {
    setTheme(_theme);
  };
  return (
    <React.StrictMode>
      <ThemeProvider theme={theme ? LightTheme : DarkTheme}>
        <Provider store={store}>
          <App changeTheme={handleChangeTheme} />
        </Provider>
      </ThemeProvider>
    </React.StrictMode>
  );
};

ReactDOM.render(<AppWrapper />, document.getElementById('root'));
