import firebase from 'firebase/app';
import 'firebase/messaging';
import 'firebase/auth';
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: 'AIzaSyDpRI-PAC1gUGf4RiCD-hZ_EdVa2WiQ50A',
  authDomain: 'fir-7aa83.firebaseapp.com',
  projectId: 'fir-7aa83',
  storageBucket: 'fir-7aa83.appspot.com',
  messagingSenderId: '585699297025',
  appId: '1:585699297025:web:548ac6a449fa7bc153bd00',
};

firebase.initializeApp(firebaseConfig);
firebase.auth().languageCode = 'it';
export const auth = firebase.auth();
const messaging = firebase.messaging();

// export const getToken = () => {
//   return messaging
//     .getToken({
//       vapidKey:
//         "BKjnAlNUjkSFLn5oI07rOA514ZpYiDp_1XN-ODKaMjres8gLAatfBQsYlyVEHNs6afgLMHr20lIJEnewCG5FXps",
//     })
//     .then((currentToken) => {
//       if (currentToken) {
//         localStorage.setItem("token_device", JSON.stringify(currentToken));
//         // Track the token -> client mapping, by sending to backend server
//         // show on the UI that permission is secured
//       } else {
//         console.log(
//           "No registration token available. Request permission to generate one."
//         );
//         // shows on the UI that permission is required
//       }
//     })
//     .catch((err) => {
//       console.log("An error occurred while retrieving token. ", err);
//       // catch error while creating client token
//     });
// };
// export const onMessageListener = () =>
//   new Promise((resolve) => {
//     messaging.onMessage((payload) => {
//       resolve(payload);
//     });
//   });
