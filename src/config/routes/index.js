import { Loading } from "atoms";
import React, {
  lazy,
  Suspense,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from "react";
import { useSelector } from "react-redux";
import { Route, Switch, useLocation } from "react-router-dom";
import { PrivateTemplate, PublicTemplate } from "templates";
import { Constant, Routers } from "utils";
//public page
const Home = lazy(() => import("pages/UnAuthPages/Home"));
const BookCar = lazy(() => import("pages/UnAuthPages/BookCar"));
const Login = lazy(() => import("pages/UnAuthPages/Login"));
const Register = lazy(() => import("pages/UnAuthPages/Register"));
const Contact = lazy(() => import("pages/UnAuthPages/Contact"));
const News = lazy(() => import("pages/UnAuthPages/News"));
const NewsDetail = lazy(() => import("pages/UnAuthPages/NewsDetail"));
const EmptyPage = lazy(() => import("pages/EmptyPage"));
const VerifyPage = lazy(() => import("pages/VerifyPage"));
const RoutePage = lazy(() => import("pages/UnAuthPages/RoutePage"));
const ForgotPassword = lazy(() => import("pages/UnAuthPages/ForgotPassword"));
const IntroducePage = lazy(() => import("pages/UnAuthPages/IntroducePage"));
const ResetPassword = lazy(() => import("pages/UnAuthPages/ResetPassword"));
const CheckBooking = lazy(() => import("pages/UnAuthPages/CheckBooking"));
const InvoiceStatusPage = lazy(() => import("pages/UnAuthPages/InvoiceStatus"));

//private
const Profile = lazy(() => import("pages/UnAuthPages/Profile"));

const Routes = () => {
  const user = useSelector((data) => data.user);
  const location = useLocation();
  const [isLoggin, setIsLoggedIn] = useState(true);
  const isPublicRouter = useMemo(() => {
    return (
      Constant.publicRouter.map((e) => e.URL).indexOf(location.pathname) > -1
    );
  }, [location.pathname]);
  const isPrivateRouter = useMemo(() => {
    return (
      Constant.privateRouter.map((e) => e.URL).indexOf(location.pathname) > -1
    );
  }, [location.pathname]);
  useEffect(() => {
    if (user) {
      return setIsLoggedIn(false);
    }
    setIsLoggedIn(true);
  }, [user]);
  const _handleBadRouter = useCallback(() => {
    if (!isPrivateRouter && !isPublicRouter) {
      return (
        <Route
          path={location.pathname}
          render={(props) => {
            return (
              <EmptyPage
                {...props}
                title="404 Not Found"
                message="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Odit nisi tenetur dolorem provident doloribus nobis esse inventore? Hic ratione nisi illum qui, aperiam perferendis architecto, facere sequi doloribus dignissimos accusamus."
                autoNavigate={Routers.HOME}
                buttonTitle="Go Home"
              />
            );
          }}
        />
      );
    }
  }, [location.pathname, user]);
  const _renderPrivateRoute = useCallback(() => {
    return <PrivateTemplate />;
  });
  const _renderPublicRoute = useCallback(() => {
    return (
      <PublicTemplate>
        <Route
          exact
          path="/"
          render={(props) => {
            return <Home {...props} />;
          }}
        />
        {isLoggin && (
          <Route
            exact
            path={Routers.REGISTER}
            render={(props) => {
              return <Register {...props} />;
            }}
          />
        )}
        {isLoggin && (
          <Route
            exact
            path={Routers.RESET}
            render={(props) => {
              return <ResetPassword {...props} />;
            }}
          />
        )}
        {isLoggin && (
          <Route
            exact
            path={Routers.FORGOT}
            render={(props) => {
              return <ForgotPassword {...props} />;
            }}
          />
        )}
        {!isLoggin && (
          <Route
            exact
            path={Routers.PROFILE}
            render={(props) => {
              return <Profile {...props} />;
            }}
          />
        )}
        {isLoggin && (
          <Route
            exact
            path={Routers.LOGIN}
            render={(props) => {
              return <Login {...props} />;
            }}
          />
        )}
        <Route
          exact
          path={Routers.NAV[0].URL}
          render={(props) => {
            return <BookCar {...props} />;
          }}
        />
        <Route
          exact
          path={Routers.NAV[1].URL}
          render={(props) => {
            return <CheckBooking {...props} />;
          }}
        />
        <Route
          exact
          path={Routers.NAV[2].URL}
          render={(props) => {
            return <RoutePage {...props} />;
          }}
        />
        <Route
          exact
          path={Routers.NAV[3].URL}
          render={(props) => {
            return <IntroducePage {...props} />;
          }}
        />
        <Route
          exact
          path={Routers.NAV[4].URL}
          render={(props) => {
            return <News {...props} />;
          }}
        />
        <Route
          exact
          path={Routers.NEWS.CHILD[0].URL}
          render={(props) => {
            return <NewsDetail {...props} />;
          }}
        />
        <Route
          exact
          path={Routers.NAV[5].URL}
          render={(props) => {
            return <Contact {...props} />;
          }}
        />
        <Route
          exact
          path={Routers.VERIFYPAGE.URL}
          render={(props) => {
            return <VerifyPage {...props} />;
          }}
        />
        <Route
          exact
          path={Routers.PAYMENT.URL}
          render={(props) => {
            return <InvoiceStatusPage {...props} />;
          }}
        />

        {_handleBadRouter()}
      </PublicTemplate>
    );
  }, [isLoggin]);

  const route = useCallback(() => {
    return _renderPublicRoute();
  }, [user, isLoggin]);

  return (
    <Suspense fallback={<Loading />}>
      <Switch>{route()}</Switch>
    </Suspense>
  );
};

Routes.propTypes = {};

export default Routes;
