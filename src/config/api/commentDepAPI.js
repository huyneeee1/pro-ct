import { axiosClient } from "./axiosClient";
const commentDepAPI = {
  getAll(id, page) {
    const url = `client/customers/view-comment-departure/${id}?page=${page}`;
    return axiosClient.get(url);
  },
  commentDeparture(id, data) {
    const url = `client/customers/comment-departure/${id}`;
    return axiosClient.post(url, data);
  },
  contact(data) {
    const url = `client/customers/contact-add`;
    return axiosClient.post(url, data);
  },
};
export default commentDepAPI;
