import { axiosClient } from "./axiosClient";
const departureAPI = {
  getAll(params) {
    const url = `client/customers/departure-filter`;
    return axiosClient.get(url, {
      params,
    });
  },
  createOrder(id, data) {
    const url = `client/customers/invoice/add-invoice/${id}`;
    return axiosClient.post(url, data);
  },
  payment(data) {
    const url = `client/create`;
    return axiosClient.post(url, data);
  },
  commentDeparture(id, data) {
    const url = `client/customers/comment-departure/${id}`;
    return axiosClient.post(url, data);
  },
  updateDepature(id, data) {
    const url = `client/customers/update-departure/${id}`;
    return axiosClient.post(url, data);
  },
  historyDeparture() {
    const url = `client/customers/invoice/history-invoice`;
    return axiosClient.get(url);
  },
  listPolicyDeparture(id) {
    const url = `client/customers/car-departure/${id}`;
    return axiosClient.get(url);
  },
};
export default departureAPI;
