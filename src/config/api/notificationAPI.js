import { axiosClient } from "./axiosClient";
const notificationAPI = {
  sendMessageToTopic(body) {
    const url = `https://fcm.googleapis.com/fcm/send`;
    return fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${process.env.TOKEN_AUTHENTICATION_FIREBASE}`,
      },
      body: JSON.stringify(body),
    })
      .then((response) => response.json())
      .then((data) => {
        return data;
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  },
  checkIsRegister(device_token) {
    const url = `https://iid.googleapis.com/iid/v1/${device_token}/rel/topics/quanghuylemounsine`;
    return fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${process.env.TOKEN_AUTHENTICATION_FIREBASE}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        return data;
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  },
  registerDevice(body) {
    const url = `https://iid.googleapis.com/iid/v1:batchAdd`;
    return fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${process.env.TOKEN_AUTHENTICATION_FIREBASE}`,
      },
      body: JSON.stringify(body),
    })
      .then((response) => response.json())
      .then((data) => {
        return data;
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  },
  sendMessageToDB(body) {
    const url = `client/customers/notification/store`;
    return axiosClient.post(url, body);
  },
  listNotification(id, params) {
    const url = `client/customers/notification/customer/${id}`;
    return axiosClient.get(url, {
      params,
    });
  },
  updateStatusNotification(id, status) {
    const url = `client/customers/notification/update/${id}`;
    return axiosClient.post(url, status);
  },
  registerPushNotification(body, token) {
    const url = `client/customers/notification/store`;
    return axiosClient.post(url, body, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
};
export default notificationAPI;
