export { default as authAPI } from "./authAPI";
export { default as departureAPI } from "./departureAPI";
export { default as commentDepAPI } from "./commentDepAPI";
export { default as invoiceAPI } from "./invoiceAPI";
export { default as notificationAPI } from "./notificationAPI";
export { default as newsAPI } from "./newsAPI";
