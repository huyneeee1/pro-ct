import { axiosClient } from "./axiosClient";
const invoiceAPI = {
  checkingInvoince(id) {
    const url = `client/customers/invoice-code-filter?invoice_code=${id}`;
    return axiosClient.get(url);
  },
};
export default invoiceAPI;
