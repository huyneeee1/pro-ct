import { axiosClient } from "./axiosClient";
const authAPI = {
  login(user) {
    const url = `client/customers/login`;
    return axiosClient.post(url, user);
  },
  register(user) {
    const url = `client/customers/register`;
    return axiosClient.post(url, user);
  },
  verify() {
    const url = `client/customers/email/verification-notification`;
    return axiosClient.post(url, {});
  },
  verification(id, hash, express, token) {
    const url = `customers/verify-email/${id}/${hash}?${express}`;
    return axiosClient.get(url, {
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${token}`,
      },
    });
  },
  logout() {
    const url = `client/customers/logout`;
    return axiosClient.post(url);
  },
  forgotPasword(email) {
    const url = `client/customers/forgot-password`;
    return axiosClient.post(url, email);
  },
  resetPasword(data) {
    const url = `client/customers/reset-password`;
    return axiosClient.post(url, JSON.stringify(data), {
      headers: {
        "Content-Type": "application/json",
      },
    });
  },
  updateProfile(id, user) {
    const url = `client/customers/update-login/${id}`;
    return axiosClient.post(url, user);
  },
  showProfile(id) {
    const url = `client/customers/show/${id}`;
    return axiosClient.get(url);
  },
  changePassword(id, data) {
    const url = `client/customers/update-password/${id}`;
    return axiosClient.post(url, data);
  },
};
export default authAPI;
