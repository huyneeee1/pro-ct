import { axiosClient } from "./axiosClient";
const newsAPI = {
  getAll(page) {
    const url = `client/customers/news/list?page=${page}`;
    return axiosClient.get(url);
  },
  detail(slug) {
    const url = `client/customers/news/detail/${slug}`;
    return axiosClient.get(url);
  },
};
export default newsAPI;
