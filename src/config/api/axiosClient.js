import axios from "axios";
export const axiosClient = axios.create({
  baseURL: process.env.BASE_URL,
  headers: {
    "Content-Type": "multipart/form-data",
    Accept: "*",
    "X-CSRF-TOKEN": "",
    Authorization: {
      toString() {
        return `Bearer ${JSON.parse(localStorage.getItem("token"))}`;
      },
    },
  },
  timeout: 2 * 60 * 1000,
});
