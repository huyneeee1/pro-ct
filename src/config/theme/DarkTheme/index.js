const LightTheme = {
  colors: {
    white: "#FFFFFF",
    overlay: "rgba(0, 0, 0, 0.5)",
    primary: "#f09168", // light-mode
    orangePastel: ["#282A2D", "#282A2D"],
    bluePastel: "#8aa6ff",
    textPurple: [
      "#5e0a3a",
      "#ac829b",
      "#d5c5cf",
      "#141850",
      "#303179",
      "#1f1d2b",
      "#ec6190",
    ],
    yellowPastel: "#ffd188",
    kemPastel: ["#5F6368", "#f5cac2", "#fae5df"],
    bgBooking: "#282A2D",

    title: "#263240",
    grey: [
      "#5F6368", //0
      "#3C4043", //1
      "#2E3134", //2
      "#282A2D", //3
      "#202124", //4
      "#17181B", //5
      "#0E1013", //6
    ],
    black: [
      "#BDC1C6", //0
      "#F1F3F4", //1
      "#E8EAED", //2
      "#282A2D", //3
      "#BDC1C6", //4
      "#9AA0A6", //5
      "#80868B", //6
    ],
    footer: "#2a663c",
    content: "#FBC6A4",
    greenPastel: "#C9E4C5",
    secondary: [
      "#000000",
      "#333333",
      "#4E5260",
      "#828282",
      "#B7BBCB",
      "#E0E0E0",
      "#F2F3F7",
      "#FAFAFA",
    ],
    tertiary: "#08a0f7",
    status: ["#EB5757", "#22b07d"],
    border: "#E0E0E0",
    error: "#EB5757",
    background: ["#FDFDFD"],
    progress: ["#6FCF97"],
    warning: "#FFD0BC",
  },
  fontSizes: {},
  shadow: {
    shadow_1: "0 1px 20px rgba(0, 0, 0, 0.05)",
    shadow_2: "rgba(100, 100, 111, 0.2) 0px 7px 29px 0px",
  },
};

export default LightTheme;
