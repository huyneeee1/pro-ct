const LightTheme = {
  colors: {
    white: "#FFFFFF",
    overlay: "rgba(0, 0, 0, 0.5)",
    primary: "#e9633b", // light-mode
    orangePastel: ["#fda082", "#ffa079"],
    bluePastel: "#8aa6ff",
    textPurple: [
      "#5e0a3a",
      "#ac829b",
      "#d5c5cf",
      "#141850",
      "#303179",
      "#ec6190",
    ],
    yellowPastel: "#ffd188",
    kemPastel: ["#fff6f3", "#f5cac2", "#fae5df"],
    bgBooking: "#F8F8FA",
    title: "#263240",
    grey: [
      "#F8F8FA", //0
      "#F1F3F4", //1
      "#E8EAED", //2
      "#DADCEO", //3
      "#BDC1C6", //4
      "#9AA0A6", //5
      "#80868B", //6
    ],
    black: [
      "#5F6368", //0
      "#3C4043", //1
      "#2E3134", //2
      "#282A2D", //3
      "#202124", //4
      "#9AA0A6", //5
      "#0E1013", //6
    ],
    footer: "#2a663c",
    content: "#FBC6A4",
    greenPastel: "#C9E4C5",
    secondary: [
      "#000000",
      "#333333",
      "#4E5260",
      "#828282",
      "#B7BBCB",
      "#E0E0E0",
      "#F2F3F7",
      "#FAFAFA",
    ],
    tertiary: "#08a0f7",
    status: ["#EB5757", "#27AE60"],
    border: "#E0E0E0",
    error: "#EB5757",
    background: ["#FDFDFD"],
    progress: ["#6FCF97"],
    warning: "#FFD0BC",
  },
  fontSizes: {},
  shadow: {
    shadow_1: "0 1px 20px rgba(0, 0, 0, 0.05)",
    shadow_2: "rgba(100, 100, 111, 0.2) 0px 7px 29px 0px",
  },
};

export default LightTheme;
