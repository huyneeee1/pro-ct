import React from "react";
import PropTypes from "prop-types";

const PrivateTemplate = ({ children }) => {
  return (
    <div>
      <p>Private Template</p>
      <div>{children}</div>
    </div>
  );
};

PrivateTemplate.propTypes = {
  children: PropTypes.any,
};

export default PrivateTemplate;
