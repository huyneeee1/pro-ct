import { Container, Content, Footer, Header } from "rsuite";
import styled from "styled-components";
export const WrapperHeader = styled(Header)`
  height: auto;
  position: sticky;
  top: -1px;
  height: auto;
  z-index: 99;
`;
export const WrapperFooter = styled(Footer)`
  justify-content: center;
  align-items: center;
  display: flex;
  flex-direction: column;
`;
export const WrapperContent = styled(Content)`
  display: flex;
  justify-content: center;
  background: ${(props) => props.theme.colors.grey[2]};
`;
export const WrapperContainer = styled(Container)`
  width: 100%;
  height: 100%;
`;
