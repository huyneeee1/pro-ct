import { Footer, HeaderTop, MenuFooter, PublicHeader } from "organisms";
import PropTypes from "prop-types";
import React from "react";
import {
  WrapperContainer,
  WrapperContent,
  WrapperFooter,
  WrapperHeader,
} from "./styled";
import { HistoryInvoince } from "molecules";
import { useSelector } from "react-redux";
const PublicTemplate = ({ children, ...others }) => {
  const history = useSelector((state) => state.history);
  return (
    <WrapperContainer>
      <HeaderTop />
      <WrapperHeader>
        <PublicHeader />
      </WrapperHeader>
      <WrapperContent>{children}</WrapperContent>
      <WrapperFooter>
        <MenuFooter />
        <Footer />
      </WrapperFooter>
      {history && <HistoryInvoince data={history} />}
    </WrapperContainer>
  );
};

PublicTemplate.propTypes = {
  children: PropTypes.any,
};

export default React.memo(PublicTemplate);
