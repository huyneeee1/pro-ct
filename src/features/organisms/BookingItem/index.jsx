import { BaseIcon, BaseTitle } from 'atoms';
import dayjs from 'dayjs';
import { withArray, withEmpty } from 'exp-value';
import { DescriptionBookingItem } from 'molecules';
import { ModalBooking } from 'organisms';
import React, { useCallback, useState } from 'react';
import { useSelector } from 'react-redux';
import { Animation } from 'rsuite';
import { useAlert } from 'hooks';

import {
  AddressText,
  At,
  Button,
  ButtonAction,
  CateCar,
  Image,
  ItemTimeLine,
  Link,
  TextTime,
  WrappeMiddle,
  Wrapper,
  WrapperAddress,
  WrapperConent,
  WrapperDropDown,
  WrapperImage,
  WrapperPrice,
  WrapperRight,
  WrapperTimeLine,
  WrapperRow,
} from './styled';
import { useTheme } from 'styled-components';
import { departureAPI } from 'config/api';
const BookingItem = ({ data, listBank }) => {
  const theme = useTheme();
  const history = useSelector((state) => state.history);
  const [toggle, setToggle] = useState(false);
  const [modal, setModal] = useState(false);
  const [dataImage, setDataImage] = useState([]);
  const [dataPolicies, setDataPolicies] = useState([]);
  const { showWarning } = useAlert();
  const user = useSelector((state) => state.user);

  const setToggleModal = useCallback(() => {
    setModal(false);
  }, [modal]);

  const getPolicyDeparture = useCallback(async () => {
    try {
      const {
        data: { data: response },
      } = await departureAPI.listPolicyDeparture(data?.car_id);
      if (response) {
        setDataImage(withArray('car_images', response));
        setDataPolicies(withArray('policies', response));
      }
    } catch (error) {}
  }, [toggle, data]);

  const content = useCallback(() => {
    return (
      <WrapperConent>
        <WrapperImage>
          <Image
            source={`${process.env.BASE_URL_IMAGE}${withEmpty(
              'image',
              data.car_departure
            )}`}
          />
        </WrapperImage>
        <WrappeMiddle>
          <BaseTitle H2 bold black>
            {withEmpty('name', data)}
          </BaseTitle>
          <WrapperRow>
            <CateCar $bg={theme.colors.kemPastel[1]}>
              {withEmpty('name', data.car_departure)}
            </CateCar>
            <CateCar $bg={theme.colors.kemPastel[1]}>{`${withEmpty(
              'number_seats',
              data.car_departure
            )} chỗ`}</CateCar>
            <CateCar $bg={theme.colors.kemPastel[1]}>
              {withEmpty('color', data.car_departure)}
            </CateCar>
            <CateCar $bg={theme.colors.kemPastel[1]}>
              {withEmpty('license_plates', data.car_departure)}
            </CateCar>
          </WrapperRow>

          <TextTime>Giờ khởi hành sớm nhất</TextTime>
          <WrapperTimeLine className='custom-timeline'>
            <ItemTimeLine
              dot={<BaseIcon name='feather-disc' className='rs-icon' />}
            >
              <WrapperAddress>
                <At>
                  {dayjs(withEmpty('start_time', data)).format(
                    'MMM D, YYYY h:mm A'
                  )}
                </At>
                <AddressText>
                  {withEmpty('go_location_wards', data)}
                </AddressText>
              </WrapperAddress>
            </ItemTimeLine>
            <ItemTimeLine />
            <ItemTimeLine />
            <ItemTimeLine />
            <ItemTimeLine />
            <ItemTimeLine
              dot={<BaseIcon name='feather-map-pin' className='rs-icon' />}
            >
              <WrapperAddress>
                <At>
                  {dayjs(withEmpty('end_time', data)).format(
                    'MMM D, YYYY h:mm A'
                  )}
                </At>
                <AddressText>
                  {withEmpty('come_location_wards', data)}
                </AddressText>
              </WrapperAddress>
            </ItemTimeLine>
          </WrapperTimeLine>
        </WrappeMiddle>
        <WrapperRight>
          <WrapperPrice>
            <BaseTitle H4={true} black>
              Giá chỉ từ
            </BaseTitle>
            <BaseTitle H2={true} bold={true} black>
              {withEmpty('price', data)
                .toString()
                .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
              VND
            </BaseTitle>
          </WrapperPrice>
          <ButtonAction>
            <Button
              onClick={() => {
                setToggle(!toggle);
                getPolicyDeparture();
              }}
              blue
            >
              Thông tin
            </Button>
            <Button warning={true}>
              <Link href={`tel:${withEmpty('phone', data.user_departure)}`}>
                Liên Hệ
              </Link>
            </Button>
            <Button
              primary={true}
              onClick={() => {
                setToggle(false);
                if (!history) {
                  if (!user) {
                    return showWarning('Vui lòng đăng nhập để đặt vé!');
                  }
                  setModal(true);
                  return;
                }
                return showWarning('Bạn đang đặt một chuyến xe!');
              }}
            >
              Đặt vé
            </Button>
          </ButtonAction>
        </WrapperRight>
      </WrapperConent>
    );
  }, [toggle, history, data]);

  const Panel = React.forwardRef(({ ...props }, ref) => (
    <WrapperDropDown {...props} ref={ref}>
      <DescriptionBookingItem
        dataImage={dataImage}
        dataPolicies={dataPolicies}
        dataDriver={withEmpty('user_departure', data)}
        idDeparture={withArray('id', data)}
        dataDepature={data}
        toggle={toggle}
      />
    </WrapperDropDown>
  ));

  return (
    <Wrapper>
      {content()}
      <Animation.Collapse in={toggle}>
        {(props, ref) => <Panel {...props} ref={ref} />}
      </Animation.Collapse>
      <ModalBooking
        dataDepature={data}
        show={modal}
        onHide={() => setModal(false)}
        setToggleModal={setToggleModal}
        listBank={listBank}
      />
    </Wrapper>
  );
};

BookingItem.propTypes = {};

export default React.memo(BookingItem);
