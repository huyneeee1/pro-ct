import styled, { css } from "styled-components";
import { BaseImage, BaseText, BaseButton, BaseLink } from "atoms";
import { Timeline } from "rsuite";
export const Wrapper = styled.div`
  margin-bottom: 20px;
  width: 100%;
  border-radius: 10px;
  padding: 15px;
  display: flex;
  flex-direction: column;
  height: auto;
  overflow: hidden;
  .rs-icon {
    width: 18px;
    height: 18px;
    position: absolute;
    top: -3px;
    left: -4px;
    font-weight: bold !important;
    color: ${(props) => props.theme.colors.black[3]};
  }
  .custom-timeline .rs-timeline-item-content {
    margin-left: 5px;
  }
  background-color: ${(props) => props.theme.colors.bgBooking};
  box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
`;
export const WrapperConent = styled.div`
  display: flex;
  min-height: 250px;
  width: 100%;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;
export const WrapperDropDown = styled.div`
  padding-top: 20px;
`;
export const WrapperImage = styled.div`
  display: flex;
  justify-content: center;
  width: 26%;
  overflow: hidden;
  border-radius: 10px;
  @media (max-width: 768px) {
    width: 100%;
  }
`;

export const WrapperRight = styled.div`
  display: flex;
  width: 34%;
  flex-direction: column;
  text-align: right;
  justify-content: space-between;
  @media (max-width: 768px) {
    width: 100%;
  }
`;
export const WrappeMiddle = styled.div`
  display: flex;
  width: 40%;
  gap: 10px;
  padding-left: 15px;
  flex-direction: column;
  @media (max-width: 768px) {
    width: 100%;
    margin: 10px 0;
  }
`;
export const Image = styled(BaseImage)`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;
export const CateCar = styled(BaseText)`
  font-size: 16px;
  font-weight: 500;
  margin-top: 0.3rem;
  margin-right: 5px;
  ${(props) =>
    props.$bg &&
    css`
      background: ${props.$bg};
      color: ${(props) => props.theme.colors.textPurple[5]};
      width: fit-content;
      border-radius: 3px;
      font-weight: bold;
      padding: 3px 5px;
    `}
`;
export const TextTime = styled(BaseText)`
  font-weight: bold;
`;
export const WrapperTimeLine = styled(Timeline)``;
export const ItemTimeLine = styled(Timeline.Item)``;
export const At = styled(BaseText)`
  font-weight: bold;
  font-size: 16px;
  margin-right: 2px;
`;
export const AddressText = styled(BaseText)`
  margin-left: 2px;
  color: ${(props) => props.theme.colors.primary};
  font-weight: 600;
`;

export const WrapperAddress = styled.div`
  display: flex;
  flex-direction: column;
`;
export const ButtonAction = styled.div`
  display: flex;
  justify-content: flex-end;
  gap: 15px;
  @media (max-width: 768px) {
    justify-content: flex-end;
    & > button {
      margin: 0 5px;
    }
  }
`;
export const Button = styled(BaseButton)``;
export const WrapperPrice = styled.div`
  @media (max-width: 768px) {
    margin: 10px 0;
  }
`;
export const Link = styled(BaseLink)`
  color: ${(props) => props.theme.colors.white};
`;
export const WrapperRow = styled.div`
  display: flex;
  flex-wrap: wrap;
`;
