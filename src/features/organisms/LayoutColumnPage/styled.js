import styled from "styled-components";
import {
  BaseWrapper,
  BaseImage,
  BaseFlexContainer,
  BaseTitle,
  BaseText,
  BaseButton,
} from "atoms";
export const Wrapper = styled(BaseFlexContainer)`
  height: 100vh;
  flex-direction: row;
  @media (max-width: 768px) {
    flex-direction: column;
    height: 85vh;
  }
`;
export const Image = styled(BaseImage)`
  width: 100%;
`;
export const Column = styled.div`
  display: flex;
  width: 60%;
  height: 100%;
  justify-content: center;
  @media (max-width: 768px) {
    height: auto;
  }
  &:nth-child(2) {
    flex-direction: column;

    padding: 0 30px;
    align-items: flex-start;
    width: 40%;
    @media (max-width: 768px) {
      width: 100%;
      align-items: center;
    }
  }
`;
export const Title = styled(BaseTitle)`
  font-size: 2.5rem;
  font-weight: bold;
  @media (max-width: 768px) {
    font-size: 1.7rem;
  }
`;
export const Text = styled(BaseText)`
  margin: 20px 0;
`;
export const Button = styled(BaseButton)``;
