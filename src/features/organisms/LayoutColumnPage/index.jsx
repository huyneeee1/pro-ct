import PropTypes from "prop-types";
import React from "react";
import { Column, Image, Wrapper } from "./styled";
const LayoutColumnPage = ({ urlImage, children }) => {
  return (
    <Wrapper>
      <Column>
        <Image source={urlImage} />
      </Column>
      <Column column={true}>{children}</Column>
    </Wrapper>
  );
};
LayoutColumnPage.propTypes = {
  urlImage: PropTypes.string,
  children: PropTypes.node,
};
export default React.memo(LayoutColumnPage);
