import styled from "styled-components";
export const Wrapper = styled.div`
  height: auto;
  width: 70%;
  display: flex;
  flex-direction: column;
  cursor: pointer;
  margin: 30px 0;
  @media screen and (max-width: 768px) {
    width: 90%;
  }
`;
export const Title = styled.div`
  color: ${(props) => props.theme.colors.black[2]};
  font-size: 32px;
  font-weight: bold;
  border-bottom: 1px solid ${(props) => props.theme.colors.black[4]};
  padding-bottom: 5px;
  margin-bottom: 20px;
  `;
