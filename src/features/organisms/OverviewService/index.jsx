import React from 'react'
import { Wrapper, Title } from './styled'
import { OverviewItem } from 'molecules'
import { Constant } from 'utils'
const OverviewService = () => {
    return (
        <Wrapper>
            <Title>Tổng quan về dịch vụ</Title>
            {Constant.OVERVIEW.map((item,index)=>{
                return <OverviewItem key={index} title={item.title} content={item.content} />
            })}
            
        </Wrapper>
    )
}

export default OverviewService
