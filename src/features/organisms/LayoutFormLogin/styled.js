import styled from "styled-components";
import {
  BaseFlexContainer,
  BaseWrapper,
  BaseImage,
  BaseTitle,
  BaseText,
} from "atoms";
export const Container = styled.div`
  display: flex;
  width: 90%;
  padding: 40px 0;
  @media (max-width: 768px) {
    flex-direction: column-reverse;
  }
`;
export const Column = styled(BaseWrapper)`
  width: 100%;
  justify-content: center;
  align-items: center;
  background-color: ${(props) => props.theme.colors.bgBooking};
  border-radius: 20px;
  box-shadow: ${(props) => props.theme.shadow.shadow_1};
  padding: 0 20px;
  &:nth-child(1) {
    background-color: ${(props) => props.theme.colors.orangePastel[0]};
    margin: 30px 0;
    flex-direction: column;
    border-radius: 20px 0 0 20px;
    padding: 0 30px;
    box-shadow: rgb(0 0 0 / 40%) 0px 6px 5px,
      rgb(0 0 0 / 30%) 0px 10px 13px -3px, rgb(0 0 0 / 20%) 2px -3px 0px inset;
    @media (max-width: 768px) {
      border-radius: 20px;
      padding: 0 10px;
    }
  }
`;
export const RowSale = styled(BaseFlexContainer)`
  height: 150px;
  flex-direction: row;
`;
export const Image = styled(BaseImage)`
  display: flex;
  width: 25%;
`;
export const Title = styled(BaseTitle)`
  color: ${(props) => props.theme.colors.grey[0]};
`;
export const Text = styled(BaseText)`
  color: ${(props) => props.theme.colors.grey[0]};
`;
export const RowText = styled.div`
  margin-left: 10px;
`;
