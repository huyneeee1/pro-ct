import React from "react";
import PropTypes from "prop-types";
import {
  Container,
  Column,
  RowSale,
  Image,
  Title,
  Text,
  RowText,
} from "./styled";
import { IMAGES } from "assets";
const LayoutFormLogin = ({ children }) => {
  return (
    <Container>
      <Column>
        <RowSale>
          <Image source={IMAGES.ENDOW1} />
          <RowText>
            <Title H2 bold black>
              Nhận thưởng cho mỗi đặt chỗ
            </Title>
            <Text>
              Tích điểm cho mỗi lần đặt vé tại Quang Huy Lemounsine. Điểm thưởng
              có thể được quy đổi ra vé hoặc đổi những phần quà hấp dẫn.
            </Text>
          </RowText>
        </RowSale>
        <RowSale>
          <Image source={IMAGES.ENDOW2} />
          <RowText>
            <Title H2 bold black>
              Trải nghiệm đặt chỗ suôn sẻ
            </Title>
            <Text>
              Tính năng thông báo giúp bạn dễ dàng đặt vé vào thời điểm dễ dàng
              nhất.
            </Text>
          </RowText>
        </RowSale>
        <RowSale>
          <Image source={IMAGES.ENDOW3} />
          <RowText>
            <Title H2 bold black>
              Chia sẻ cảm xúc của cá nhân về chuyến đi
            </Title>
            <Text>Đánh giá, bình luận về mức độ hài lòng của chuyến đi.</Text>
          </RowText>
        </RowSale>
      </Column>
      <Column>{children}</Column>
    </Container>
  );
};

LayoutFormLogin.propTypes = {
  children: PropTypes.node,
};

export default LayoutFormLogin;
