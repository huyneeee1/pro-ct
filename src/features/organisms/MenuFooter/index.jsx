import React from "react";
import { Wrapper, Column, Title, Text, Button, InputContact } from "./styled";
import { Constant } from "utils";
import { CustomInput } from "molecules";
import { BaseForm, BaseIcon } from "atoms";
const MenuFooter = () => {
  return (
    <Wrapper>
      <Column>
        <Title H4 uppercase>
          Khám phá và tận hưởng
        </Title>
        <Text $padding>
          Quang Huy limousine được ra đời và phát triển với mong muốn mang đến
          một dịch vụ đẳng cấp, thuận tiện và tiết kiệm hơn cho khách hàng. Hệ
          thống Quang Huy limousine với các công cụ được thiết kế tối giản và
          thông minh giúp bất kì ai cũng có thể dễ dàng thao tác, dẽ dàng tìm
          kiếm dịch vụ theo nhu cầu cá nhân, dội ngũ nhân sự tận tình hỗ trợ
          khách hàng, hotline hỗ trợ 24/7
        </Text>
      </Column>

      <Column padding="0 30px">
        <Title H4 uppercase>
          Trung tâm đặt vé trực tuyến
        </Title>
        <Button>Hotline: {Constant.PROFILE.PHONE}</Button>
        <Text bold>Giờ làm việc: 24/7</Text>
        <Text bold>Email: {Constant.PROFILE.EMAIL}</Text>
        <Text bold>Phone: {Constant.PROFILE.PHONE}</Text>
      </Column>
      <Column>
        <Title H4 uppercase>
          Liên hệ với chúng tôi
        </Title>
        <Text>
          Vui lòng để lại email, chúng tôi sẽ cập nhật những chương trình ưu đãi
          và tin tức mới nhất của Quang Huy limousine tới quý khách
        </Text>
      </Column>
    </Wrapper>
  );
};

export default MenuFooter;
