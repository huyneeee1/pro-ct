import styled, { css } from "styled-components";
import {
  BaseFlexContainer,
  BaseTitle,
  BaseText,
  BaseButton,
  BaseWrapper,
} from "atoms";
import { CustomInput } from "molecules";
export const Wrapper = styled(BaseFlexContainer)`
  flex-direction: row;
  height: auto;
  align-items: flex-start;
  justify-content: center;
  background: ${(props) => props.theme.colors.grey[1]};
  padding: 20px 10%;
  @media screen and (max-width: 768px) {
    flex-direction: column;
    width: 100%;
    padding: 10px 20px;
  }
`;
export const Column = styled(BaseWrapper)`
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  ${(props) =>
    props.padding &&
    css`
      padding: ${props.padding};
    `}
  @media screen and (max-width: 768px) {
    padding: 0;
  }
`;

export const Text = styled(BaseText)`
  ${(props) => props.$padding && `padding-bottom:10px;`}
`;
export const Title = styled(BaseTitle)`
  margin: 20px 0;
  font-weight: bold;
  color: ${(props) => props.theme.colors.black[0]};
`;
export const Button = styled(BaseButton)`
  background: ${(props) => props.theme.colors.orangePastel[0]};
  color: ${(props) => props.theme.colors.grey[0]};
  font-size: 24px;
  font-weight: 900;
  padding: 15px 10px;
  margin-bottom: 10px 0;
  width: 100%;
`;
export const InputContact = styled.div``;
