import styled, { css } from "styled-components";
import { BaseItemGrid, BaseIcon, BaseText } from "atoms";
import { FlexboxGrid } from "rsuite";
export const Wrapper = styled(FlexboxGrid)`
  line-height: 50px;
  height: 50px;
  padding: 0 20px;
  background-color: ${(props) => props.theme.colors.grey[0]};
`;
export const Column = styled(BaseItemGrid)`
  ${(props) =>
    props.$textRight &&
    css`
      text-align: right;
      justify-content: flex-end;
    `}
  display:flex;
  height: 100%;
  align-items: center;
`;
export const Icon = styled(BaseIcon)`
  margin: 0 5px;
  color: ${(props) => props.theme.colors.primary};
  font-weight: 500;
`;
export const Text = styled(BaseText)`
  margin-right: 5px;
  ${(props) =>
    props.primary &&
    css`
      color: ${props.theme.colors.orangePastel[1]};
    `}
`;
export const WrapperText = styled.div`
  display: flex;
  align-items: center;
`;
export const WrapperSearch = styled.div`
  display: flex;
  background-color: ${(props) => props.theme.colors.grey[0]};
`;
