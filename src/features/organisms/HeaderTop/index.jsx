import React from "react";
import { Constant } from "utils";
import { Column, Icon, Text, Wrapper, WrapperText } from "./styled";
import { ToggleDarkLight } from "molecules";
const HeaderTop = () => {
  return (
    <Wrapper>
      <Column colspan={12}>
        <WrapperText>
          <Icon name="feather-phone-call" size={15} />
          <Text>Hotline: </Text>
          <Text bold={true} primary={true}>
            {Constant.PROFILE.PHONE}
          </Text>
        </WrapperText>
      </Column>
      <Column colspan={12} $textRight={true}>
        <WrapperText>
          <ToggleDarkLight />
          <Text>Language: 🇻🇳 </Text>
        </WrapperText>
      </Column>
    </Wrapper>
  );
};

export default React.memo(HeaderTop);
