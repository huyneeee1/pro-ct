import { IMAGES } from "assets";
import { authAPI } from "config/api";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import { useAlert } from "hooks";
import { NotificationModule, ToggleDarkLight } from "molecules";
import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { logout } from "reduxFolder/userSlice";
import { useTheme } from "styled-components";
import { Routers } from "utils";
import {
  Brand,
  DropdownItem,
  Icon,
  Logo,
  NavItem,
  NavItemDrop,
  Profile,
  Wrapper,
  WrapperDropdown,
  WrapperDropDownNav,
  WrapperLogin,
  WrapperNav,
  WrapperNavBar,
  WrapperNavMobie,
  NoneImg,
} from "./styled";
import { resetHistoryInvoince } from "reduxFolder/historyInvoiceSlice";
import { resetNotification } from "reduxFolder/notificationSlice";
import { setLoading } from "reduxFolder/loadingSlice";
const PublicHeader = (props) => {
  dayjs.extend(relativeTime);
  const theme = useTheme();
  const history = useHistory();
  const [activeKey, setActiveKey] = useState(null);
  const { showSuccess, showError } = useAlert();
  const [currentUser, setCurrentUser] = useState(null);
  const [listNav, setListNav] = useState([]);
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const [toggleNavMb, setToggleNavMb] = useState(true);
  const renderIconButton = useCallback(
    (props, ref) => {
      return user?.image ? (
        <Profile
          {...props}
          ref={ref}
          circle
          src={`${process.env.BASE_URL_IMAGE}${user?.image}`}
        />
      ) : (
        <NoneImg {...props} ref={ref}>
          {user?.last_name?.toUpperCase().substring(0, 1)}
        </NoneImg>
      );
    },
    [user]
  );
  const handleLogout = useCallback(async () => {
    dispatch(setLoading(true));
    try {
      const response = await authAPI.logout();
      if (response) {
        showSuccess("Đăng xuất thành công !");
        dispatch(logout());
        dispatch(resetNotification());
        dispatch(resetHistoryInvoince());
        history.push(history.push(Routers.HOME));
        dispatch(setLoading(false));
      }
    } catch (error) {
      dispatch(setLoading(false));
      showError("Đăng xuất thất bại!");
    }
  }, [user]);
  useEffect(() => {
    setListNav([...Routers.NAV]);
  }, []);
  useEffect(() => {
    if (!user) {
      setCurrentUser(null);
      return;
    }
    setCurrentUser(user);
  }, [user]);
  const _renderNavMobie = useCallback(() => {
    return (
      <WrapperNavMobie>
        <Wrapper>
          <Logo
            source={IMAGES.LOGOWHITE}
            onClick={() => history.push(history.push(Routers.HOME))}
          />
        </Wrapper>

        <Wrapper>
          {user && <NotificationModule />}
          <Icon
            name="feather-align-justify"
            onClick={() => {
              setToggleNavMb(false);
            }}
          />
        </Wrapper>
        <WrapperDropDownNav $display={toggleNavMb}>
          <Brand to="/" onClick={() => setToggleNavMb(true)}>
            <Logo source={IMAGES.LOGOWHITE} />
          </Brand>
          <Icon
            name="feather-x"
            $position
            onClick={() => {
              setToggleNavMb(true);
            }}
            style={{ color: theme.colors.bgBooking }}
          />
          <WrapperNav active={activeKey} onSelect={setActiveKey}>
            {listNav.map((item, index) => {
              return (
                <NavItemDrop
                  key={index}
                  to={item.URL}
                  onClick={() => {
                    setToggleNavMb(true);
                  }}
                  eventKey={item.NAME}
                >
                  {item.NAME}
                </NavItemDrop>
              );
            })}
          </WrapperNav>
          <WrapperLogin
            onClick={() => {
              history.push(history.push(Routers.LOGIN));
              setToggleNavMb(true);
            }}
          >
            Login
          </WrapperLogin>
          <ToggleDarkLight display />
        </WrapperDropDownNav>
      </WrapperNavMobie>
    );
  }, [toggleNavMb]);
  return (
    <>
      <WrapperNavBar {...props}>
        <Brand to="/">
          <Logo source={IMAGES.LOGOWHITE} />
        </Brand>
        <WrapperNav onSelect={setActiveKey} activeKey={activeKey}>
          {listNav.map((item, index) => {
            return (
              <NavItem key={index} to={item.URL}>
                {item.NAME}
              </NavItem>
            );
          })}
        </WrapperNav>
        <Wrapper $notifications>
          {user && <NotificationModule />}
          {currentUser ? (
            <WrapperDropdown
              menuStyle={{
                width: "170px",
                backgroundColor: theme.colors.bgBooking,
              }}
              renderToggle={renderIconButton}
            >
              <DropdownItem
                onClick={() => {
                  history.push(Routers.PROFILE);
                }}
              >
                Thông tin cá nhân
              </DropdownItem>
              <DropdownItem onClick={handleLogout}>Đăng xuất</DropdownItem>
            </WrapperDropdown>
          ) : (
            <NavItem to={Routers.LOGIN}>Đăng nhập</NavItem>
          )}
        </Wrapper>
      </WrapperNavBar>
      {_renderNavMobie()}
    </>
  );
};

export default React.memo(PublicHeader);
