import styled, { css, keyframes } from "styled-components";
import { Navbar, Nav, Dropdown, Avatar } from "rsuite";
import { BaseIcon, BaseImage, BaseBadge, BaseText, BaseTitle } from "atoms";
import { NavLink } from "react-router-dom";
export const WrapperNavBar = styled(Navbar)`
  height: 100%;
  background: ${(props) => props.theme.colors.orangePastel[1]};
  display: flex;
  justify-content: space-around;
  align-items: center;
  padding: 15px 0;
  position:relative;
  .rs-navbar-item-active {
    color: ${(props) => props.theme.colors.textPurple[1]} !important;
    text-decoration: none !important;
  }
  .rs-navbar-item:hover {
    background-color:${(props) => props.theme.colors.white}; !important;
    text-decoration: none !important;
    color: ${(props) => props.theme.colors.primary} !important;
    border-bottom: 2px solid ${(props) => props.theme.colors.primary};
  }
    .rs-avatar > .rs-avatar-image {
    object-fit: contain !important;
  }
  @media screen and (max-width: 768px) {
    display: none;
  }

`;

export const WrapperNav = styled(Nav)`
  display: flex;
  justify-content: center;
  align-items: center;
  @media screen and (max-width: 768px) {
    flex-direction: column;
    width: 100%;
  }
`;
const rotate = keyframes`
  from {
    top:-100vh;  
    opacity:0;
  }

  to {
    top:0;
    opacity:1;
  }
`;
const navLinkFade = keyframes`
  from{
    opacity:0;
    transform: translateY(-100vh);
  }
  to{
    opacity: 1;
    transform: translateY(0px);
  }
`;
export const WrapperDropDownNav = styled.div`
  background: ${(props) => props.theme.colors.orangePastel[1]};
  position: fixed;
  width: 100vw;
  z-index: 9;
  height: 100vh;
  top: 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px 0;
  justify-content: center;
  animation: ${rotate} 1.5s ease forwards;
  ${(props) =>
    props.$display &&
    css`
      display: none;
    `};
`;
export const Brand = styled(NavLink)`
  padding: 0px 0px !important;
  display: flex;
  @media screen and (max-width: 768px) {
    position: absolute;
    top: 10px;
  }
`;

export const NavItem = styled(NavLink)`
  font-weight: bold;
  font-size: 16.5px;
  display: flex;
  align-items: center;
  padding: 15px 20px;
  color: ${(props) => props.theme.colors.white};
  &:hover {
    color: ${(props) => props.theme.colors.white};

    text-decoration: none;
    border-bottom: 2px solid ${(props) => props.theme.colors.white};
  }

  &.active {
    color: ${(props) => props.theme.colors.white};
    text-decoration: none;
    border-bottom: 2px solid ${(props) => props.theme.colors.white};
  }
`;
export const NavItemDrop = styled(NavItem)`
  animation: ${navLinkFade} 1.5s ease forwards;
`;
export const WrapperDropdown = styled(Dropdown)``;
export const DropdownItem = styled(Dropdown.Item)`
  font-weight: bold;
  font-size: 15px;
`;
export const Logo = styled(BaseImage)`
  width: 120px;
  @media screen and (max-width: 768px) {
    width: 80px;
    margin-left: 10px;
  }
`;
export const Profile = styled(Avatar)`
  cursor: pointer;
`;
export const WrapperNavMobie = styled.div`
  @media screen and (min-width: 768px) {
    display: none;
  }
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 15px 0;
  height: 100%;
  background-color: ${(props) => props.theme.colors.orangePastel[1]};
`;
export const Icon = styled(BaseIcon)`
  width: 30px;
  height: 30px;
  display: flex;
  margin-right: 10px;
  color: ${(props) => props.theme.colors.bgBooking};
  ${(props) =>
    props.$position &&
    css`
      position: absolute;
      top: 10px;
      right: 10px;
    `}
`;
export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  min-width: 80px;
  ${(props) =>
    props.$notifications &&
    `
    .rs-dropdown-item {
      white-space:normal;
    }
  `}
`;
export const WrapperLogin = styled.div`
  margin-top: 20px;
  width: 120px;
  padding: 10px 20px;
  text-align: center;
  font-size: 15px;
  font-weight: bold;
  color: ${(props) => props.theme.colors.bgBooking};
  border: 1px solid ${(props) => props.theme.colors.bgBooking};
  border-radius: 3px;
`;
export const NoneImg = styled.div`
  cursor: pointer;
  background: #fff;
  border-radius: 50%;
  height: 40px;
  width: 40px;
  line-height: 40px;
  text-align: center;
  font-weight: bold;
  font-size: 20px;
  color: #e9633b;
`;
