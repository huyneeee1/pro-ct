import React, { useState, useEffect } from "react";
import { NewItemSideBar } from "molecules";
import { newsAPI } from "config/api";
import { useDispatch } from "react-redux";
import { setLoading } from "reduxFolder/loadingSlice";
const SideBarNews = () => {
  const dispatch = useDispatch();
  const [data, setData] = useState([]);
  useEffect(() => {
    dispatch(setLoading(true));

    async function execute() {
      try {
        const { data: response } = await newsAPI.getAll(1);
        if (response) {
          setData(response.data);
          dispatch(setLoading(false));
        }
      } catch (error) {
        dispatch(setLoading(false));
      }
    }
    execute();
  }, []);
  return (
    <>
      {data?.length > 0 &&
        data.map((ele, index) => {
          return <NewItemSideBar data={ele} key={index} />;
        })}
    </>
  );
};

export default SideBarNews;
