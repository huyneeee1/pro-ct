import styled, { css } from "styled-components";
import { FlexboxGrid } from "rsuite";
import { BaseItemGrid, BaseImage, BaseFlexContainer } from "atoms";
export const Wrapper = styled(FlexboxGrid)`
  width: 100%;
  padding: 30px 10%;
  background: ${(props) => props.theme.colors.orangePastel[1]};
  color: ${(props) => props.theme.colors.grey[2]};
  align-items: flex-start;
  @media screen and (max-width: 768px) {
    flex-direction: column;
    padding: 10px;
  }
`;
export const Column = styled(BaseItemGrid)`
  padding: 0 10px;
  display: flex;
  align-items: center;
  flex-direction: column;
  @media screen and (max-width: 768px) {
    width: 100%;
    margin: 10px 0;
  }
`;
export const Logo = styled(BaseImage)`
  width: 200px;
`;
export const WrapperText = styled(BaseFlexContainer)`
  align-items: flex-start;
  gap: 10px;
`;
