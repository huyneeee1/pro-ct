import React from 'react';
import { Wrapper, Column, Logo, WrapperText } from './styled';
import { BaseText, BaseTitle } from 'atoms';
import { IMAGES } from 'assets';
import { Constant } from 'utils';
import { ActionSocial } from 'molecules';
const Footer = () => {
  return (
    <Wrapper align='middle'>
      <Column colspan={8}>
        <Logo source={IMAGES.LOGOWHITE} />
        {/* <BaseText white={true}>
          Quang Huy limousine hân hạnh được đồng hành cùng bạn!
        </BaseText> */}
      </Column>
      <Column colspan={8}>
        <WrapperText>
          <BaseTitle H3 bold uppercase white={true}>
            Quang Huy limousine
          </BaseTitle>
          <BaseText white={true}>Văn phòng: {Constant.PROFILE.OFFICE}</BaseText>
          <BaseText white={true}>Giờ làm việc: 24/7</BaseText>
          <BaseText white={true}>Email: {Constant.PROFILE.EMAIL}</BaseText>
          <BaseText white={true}>Phone: {Constant.PROFILE.PHONE}</BaseText>
        </WrapperText>
      </Column>
      <Column colspan={8}>
        <WrapperText>
          <BaseText white={true}>
            Quang Huy limousine là nền tảng cung cấp dịch vụ đặt vé online, được
            khách hàng yêu thích và tin tưởng bởi sự nhanh chóng, an toàn và
            thuận tiện
          </BaseText>
          <ActionSocial />
        </WrapperText>
      </Column>
    </Wrapper>
  );
};

export default Footer;
