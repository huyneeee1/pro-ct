import styled, { css } from "styled-components";
import {
  BaseModal,
  BaseWrapper,
  BaseButton,
  BaseFlexContainer,
  BaseInputPicker,
  BaseIcon,
  BaseRadio,
  BaseImage,
  BaseForm,
} from "atoms";
import IconButton from "rsuite/IconButton";
import { PayPalButton } from "react-paypal-button-v2";
import { Steps, RadioGroup } from "rsuite";
export const Wrapper = styled(BaseModal)``;
export const WrapperStep = styled(Steps)`
  min-height: 60px;
`;
export const StepItem = styled(Steps.Item)``;
export const ButtonGroup = styled(BaseWrapper)`
  display: flex;
  justify-content: flex-end;
  ${(props) =>
    props.$row &&
    `
    width:100%;
  justify-content: flex-start;

  `}
`;
export const Button = styled(BaseButton)`
  margin-bottom: 0 !important;
  ${(props) =>
    props.$mr &&
    css`
      margin-right: 10px;
    `}
`;

export const WrapperBody = styled(BaseFlexContainer)`
  width: 100%;
  height: 500px;
  padding: 0 10px;
  margin-top: 15px;
  align-items: center;
  overflow: scroll;
  &::-webkit-scrollbar {
    width: 0;
  }
  scrollbar-width: none;
  overflow-x: hidden;
`;
export const SelectHour = styled(BaseInputPicker)`
  width: 100%;
`;
export const Tab = styled(BaseFlexContainer)`
  flex-direction: row;
  margin-bottom: 15px;
  ${(props) =>
    props.$justifycontent &&
    css`
      justify-content: ${props.$justifycontent};
    `}
  ${(props) =>
    props.$borderBottom &&
    css`
      border-bottom: 1px solid ${(props) => props.theme.colors.secondary[4]};
    `}
`;
export const TabItem = styled.div`
  padding: 15px 20px;
  color: ${(props) => props.theme.colors.primary};
  font-weight: 800;
  border: 1px solid ${(props) => props.theme.colors.primary};
  width: 100%;
  text-align: center;
  cursor: pointer;
  &:first-child {
    border-right: 1px solid #fff;
  }
  ${(props) =>
    props.$active &&
    css`
      color: ${(props) => props.theme.colors.white};
      background: ${(props) => props.theme.colors.primary};
    `}
`;
export const WrapperContentModal = styled.div`
  height: 100%;
  width: 100%;
  ${(props) =>
    props.$height &&
    `
    height: 100%;
  `}
  ${(props) =>
    props.$flex &&
    css`
      display: flex;
      align-items: center;
      justify-content: center;
      flex-direction: column;
    `}
  ${(props) =>
    props.$row &&
    css`
      flex-direction: row;
    `}
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;
export const Icon = styled(BaseIcon)`
  width: 30px;
  height: 30px;
  margin: 10px 0;
  color: ${(props) => props.theme.colors.secondary[2]};
  ${(props) =>
    props.$actived &&
    css`
      color: ${(props) => props.theme.colors.progress};
    `}
  ${(props) =>
    props.$chossed &&
    css`
      color: ${(props) => props.theme.colors.error};
    `}
`;
export const WrapperCar = styled.div`
  width: 150px;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  justify-items: center;
  align-items: center;
  background: ${(props) => props.theme.colors.secondary[4]};
  border-radius: 40px 40px 0px 0px;
`;
export const Column = styled(BaseFlexContainer)`
  padding: 15px 20px;
  ${(props) =>
    props.$border &&
    css`
      box-shadow: 0px 0px 9px 2px #888888;
      border-radius: 8px;
    `};
  ${(props) =>
    props.$row &&
    css`
      flex-direction: row;
    `}
  ${(props) =>
    props.$mr &&
    css`
      margin: 10px 0;
    `}
`;
export const ButtonSeat = styled(IconButton)`
  ${(props) =>
    props.$visible &&
    css`
      background: unset;
      :hover,
      :active,
      :focus {
        background: none;
        border: none;
      }
      &:nth-child(5),
      :nth-child(8),
      :nth-child(1) {
        visibility: hidden;
      }
    `}
`;
export const WrapperText = styled(BaseWrapper)`
  justify-content: space-between;
  align-items: center;
  width: 100%;
  ${(props) =>
    props.$justifyStart &&
    css`
      justify-content: flex-start;
    `}
  ${(props) =>
    props.$mr &&
    css`
      margin: ${props.$mr};
    `}
    padding: 8px 0;
  ${(props) =>
    props.$border &&
    css`
      border-top: 1px solid ${(props) => props.theme.colors.secondary[4]};
    `}
  ${(props) =>
    props.$column &&
    css`
      flex-direction: column;
      justify-content: flex-start;
      align-items: start;
    `}
  ${(props) =>
    props.$borderBottom &&
    css`
      border-bottom: 1px solid ${(props) => props.theme.colors.secondary[4]};
      margin-bottom: 10px;
    `}
`;
export const RadioPicker = styled.input`
  display: flex;
  width: 10%;
  margin-top: 13px;
  height: 18px;
`;
export const WrapperPayment = styled(BaseWrapper)`
  width: 90%;
  flex-direction: column;
`;
export const Form = styled(BaseForm)`
  display: contents;
  ${(props) =>
    props.$hidden &&
    css`
      margin: 0 !important;
    `}
  margin: 10px 0;
  // width:70%;
  ${(props) =>
    props.$padding &&
    css`
      padding: ${props.$padding};
    `}
`;
export const WrapperRowInput = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  & > .rs-form-group {
    width: 48%;
  }
`;

export const WrapperContent = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  ${(props) =>
    props.$center &&
    `
    justify-content:center;
    align-items: center;
    text-align:center;
  `}
`;
export const WrapperPayPal = styled(PayPalButton)`
  width: 100%;
  display: flex;
`;
export const SelectOptionPayment = styled(BaseRadio)``;
export const SelectGroup = styled(RadioGroup)`
  width: 100%;
`;
export const Image = styled(BaseImage)`
  width: 80px;
  height: 30px;
`;
