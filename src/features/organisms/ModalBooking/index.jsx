import firebase from '@firebase/app';
import CheckIcon from '@rsuite/icons/Check';
import LocationIcon from '@rsuite/icons/Location';
import TaskIcon from '@rsuite/icons/Task';
import UserInfoIcon from '@rsuite/icons/UserInfo';
import { IMAGES } from 'assets';
import { BaseIcon, BaseImage, BaseText, BaseTitle } from 'atoms';
import { departureAPI, notificationAPI } from 'config/api';
import { auth } from 'config/firebase';
import { withEmpty, withNumber } from 'exp-value';
import { useAlert } from 'hooks';
import html2canvas from 'html2canvas';
import { CustomInput, FormOTP } from 'molecules';
import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useState } from 'react';
import QRCode from 'react-qr-code';
import { useDispatch, useSelector } from 'react-redux';
import { saveHistoryInvoince } from 'reduxFolder/historyInvoiceSlice';
import { setLoading } from 'reduxFolder/loadingSlice';
import {
  Button,
  ButtonGroup,
  Column,
  Form,
  SelectGroup,
  SelectHour,
  SelectOptionPayment,
  StepItem,
  Wrapper,
  WrapperBody,
  WrapperContent,
  WrapperContentModal,
  WrapperPayment,
  WrapperStep,
  WrapperText,
  Image,
} from './styled';
import axios from 'axios';
import { formStep1Validate } from './validation';
import io from 'socket.io-client';

const ModalBooking = ({
  show,
  onHide,
  setToggleModal,
  dataDepature,
  listBank,
}) => {
  const { showWarning, showSuccess, showError } = useAlert();
  const [step, setStep] = useState(0);
  const [dataSeat, setDataSeat] = useState([]);
  const [canvas, setCanvas] = useState('');
  const [visbleQRCODE, setVisbleQRCODE] = useState(false);
  const [invoiceCODE, setInvoiceCODE] = useState('');
  const user = useSelector((state) => state.user);
  const [otpValue, setOtpValue] = useState({
    otp: '',
    status: true,
  });
  const [result, setResult] = useState('');
  const dispatch = useDispatch();
  const [data, setData] = useState({
    name: '',
    phone: '',
    email: '',
    note: '',
    go_point: '',
    come_point: '',
    gioXuatPhat: '',
    form_payment: 1,
    quantity: 1,
    bank_code: 'NCB',
  });
  const socket = io.connect(process.env.WEBSOCKET);
  const [redrect, setRedrect] = useState(null);

  useEffect(() => {
    let arr = [];
    const seats_departures = withEmpty('seats_departures', dataDepature);
    for (let i = 1; i <= seats_departures; i++) {
      arr.push({
        label: `${i} vé`,
        value: i,
      });
    }
    setDataSeat(arr);
  }, [dataDepature]);

  useEffect(() => {
    html2canvas(document.querySelector('#QRCODE')).then((_canvas) => {
      setCanvas(_canvas);
    });
  }, [show]);

  const sendMessage = async (body) => {
    await socket.emit('send_message', body);
  };

  const onHandleInput = useCallback(
    (key, value) => {
      setData((current) => ({ ...current, [key]: value }));
    },
    [data]
  );

  const Step = () => {
    return (
      <WrapperStep current={step}>
        <StepItem
          title='Bước 1'
          description='Thông tin khách hàng'
          icon={<UserInfoIcon style={{ fontSize: 22 }} />}
        />
        <StepItem
          title='Bước 2'
          description='Xác minh'
          icon={<LocationIcon style={{ fontSize: 22 }} />}
        />
        <StepItem
          title='Bước 3'
          description='Chọn vé'
          icon={<TaskIcon style={{ fontSize: 22 }} />}
        />
        <StepItem
          title='Bước 4'
          description='Thanh toán'
          icon={<CheckIcon style={{ fontSize: 22 }} />}
        />
      </WrapperStep>
    );
  };

  const sendSMS = async (_invoiceCODE, toPhone) => {
    const to = `+84${toPhone.substring(1)}`;
    let dataSMS = new FormData();
    dataSMS.append('From', process.env.PHONE_FROM_SMS);
    dataSMS.append('To', to);
    dataSMS.append(
      'Body',
      `Cảm ơn bạn đã đặt vé tại Quang Huy Limounsine. Mã vé :${_invoiceCODE}. Bạn có thể check vé tại đây : ${process.env.URL_QR_CODE}${_invoiceCODE}`
    );
    try {
      await axios.post(process.env.API_SEND_SMS, dataSMS, {
        auth: {
          username: process.env.USER_SEND_SMS,
          password: process.env.PASSWORD_SMS,
        },
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      });
    } catch (error) {}
  };

  const onSubmit = useCallback(() => {
    const isPhone = (phone) => {
      return /^(84|0[3|5|7|8|9])+([0-9]{8})\b/.test(phone);
    };
    const isEmail = (email) => {
      return /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        email
      );
    };
    if (step === 0) {
      if (
        !withEmpty('name', data) ||
        !isPhone(withEmpty('phone', data)) ||
        !isEmail(withEmpty('email', data))
      ) {
        return;
      } else {
        if (otpValue.status) {
          const { phone } = data;
          let _verify = new firebase.auth.RecaptchaVerifier(
            'recaptcha-container',
            {
              size: 'invisible',
            }
          );
          auth
            .signInWithPhoneNumber(`+84${phone}`, _verify)
            .then((_result) => {
              setResult(_result);
            })
            .catch((err) => {
              showError('Error');
            });
        }
      }
    }
    if (step === 3) {
      return;
    }
    setStep(step + 1);
  }, [data, step, otpValue]);

  const handleSubmit = useCallback(() => {
    dispatch(setLoading(true));
    async function execute() {
      try {
        let formData = new FormData();
        formData.append('phone', data.phone);
        formData.append('name', data.name);
        formData.append('email', data.email);
        formData.append('go_point', dataDepature.go_location_wards);
        formData.append('come_point', dataDepature.come_location_wards);
        formData.append('quantity', data.quantity);
        formData.append('note', data.note || 'null');
        formData.append('total_price', data.quantity * dataDepature.price);
        formData.append(
          'form_payment',
          data.form_payment === 1
            ? 'Thanh toán online'
            : 'Thanh toán khi lên xe'
        );
        formData.append('status', 0);
        // append data departure update
        const {
          data: { data: response },
        } = await departureAPI.createOrder(
          withEmpty('id', dataDepature),
          formData
        );
        if (response) {
          dispatch(
            saveHistoryInvoince({
              ...response,
              departure: { name: dataDepature.name },
            })
          );
          const invoince_CODE = withEmpty('invoice_code', response);
          const title = `Khách hàng: ${data?.name} đặt ${dataDepature.name}`;
          const content = `Khách hàng ${data?.name} ${user?.last_name} đặt ${data.quantity} vé . Mã số vé là : #${invoince_CODE}. SDT liên hệ : ${data?.phone}`;
          let bodySendDBNotifi = new FormData();
          bodySendDBNotifi.append('token_device', 'none');
          bodySendDBNotifi.append('invoice_id', response.id);
          bodySendDBNotifi.append('user_id', user?.id);
          bodySendDBNotifi.append('title', title);
          bodySendDBNotifi.append('content', content);
          bodySendDBNotifi.append('status', 0);
          bodySendDBNotifi.append('is_send', 0);
          bodySendDBNotifi.append('role_id', 1);
          bodySendDBNotifi.append(
            'avatar_notification',
            user?.image ||
              'https://gpihefp.com/wp-content/uploads/2021/05/male_boy_person_people_avatar_icon_159358.png'
          );
          const {
            data: { data: responseNoti },
          } = await notificationAPI.sendMessageToDB(bodySendDBNotifi);
          if (responseNoti) {
            const bodyNoti = {
              invoice_id: response.id,
              title,
              content,
              avatar_notification:
                user?.image ||
                'https://gpihefp.com/wp-content/uploads/2021/05/male_boy_person_people_avatar_icon_159358.png',
              is_send: 0,
              status: 0,
              created_at: new Date(),
              id: responseNoti?.id || 'none',
            };
            sendMessage(bodyNoti);
          }
          setInvoiceCODE(invoince_CODE);
          setVisbleQRCODE(true);
          setToggleModal(false);
          showSuccess('Đặt vé thành công !');
          setData('');
          setStep(0);
          setOtpValue({
            otp: '',
            status: true,
          });
          sendSMS(withEmpty('invoice_code', response), data.phone);
          dispatch(setLoading(false));
          // case payment online
          if (data.form_payment === 1) {
            let paymentData = new FormData();
            paymentData.append('amount', response.total_price);
            paymentData.append('order_desc', `${response.name} dat xe`);
            paymentData.append('order_type', 'billpayment');
            paymentData.append('language', 'vn');
            paymentData.append('bank_code', data.bank_code?.toUpperCase());
            paymentData.append('invoice_id', response.id);
            const { data: resPayment } = await departureAPI.payment(
              paymentData
            );
            if (resPayment?.code === '00') {
              setRedrect(resPayment.data);
            }
            return;
          }
        }
      } catch (error) {
        setData('');
        setStep(0);
        setOtpValue({
          otp: '',
          status: true,
        });
        setToggleModal(false);
        setInvoiceCODE('');
        dispatch(setLoading(false));
        showError(
          'Đặt vé thất bại ! Quý khách vui lòng kiểm tra lại hoặc liên hệ với nhân viên tư vấn khách hàng'
        );
      }
    }
    execute();
  }, [data, user]);

  const onVerifyOTP = useCallback(() => {
    const { otp } = otpValue;
    dispatch(setLoading(true));
    result
      .confirm(otp)
      .then((_result) => {
        dispatch(setLoading(false));
        showSuccess('Xác thực số điện thoại thành công !');
        setStep(2);
        setOtpValue({ ...otpValue, status: false });
      })
      .catch((err) => {
        dispatch(setLoading(false));
        showError(err);
      });
  }, [otpValue]);

  const TextRow = ({ textRight, textLeft, mr, ...others }) => {
    return (
      <WrapperText {...others}>
        <BaseTitle bold mr={mr} black>
          {textLeft}
        </BaseTitle>
        <BaseTitle bold black>
          {textRight}
        </BaseTitle>
      </WrapperText>
    );
  };

  const _renderInfoCustomBooking = useCallback(() => {
    return (
      <Column $border={true} $mr={true}>
        <WrapperText $borderBottom={true}>
          <BaseTitle bold H3 primary>
            Thông tin khách hàng
          </BaseTitle>
        </WrapperText>
        <WrapperText $justifyStart={true}>
          <BaseText>
            Thông tin khách hàng đặt vé: <b>{data?.name}</b> -
            <b>{data?.phone}</b>
          </BaseText>
        </WrapperText>
        <WrapperText $justifyStart={true}>
          <BaseIcon name='feather-map-pin' />
          <BaseText>
            Điểm đón : <b>{dataDepature?.go_location_wards}</b> -
            <b>{dataDepature?.go_location_district}</b> -
            <b>{dataDepature?.go_location_city}</b>
          </BaseText>
        </WrapperText>
        <WrapperText $justifyStart={true}>
          <BaseIcon name='feather-map-pin' />
          <BaseText>
            Điểm đón : <b>{dataDepature?.come_location_wards}</b> -
            <b>{dataDepature?.come_location_district}</b> -
            <b>{dataDepature?.come_location_city}</b>
          </BaseText>
        </WrapperText>
        <WrapperText $justifyStart={true}>
          <BaseIcon name='feather-clock' />
          <BaseText>
            Thời gian xuất phát: <b>{dataDepature?.start_time}</b>
          </BaseText>
        </WrapperText>
      </Column>
    );
  }, [data]);

  const _renderInfoBooking = useCallback(() => {
    return (
      <Column $border={true} $mr>
        <WrapperText $justifyStart={true} $mr='10px 0'>
          <BaseTitle H2 bold mr='0 10px 0 0' black>
            {withEmpty('name', dataDepature)}
          </BaseTitle>
          <BaseText tertiary>
            {data.form_payment === 1
              ? 'Thanh toán online'
              : 'Thanh toán khi lên xe'}
          </BaseText>
        </WrapperText>
        <WrapperText $justifyStart={true}>
          <BaseIcon name='feather-box' />
          <BaseTitle mr='0 0 0 10px' bold H4 primary>
            Huỷ chuyến không thu phí
          </BaseTitle>
        </WrapperText>
        {TextRow({
          textLeft: withEmpty('name', dataDepature),
          textRight: `${withNumber('price', dataDepature)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ',')} VND`,
        })}
        {TextRow({
          textLeft: 'Số lượng vé',
          textRight: `x ${withEmpty('quantity', data)}`,
        })}
        {TextRow({ textLeft: 'Phí dịch vụ', textRight: '0 VND' })}
        <WrapperText $border={true}>
          <BaseTitle bold H3 primary>
            {data.form_payment === 1
              ? 'Thanh toán online'
              : 'Thanh toán khi lên xe'}
          </BaseTitle>
          <BaseTitle bold H3 black>
            {(data.quantity * withNumber('price', dataDepature))
              .toString()
              .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}{' '}
            VND
          </BaseTitle>
        </WrapperText>
      </Column>
    );
  }, [data]);

  const _renderService = useCallback(() => {
    return (
      <Column $border={true} $mr={true}>
        <WrapperText $borderBottom={true}>
          <BaseTitle bold H3 primary>
            Chính sách hủy đặt xe
          </BaseTitle>
        </WrapperText>
        <BaseText>
          Bạn chỉ có thể huỷ chuyến trước 2 tiếng sau khoảng thời gian này.
          <b> {withEmpty('end_time', dataDepature)}</b>. Vui lòng liên hệ với
          chúng tôi sớm nhất để có thể giúp bạn huỷ vé. Hotline :{' '}
          <i>0989999999</i>
        </BaseText>
      </Column>
    );
  });

  const ItemStep1 = useCallback(() => {
    return (
      <WrapperContent>
        <CustomInput
          label='Họ và tên'
          name='name'
          value={withEmpty('name', data)}
          placehoder='Họ và tên'
          onChange={(value) => onHandleInput('name', value)}
          leftIcon={<BaseIcon size={18} name='feather-user' />}
          require
        />
        <CustomInput
          label='Email'
          name='email'
          value={withEmpty('email', data)}
          placehoder='Email'
          onChange={(value) => onHandleInput('email', value)}
          leftIcon={<BaseIcon size={18} name='feather-mail' />}
          require
        />
        <CustomInput
          label='Số điện thoại'
          name='phone'
          placehoder='Số điện thoại'
          value={withEmpty('phone', data)}
          onChange={(value) => {
            onHandleInput('phone', value);
            if (withEmpty('phone', data) !== value)
              setOtpValue({ ...otpValue, status: true });
          }}
          leftIcon={<BaseIcon size={18} name='feather-smartphone' />}
          require
        />
        <CustomInput
          label='Ghi chú'
          name='note'
          placehoder='Ghi chú'
          value={withEmpty('note', data)}
          onChange={(value) => onHandleInput('note', value)}
          leftIcon={<BaseIcon size={18} name='feather-file-text' />}
        />
        <BaseTitle H4 bold error>
          Lưu ý: Vui lòng điền thông tin chính xác . Số điện thoại và email sẽ
          được nhà xe liên hệ để xác nhận lại vé !
        </BaseTitle>
      </WrapperContent>
    );
  }, [data]);

  const ItemStep2 = useCallback(() => {
    return (
      <>
        <WrapperContentModal>
          <FormOTP
            value={withEmpty('otp', otpValue)}
            handleChangeOTP={(_value) => {
              setOtpValue({ ...otpValue, otp: _value });
            }}
            handleVerifyOTP={onVerifyOTP}
          />
        </WrapperContentModal>
      </>
    );
  }, [step, data, otpValue]);

  const ItemStep3 = useCallback(() => {
    return (
      <>
        <WrapperContentModal $flex={true} $row={true}>
          <Column>
            <BaseImage source={IMAGES.TRAVEL} width={'100%'} />
            <BaseTitle H4 black mr={'5px 0'}>
              <b>Có thể bạn đã biết: </b>
              Khuyến khích khách hàng thanh toán online để giúp giảm thời gian
              thủ tục khi lên xe !
            </BaseTitle>
          </Column>
          <Column>
            <WrapperPayment>
              <BaseTitle H4 bold black>
                Chọn vé:
              </BaseTitle>
              <WrapperText $justifyStart>
                <SelectHour
                  cleanable={false}
                  data={dataSeat}
                  defaultValue={withEmpty('quantity', data)}
                  onChange={(value) => onHandleInput('quantity', value)}
                />
              </WrapperText>
              <WrapperText $justifyStart>
                <BaseTitle H4 bold black>
                  Giá vé:{' '}
                  {withNumber('price', dataDepature)
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}{' '}
                  VND
                </BaseTitle>
              </WrapperText>
              <BaseTitle H4 bold black>
                Tổng tiền:{' '}
                {(data.quantity * withNumber('price', dataDepature))
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                VND
              </BaseTitle>
              <BaseTitle H4 error bold mr={'10px 0 0 0 '}>
                Lưu ý !
              </BaseTitle>

              <BaseTitle H4 black mr={'5px 0'}>
                ● Trẻ em dưới 3 tuổi sẽ không mất vé!
              </BaseTitle>
              <BaseTitle H4 black mr={'5px 0'}>
                ● Khi đặt <b> từ 3 vé </b> trở lên bạn sẽ bắt buộc phải thanh
                toán online!
              </BaseTitle>
            </WrapperPayment>
          </Column>
        </WrapperContentModal>
      </>
    );
  }, [dataSeat, data]);

  const ItemStep4 = useCallback(() => {
    return (
      <WrapperContentModal $height={step === 3 ? true : false}>
        <WrapperText $column={true}>
          <BaseTitle bold H3 primary>
            Chọn hình thức thanh toán
          </BaseTitle>
          <SelectGroup
            name='form_payment'
            onChange={(value) => {
              onHandleInput('form_payment', value);
            }}
            value={withEmpty('form_payment', data)}
          >
            <SelectOptionPayment
              name='form_payment'
              label={'Thanh toán khi lên xe'}
              value={0}
              disabled={data?.quantity >= 3 ? true : false}
            />
            <SelectOptionPayment
              name='form_payment'
              label={'Thanh toán online'}
              value={1}
            />
            {data.form_payment === 1 && (
              <SelectHour
                cleanable={false}
                data={listBank}
                defaultValue={withEmpty('bank_code', data)}
                onChange={(value) => onHandleInput('bank_code', value)}
                renderMenuItem={(label, item) => {
                  return (
                    <WrapperText>
                      <BaseTitle H4 black>
                        {label}
                      </BaseTitle>
                      <Image source={item.logo} />
                    </WrapperText>
                  );
                }}
              />
            )}
          </SelectGroup>
        </WrapperText>
        {_renderInfoBooking()}
        {_renderInfoCustomBooking()}
        {_renderService()}
      </WrapperContentModal>
    );
  }, [data, listBank]);

  const ItemStepContent = useCallback(() => {
    switch (step) {
      case 0:
        return ItemStep1();
      case 1:
        return ItemStep2();
      case 2:
        return ItemStep3();
      case 3:
        return ItemStep4();
    }
  }, [data, step, otpValue, dataSeat]);

  const footerModal = useCallback(() => {
    return (
      <ButtonGroup>
        {step !== 0 && (
          <Button
            secondary={true}
            onClick={() => {
              if (!otpValue.status) {
                if (step === 2) {
                  setStep(0);
                  return;
                }
              }
              setStep(step - 1);
            }}
            $mr={true}
          >
            Prev
          </Button>
        )}
        {step === 3 ? (
          <Button
            primary={true}
            $mr={true}
            type='submit'
            onClick={() => {
              handleSubmit();
            }}
          >
            Submit
          </Button>
        ) : (
          <Button
            primary={true}
            $mr={true}
            type='submit'
            disabled={step === 1 && otpValue.status ? true : false}
            onClick={() => {
              onSubmit();
            }}
          >
            Next
          </Button>
        )}
      </ButtonGroup>
    );
  }, [step, otpValue, data]);

  const bodyModal = useCallback(() => {
    return (
      <>
        {Step()}
        <Form
          onSubmit={onSubmit}
          formValue={data}
          model={formStep1Validate}
          name='form'
        >
          <WrapperBody>{ItemStepContent()}</WrapperBody>
          {footerModal()}
        </Form>
      </>
    );
  }, [data, step, otpValue, dataSeat]);

  const _renderQRCODE = useCallback(() => {
    const handleDowload = (e) => {
      const imgageData = canvas.toDataURL('image/png');
      // Now browser starts downloading it instead of just showing it
      const newData = imgageData.replace(
        /^data:image\/png/,
        'data:application/octet-stream'
      );

      const dowload = document.querySelector('#download');
      dowload.setAttribute('href', newData);
      dowload.setAttribute('download', 'qrcode.png');
    };
    return (
      <WrapperContent $center id='QRCODE'>
        <BaseTitle H4 bold primary>
          Mã vé của bạn là : <i>{invoiceCODE}</i>
        </BaseTitle>
        <BaseTitle H5 bold mr='10px 0' black>
          Bạn có thể lưu lại mã QR CODE này để đưa cho nhân viên kiểm tra thông
          tin vé xe của mình !
          <a onClick={(e) => handleDowload(e)} href='#' id='download'>
            Tại đây
          </a>
          {redrect?.length &&
            'Vui lòng tắt thông báo này để sang trang thanh toán'}
        </BaseTitle>
        <QRCode value={`${process.env.URL_QR_CODE}${invoiceCODE}`} />
      </WrapperContent>
    );
  }, [canvas, invoiceCODE, redrect]);

  const handleHideQRCODE = useCallback(() => {
    setVisbleQRCODE(false);
    if (redrect.length) {
      showWarning(
        'Bạn sẽ được chuyển hướng sang trang thanh toán ngay sau đây!'
      );
      setTimeout(() => {
        window.location.assign(redrect);
      }, 3000);
    }
  }, [redrect]);
  return (
    <>
      <div id='recaptcha-container'></div>
      <Wrapper
        header={<p>Đặt vé</p>}
        show={show}
        size={'md'}
        onHide={onHide}
        body={bodyModal()}
      />
      <Wrapper
        show={visbleQRCODE}
        size={'xs'}
        body={_renderQRCODE()}
        onHide={() => {
          handleHideQRCODE();
        }}
        backdrop={'static'}
        header={<p></p>}
      />
    </>
  );
};
ModalBooking.PropTypes = {
  show: PropTypes.bool,
  onHide: PropTypes.func,
  setToggleModal: PropTypes.func,
  dataDepature: PropTypes.any,
};
export default React.memo(ModalBooking);
