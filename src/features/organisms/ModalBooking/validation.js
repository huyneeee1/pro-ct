import { SchemaModel, StringType } from "schema-typed";

export const validation = SchemaModel({
  name: StringType().isRequired("Name is required"),
  phone: StringType()
    .isRequired("Phone is required")
    .addRule((value, data) => {
      return /^(84|0[3|5|7|8|9])+([0-9]{8})\b/.test(value);
    }, "Please enter the correct phone number"),
  email: StringType()
    .isEmail("Please enter the correct email")
    .isRequired("Email is required"),
});
export const formStep1Validate = SchemaModel({
  name: StringType().isRequired("Name is required"),
  phone: StringType()
    .isRequired("Phone is required")
    .addRule((value, data) => {
      return /^(84|0[3|5|7|8|9])+([0-9]{8})\b/.test(value);
    }, "Please enter the correct phone number"),
  email: StringType()
    .isEmail("Please enter the correct email")
    .isRequired("Email is required"),
});
