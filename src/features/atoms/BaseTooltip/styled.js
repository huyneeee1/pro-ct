import { Tooltip, Whisper } from 'rsuite'
import styled from 'styled-components'

export const TooltipWrapper = styled(Tooltip)``
export const WhisperWrapper = styled(Whisper)``
