import React from 'react'
import { WhisperWrapper, TooltipWrapper } from './styled'
import PropTypes from 'prop-types'

const BaseTooltip = ({
  placement = 'top',
  trigger = 'hover',
  title,
  children,
  ...others
}) => {
  return (
    <WhisperWrapper
      speaker={<TooltipWrapper>{title}</TooltipWrapper>}
      placement={placement}
      trigger={trigger}
      {...others}
    >
      {children}
    </WhisperWrapper>
  )
}

BaseTooltip.propTypes = {
  placement: PropTypes.string,
  trigger: PropTypes.string,
  title: PropTypes.string,
  children: PropTypes.node
}

export default React.memo(BaseTooltip)
