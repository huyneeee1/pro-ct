import React from 'react'
import PropTypes from 'prop-types'
import { Wrapper } from './styled'

const BaseButton = ({
  children,
  onClick,
  fluid,
  bold,
  blue,
  primary,
  secondary,
  tertiary,
  dashed,
  warning,
  ...others
}) => {
  return (
    <Wrapper
      onClick={onClick}
      $fluid={fluid}
      $bold={bold}
      $blue={blue}
      $primary={primary}
      $secondary={secondary}
      $tertiary={tertiary}
      $dashed={dashed}
      $warning={warning}
      {...others}
    >
      {children}
    </Wrapper>
  )
}

BaseButton.propTypes = {
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func,
  fluid: PropTypes.bool,
  bold: PropTypes.bool,
  blue: PropTypes.bool,
  primary: PropTypes.bool,
  secondary: PropTypes.bool,
  tertiary: PropTypes.bool,
  dashed: PropTypes.bool,
  warning: PropTypes.bool
}

export default React.memo(BaseButton)
