import styled from "styled-components";

export const Link = styled.a`
  cursor: pointer;
  font-weight: 500;
`;
