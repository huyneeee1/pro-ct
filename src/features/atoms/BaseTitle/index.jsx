import React from "react";
import { Wrapper } from "./styled";
import PropTypes from "prop-types";

const BaseTitle = ({
  children,
  bold,
  uppercase,
  light,
  primary,
  white,
  H1,
  H2,
  H3,
  H4,
  H5,
  H6,
  mr,
  tertiary,
  error,
  black,
  ...others
}) => {
  return (
    <Wrapper
      $bold={bold}
      $uppercase={uppercase}
      $light={light}
      $primary={primary}
      $tertiary={tertiary}
      $H1={H1}
      $H2={H2}
      $H3={H3}
      $H4={H4}
      $H5={H5}
      $H6={H6}
      $mr={mr}
      $error={error}
      $white={white}
      $black={black}
      {...others}
    >
      {children}
    </Wrapper>
  );
};

BaseTitle.propTypes = {
  children: PropTypes.node,
};

export default React.memo(BaseTitle);
