import styled, { css } from "styled-components";

export const Wrapper = styled.div`
  color: ${(props) => props.theme.colors.grey[0]};
  margin-vertical: 10px;
  ${(props) =>
    props.$bold &&
    `
    font-weight: bold;
    display:  inline-block;
    `};
  ${(props) =>
    props.$uppercase &&
    `
    text-transform: uppercase;
  `}
  ${(props) => props.$tertiary && `color: ${props.theme.colors.tertiary}`};
  ${(props) => props.$light && `color: ${props.theme.colors.secondary[3]}`};
  ${(props) => props.$primary && `color: ${props.theme.colors.primary}`};
  ${(props) => props.$error && `color: ${props.theme.colors.error}`}
  ${(props) => props.$white && `color: ${props.theme.colors.grey[0]}`};
  ${(props) => props.$black && `color: ${props.theme.colors.black[0]}`};
  ${(props) => props.$H1 && `font-size: 24px`};
  ${(props) => props.$H2 && `font-size: 20px`};
  ${(props) => props.$H3 && `font-size: 18px`};
  ${(props) => props.$H4 && `font-size: 16px`};
  ${(props) => props.$H5 && `font-size: 14px`};
  ${(props) => props.$H6 && `font-size: 12px`};
  ${(props) =>
    props.$mr &&
    css`
      margin: ${props.$mr};
    `}
`;
