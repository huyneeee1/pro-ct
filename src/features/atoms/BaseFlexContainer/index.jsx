import React from "react";
import PropTypes from "prop-types";
import { Wrapper } from './styled'
const BaseFlexContainer = ({ children, width , justifycontent, alignitems, ...others }) => {
  return <Wrapper $width={width} $justifycontent={justifycontent} $alignitems={alignitems} {...others} >{children}</Wrapper>;
};

BaseFlexContainer.propTypes = {
  children: PropTypes.any,
  width: PropTypes.number,
  alignitems: PropTypes.any,
};

export default BaseFlexContainer;
