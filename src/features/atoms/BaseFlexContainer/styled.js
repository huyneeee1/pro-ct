import styled, { css } from "styled-components";
export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  width:100%;
  ${(props) =>
    props.$width &&
    css`
      width: ${props.$width}px;
    `}
  ${(props) =>
    props.$justifycontent &&
    css`
      justify-content: ${props.$justifycontent};
    `}
  ${(props) =>
    props.$alignitems &&
    css`
      align-items: ${props.$alignitems};
    `}
`;
