import React from "react";
import { Wrapper } from "./styled";
const LoadingCar = () => {
  return (
    <Wrapper className="wrapper">
      <div className="car">
        <div className="music">
          <div className="note delay-1">♬</div>
          <div className="note delay-2">♪</div>
          <div className="note delay-3">♫</div>
        </div>
        <div className="decoration">
          <div className="doors">
            <div className="door"></div>
            <div className="door"></div>
            <div className="door cabin"></div>
          </div>
        </div>
        <div className="light"> </div>
        <div className="bumber"> </div>
        <div className="wheels-group">
          <div className="wheel"></div>
          <div className="wheel"></div>
        </div>
      </div>
    </Wrapper>
  );
};

export default LoadingCar;
