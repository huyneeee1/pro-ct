import styled from "styled-components";
export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  position: relative;
  &:after {
    content: "";
    height: 2px;
    width: 130%;
    background-color: #efeaea;
    position: absolute;
    bottom: -20px;
  }
  &:before {
    content: "";
    height: 2px;
    width: 10px;
    background-color: #e9633b;
    position: absolute;
    bottom: -20px;
    animation: road-run 0.5s ease-in infinite;
    z-index: 2;
  }
  .bumber {
    position: absolute;
    bottom: 0;
    width: 110%;
    display: flex;
    justify-content: space-between;
  }
  .bumber:after {
    content: "";
    display: block;
    width: 12px;
    height: 8px;
    background-color: #efeaea;
    border-radius: 10px 10px 10px 0;
  }
  .bumber:before {
    content: "";
    display: block;
    width: 12px;
    height: 8px;
    background-color: #efeaea;
    border-radius: 10px 5px 0px 10px;
  }

  .car {
    animation: run 0.5s ease-in infinite;
    background: #e9633b;
    border-radius: 30px 30px 8px 8px;
    width: 120px;
    height: 75px;
    position: relative;
    display: flex;
    justify-content: center;
    transform: skewX(-5deg);
  }
  .car:before {
    height: 0;
  }
  .car:after {
    content: "";
    position: absolute;
    bottom: 30%;
    right: 30%;
    height: 2px;
    width: 10px;
    display: block;
    background-color: #ece8d9;
  }
  .car .music {
    position: absolute;
    top: 0;
    right: 0;
    z-index: 3;
  }
  .car .music .note {
    color: white;
    animation: music 3s linear infinite;
    position: absolute;
  }
  .car .music .note.delay-1 {
    animation-delay: 1s;
  }
  .car .music .note.delay-2 {
    animation-delay: 2s;
  }
  .car .music .note.delay-3 {
    animation-delay: 3s;
  }
  .car .decoration {
    width: 100%;
    height: 60%;
    position: absolute;
    background-color: #edf9fc;
    border-radius: 16px 20px 0px 0;
    display: flex;
    justify-content: flex-end;
    align-items: center;
    overflow: hidden;
  }
  .car .decoration .doors {
    height: 60%;
    width: 96%;
    display: flex;
    justify-content: space-around;
    position: relative;
    top: 6px;
  }
  .car .decoration .doors .door {
    display: block;
    width: 50%;
    height: 100%;
    position: relative;
    background-color: #8aa6ff;
    border-radius: 8px 8px 4px 4px;
    margin: 0 2px;
    overflow: hidden;
    display: flex;
    justify-content: flex-end;
  }
  .car .decoration .doors .door:after {
    content: "";
    display: block;
    width: 25%;
    height: 100%;
    background: rgba(255, 255, 255, 0.5);
    margin-left: 2px;
  }
  .car .decoration .doors .door:before {
    content: "";
    display: block;
    width: 4%;
    height: 100%;
    background: rgba(255, 255, 255, 0.5);
  }
  .car .decoration .doors .door.cabin {
    border-radius: 8px 0 0 4px;
    margin-right: 0;
    width: 100px;
  }
  .car .light {
    width: 4px;
    height: 8px;
    border-radius: 100px;
    background: white;
    position: absolute;
    right: 2px;
    bottom: 20px;
  }
  .car .wheels-group {
    display: flex;
    justify-content: space-around;
    position: absolute;
    width: 100%;
    bottom: 0;
  }
  .car .wheels-group .wheel {
    width: 40px;
    height: 20px;
    background-color: #000000;
    animation: wheel-scale 0.5s ease-in infinite;
    display: flex;
    justify-content: center;
    align-items: flex-end;
    border-top-left-radius: 100px;
    border-top-right-radius: 100px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
  }
  .car .wheels-group .wheel:before {
    content: "";
    position: relative;
    display: block;
    width: 32px;
    height: 32px;
    background-color: #45315d;
    border-radius: 100px;
    top: 80%;
  }
  .car .wheels-group .wheel:after {
    width: 22px;
    height: 22px;
    border-radius: 100px;
    background-color: #edf9fc;
    content: "";
    position: absolute;
    top: 48%;
    border: 6px solid #312046;
    box-sizing: border-box;
  }

  .mountains {
    position: absolute;
    width: 100%;
    z-index: 1;
    bottom: -30px;
    display: flex;
    justify-content: space-around;
  }
  .mountains .mountain {
    background: #8aa6ff;
    animation: mountain-run 10s ease-in infinite;
    position: relative;
    right: 9999px;
    transform: rotate(45deg);
    width: 50px;
    height: 50px;
  }
  @keyframes run {
    0% {
      top: 0px;
    }
    50% {
      top: 4px;
    }
    100% {
      top: 0;
    }
  }
  @keyframes wheel-scale {
    0% {
      transform: scaleX(0.7);
    }
    50% {
      transform: scaleX(0.9);
      height: 20px;
    }
    100% {
      transform: scaleX(0.7);
    }
  }
  @keyframes mountain-run {
    0% {
      right: 0%;
    }
    100% {
      right: 100%;
    }
  }
  @keyframes road-run {
    0% {
      right: 0%;
    }
    100% {
      right: 100%;
    }
  }
  @keyframes music {
    0% {
      font-size: 0.5rem;
      opacity: 0;
    }
    50% {
      opacity: 1;
      margin-top: -50px;
    }
    100% {
      margin-top: -50px;
      margin-left: -100px;
      font-size: 1rem;
      opacity: 0;
    }
  }
`;
