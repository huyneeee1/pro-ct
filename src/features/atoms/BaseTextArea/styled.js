import styled from "styled-components";
import { Input } from "rsuite";

export const Wrapper = styled.div`
  margin: 20px 0;
`;
export const Label = styled.p`
  font-weight: 500 !important;
  display: flex !important;
  color: ${(props) => props.theme.colors.secondary[1]};
  font-size: 1rem;
`;
export const WrapperTextArea = styled(Input)`
  margin: 20px auto;
  background: #ffffff;

  border: 1px solid ${(props) => props.theme.colors.secondary[5]};
  box-sizing: border-box;
  border-radius: 8px;
`;
