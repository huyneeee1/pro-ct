import { LoadingCar } from "atoms";
import React from "react";
import { LoadingWrapper } from "./styled";
const Loading = () => {
  return (
    <>
      <LoadingWrapper>
        <LoadingCar />
      </LoadingWrapper>
    </>
  );
};

export default React.memo(Loading);
