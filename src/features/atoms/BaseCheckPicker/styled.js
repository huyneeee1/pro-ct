import { CheckPicker } from 'rsuite'
import styled from 'styled-components'

export const Wrapper = styled(CheckPicker)`
  & a {
    ${({ $isError }) =>
      $isError && `border: 1px solid rgb(235, 87, 87) !important`};
    border: 1px solid #e0e0e0;
    box-sizing: border-box;
    border-radius: 8px;
    min-width: 156px;
  }
`

export const HelpText = styled.span`
  color: rgb(235, 87, 87);
`
