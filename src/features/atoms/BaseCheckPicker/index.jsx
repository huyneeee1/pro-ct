import React from 'react'
import { Wrapper, HelpText } from './styled'
import PropTypes from 'prop-types'

const BaseCheckPicker = ({ data, isError, helpText, ...others }) => {
  return (
    <>
      <Wrapper data={data} $isError={isError} {...others}></Wrapper>
      {isError && <HelpText>{helpText}</HelpText>}
    </>
  )
}

BaseCheckPicker.propTypes = {
  data: PropTypes.any,
  isError: PropTypes.boolean,
  helpText: PropTypes.string
}

export default React.memo(BaseCheckPicker)
