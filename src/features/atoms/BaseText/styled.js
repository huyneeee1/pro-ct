import styled from "styled-components";

export const Wrapper = styled.div`
  color: ${(props) => props.theme.colors.black[0]};
  font-size:15px;
  ${(props) =>
    props.$error &&
    `
    color: ${props.theme.colors.error};
    `};
  ${(props) =>
    props.$bold &&
    `
    color: ${props.theme.colors.black[0]};
    font-weight: bold;
    `};
  ${(props) =>
    props.$white &&
    `
    color: ${props.theme.colors.grey[0]};
    `};
  ${(props) =>
    props.$link &&
    `
    color: ${props.theme.colors.blue};
    cursor: pointer;
    `};
  ${(props) =>
    props.$uppercase &&
    `
    text-transform: uppercase;
  `}
  ${(props) => props.$tertiary && `color: ${props.theme.colors.tertiary}`};
`;
