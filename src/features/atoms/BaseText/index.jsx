import React from "react";
import { Wrapper } from "./styled";
import PropTypes from "prop-types";

const BaseText = ({
  children,
  bold,
  link,
  uppercase,
  tertiary,
  white,
  error,
  ...others
}) => {
  return (
    <Wrapper
      $error={error}
      $bold={bold}
      $tertiary={tertiary}
      $link={link}
      $uppercase={uppercase}
      $white={white}
      {...others}
    >
      {children}
    </Wrapper>
  );
};

BaseText.propTypes = {
  children: PropTypes.node,
  bold: PropTypes.bool,
  tertiary: PropTypes.any,
  link: PropTypes.any,
  uppercase: PropTypes.bool,
  white: PropTypes.bool,
};

export default React.memo(BaseText);
