import styled, { css } from "styled-components";
import { Animation } from "rsuite";
import { BaseTitle } from "atoms";
export const Wrapper = styled.div`
  height: auto;
  background-color: ${(props) => props.theme.colors.white};
  border-radius: 10px;
  display: flex;
  padding: 20px 15px;
  flex-direction: column;
  margin: 5px 0px;
`;
export const Title = styled(BaseTitle)`
  color: ${(props) => props.theme.colors.primary};
`;
export const WrapperHead = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
`;
export const WrapperContent = styled(Animation.Collapse)``;
export const WrapperDrop = styled.div`
  padding-top: 10px;
`;
export const Text = styled.p`
color: ${(props) => props.theme.colors.black[3]}
`
