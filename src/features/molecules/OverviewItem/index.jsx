import React, { useState } from "react";
import PropTypes from "prop-types";
import {
  Wrapper,
  Title,
  WrapperHead,
  WrapperContent,
  WrapperDrop,
  Text,
} from "./styled";
import { BaseIcon, BaseWrapper, BaseText } from "atoms";
import { Animation } from "rsuite";
const OverviewItem = ({ title, content, ...others }) => {
  const [toggle, setToggle] = useState(false);
  const Panel = React.forwardRef(({ style, ...props }, ref) => (
    <WrapperDrop {...props} ref={ref}>
      <Text>{content}</Text>
    </WrapperDrop>
  ));
  return (
    <Wrapper>
      <WrapperHead onClick={() => setToggle(!toggle)}>
        <Title H2={true} bold>
          {title}
        </Title>
        <BaseIcon
          name={!toggle ? "feather-chevron-down" : "feather-chevron-up"}
        />
      </WrapperHead>
      <WrapperContent in={toggle}>
        {(props, ref) => <Panel {...props} ref={ref} />}
      </WrapperContent>
    </Wrapper>
  );
};

OverviewItem.propTypes = {
  title: PropTypes.string,
  content: PropTypes.string,
};

export default OverviewItem;
