import styled from "styled-components";
import { BaseImage } from "atoms";
export const Wrapper = styled.div`
  margin-top: 10px;
`;
export const Image = styled(BaseImage)`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  margin: 0 5px;
`;
