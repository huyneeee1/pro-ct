import React from "react";
import { Wrapper, Image } from "./styled";
import { IMAGES } from "assets";
const ActionSocial = () => {
  return (
    <Wrapper>
      <Image source={IMAGES.FACEBOOK} />
      <Image source={IMAGES.WHATSAPP} />
      <Image source={IMAGES.INSTAGRAM} />
      <Image source={IMAGES.TELEGRAM} />
    </Wrapper>
  );
};

export default ActionSocial;
