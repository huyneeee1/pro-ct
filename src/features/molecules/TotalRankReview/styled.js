import styled from "styled-components";
import { BaseFlexContainer, BaseTitle, BaseText } from "atoms";
import { Progress } from "rsuite";
export const Wrapper = styled(BaseFlexContainer)`
  flex-direction: row;
  width: 60%;
`;
export const Column = styled.div`
  width: 60%;
  display: flex;
  flex-direction: column;
  &:nth-child(2) {
    width: 40%;
    align-items: center;
  }
`;
export const ProgessReview = styled(Progress.Line)`
  flex: 10;
`;
export const Title = styled(BaseTitle)`
  font-size: 40px;
`;
export const Text = styled(BaseText)``;
export const Row = styled.div`
  display: flex;
  align-items: center;
`;
export const Col = styled.div`
  min-width: 10px;
  flex: 1;
`;
