import React from "react";
import { BaseTitle } from "atoms";
import {
  Wrapper,
  Column,
  ProgessReview,
  Title,
  Text,
  Row,
  Col,
} from "./styled";
const TotalRankReview = () => {
  const _renderRowProgess = (total, percent) => {
    return (
      <Row>
        <Col>
          <BaseTitle H4 black bold>
            {total}
          </BaseTitle>
        </Col>
        <ProgessReview percent={percent} status="active" showInfo={false} />
      </Row>
    );
  };
  return (
    <Wrapper>
      <Column>
        {_renderRowProgess(8, 45)}
        {_renderRowProgess(4, 70)}
        {_renderRowProgess(12, 38)}
      </Column>
      <Column>
        <Title black bold>
          4,5
        </Title>
        <Text>192 Đánh giá</Text>
      </Column>
    </Wrapper>
  );
};

export default TotalRankReview;
