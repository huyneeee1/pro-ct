import React from "react";
import PropTypes from "prop-types";
import { Wrapper, Icon, Divider, Title } from "./styled";
const ServiceWrapper = ({ name, icon }) => {
  return (
    <Wrapper>
      <Icon name={icon} />
      <Divider />
      <Title>{name}</Title>
    </Wrapper>
  );
};

ServiceWrapper.propTypes = {
  name: PropTypes.string,
  icon: PropTypes.string,
};

export default ServiceWrapper;
