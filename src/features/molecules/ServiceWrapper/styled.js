import styled from "styled-components";
import { BaseIcon, BaseFlexContainer, BaseTitle } from "atoms";
export const Wrapper = styled(BaseFlexContainer)`
  width: 120px;
  height: 140px;
  border-radius: 10px;
  box-sizing: border-box;
  padding: 0 10px;
  background: ${(props) => props.theme.colors.bluePastel};
  box-shadow: 0px 5px 8px ${(props) => props.theme.colors.grey[0]};
  color: ${(props) => props.theme.colors.grey[0]};
  margin: 0 0.7rem;
  &:hover {
    box-shadow: 0px 5px 10px ${(props) => props.theme.colors.bluePastel};
  }
`;
export const Icon = styled(BaseIcon)`
  width: 50px;
  height: 50px;
`;
export const Divider = styled.div`
  width: 100%;
  border-bottom: solid;
  margin: 10px 0px;
`;
export const Title = styled(BaseTitle)`
  color: ${(props) => props.theme.colors.grey[0]};
  font-weight: bolder;
  font-size: 14px;
  text-align: center;
`;
