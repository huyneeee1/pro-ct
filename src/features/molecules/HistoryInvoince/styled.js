import styled, { keyframes, css } from "styled-components";
import { BaseIcon, BaseTitle, BaseImage } from "atoms";
export const History = styled.div`
  position: fixed;
  bottom: 10px;
  right: 5px;
  z-index: 9;
`;
const bounce2 = keyframes`
{
    0%,
    20%,
    50%,
    80%,
    100% {
      transform: translateY(0);
    }
    40% {
      transform: translateY(-50px);
    }
    60% {
      transform: translateY(-15px);
    }
  }
`;
export const Wrapper = styled.div`
  height: 230px;
  width: 400px;
  margin-left: auto;
  margin-right: auto;
  overflow: hidden;
  position: relative;
  border-radius: 10px;
  padding: 10px;
  ${(props) =>
    props.$bunce &&
    css`
      animation: ${bounce2} 2s ease infinite;
    `}
`;
export const Circle = styled.div`
  border-radius: 50%;
  height: 50px;
  width: 50px;
  position: absolute;
  top: 86%;
  left: 86%;
  transform: translate(-50%, -50%);
`;
export const Icon = styled(BaseIcon)`
  margin-right: 3px;
  ${(props) =>
    props.$status &&
    `
    color: ${props.theme.colors.tertiary}
  `}
  ${(props) =>
    props.$error &&
    `
    color: ${props.theme.colors.error}
  `}
    ${(props) =>
    props.$yellow &&
    `
    color: ${props.theme.colors.yellowPastel}
  `}
`;
export const WrapperText = styled.div`
  display: flex;
  align-items: center;
  margin: 10px 0;
`;
export const Span = styled.span`
  margin: 0 1px;
`;
export const Title = styled(BaseTitle)`
  cursor: pointer;
  &:hover {
    color: ${(props) => props.theme.colors.bluePastel};
  }
`;
export const Image = styled(BaseImage)`
  width: 100%;
  transform: scale(2);
`;
export const Text = styled.p`
color: ${(props) => props.theme.colors.error};
`
