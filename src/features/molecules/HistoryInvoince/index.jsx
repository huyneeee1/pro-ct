import { IMAGES } from "assets";
import { BaseText } from "atoms";
import React, { useCallback, useState } from "react";
import { useHistory } from "react-router-dom";
import { Badge } from "rsuite";
import { useTheme } from "styled-components";
import { Routers } from "utils";
import {
  Circle,
  History,
  Icon,
  Image,
  Span,
  Title,
  Wrapper,
  WrapperText,
  Text,
} from "./styled";
import PropTypes from "prop-types";
const HistoryInvoince = ({ data }) => {
  const history = useHistory();
  const theme = useTheme();
  const [bunce, setBunce] = useState(true);
  const [animated, setAnimated] = useState(false);
  const handelClick = useCallback(() => {
    setBunce(!bunce);
    const circle = document.querySelector(".circle");
    // The 'animated' bool serves as a toggle \\
    if (!animated) {
      Velocity(
        circle,
        {
          width: "100%",
          height: "100%",
          backgroundColor: theme.colors.bgBooking,
          borderRadius: 0,
          padding: "10px 15px",
        },
        250,
        [0.4, 0.0, 0.2, 1]
      );
      // Animate top and left properties with different easings \\
      // to create an arc-like movement path                    \\
      Velocity(
        circle,
        {
          top: "50%",
        },
        {
          duration: 250,
          easing: "easeInSine",
          queue: false,
        }
      );
      Velocity(
        circle,
        {
          left: "50%",
        },
        {
          duration: 250,
          easing: "easeOutSine",
          queue: false,
        }
      );
      setAnimated(!animated);
    } else {
      Velocity(
        circle,
        {
          width: "50px",
          height: "50px",
          borderRadius: 50,
        },
        250,
        [0.4, 0.0, 0.2, 1]
      );
      Velocity(
        circle,
        {
          top: "86%",
        },
        {
          duration: 250,
          easing: "easeOutSine",
          queue: false,
        }
      );
      Velocity(
        circle,
        {
          left: "86%",
        },
        {
          duration: 250,
          easing: "easeInSine",
          queue: false,
        }
      );
      setAnimated(!animated);
    }
  }, [animated, bunce]);
  return (
    <History>
      <Wrapper className="container" $bunce={bunce}>
        <Circle
          onClick={() => {
            handelClick();
          }}
          className="circle"
        >
          {!animated && (
            <Badge content={1}>
              <Image source={IMAGES.LEMOUNSINE} />
            </Badge>
          )}

          {animated && (
            <>
              <Title
                bold
                H3
                error
                onClick={() => {
                  history.push(
                    `${Routers.CHECK_INVOICE.URL}?search=${data?.invoice_code}`
                  );
                }}
              >
                {data?.departure?.name}
                <Span />
                (Mã chuyến: {data?.invoice_code}) - {data?.quantity} vé
              </Title>
              <WrapperText>
                <Icon name="feather-disc" size={20} $yellow />
                <BaseText>
                  Điểm đón:
                  <b>{data?.go_point}</b>
                </BaseText>
              </WrapperText>
              <WrapperText>
                <Icon name="feather-map-pin" size={20} $yellow />
                <BaseText>
                  Điểm trả:
                  <b>{data?.come_point}</b>
                </BaseText>
              </WrapperText>
              <WrapperText>
                <Icon name="feather-clock" size={20} $yellow />
                <BaseText>
                  Thời gian xuất phát: <b>{data?.departure?.start_time}</b>
                </BaseText>
              </WrapperText>
              <WrapperText>
                <Text>
                  <span style={{ fontWeight: "bold" }}>*Lưu ý:</span> Bạn chú ý
                  thu xếp ra điểm đón trước 20 - 30 phút để chuyến xe có thể đến
                  điểm dừng đúng giờ, không làm ảnh hưởng đến mọi người trên xe!
                </Text>
              </WrapperText>
            </>
          )}
        </Circle>
      </Wrapper>
    </History>
  );
};

HistoryInvoince.propTypes = {
  data: PropTypes.any,
};

export default HistoryInvoince;
