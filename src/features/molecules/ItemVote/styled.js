import styled from "styled-components";
import { BaseFlexContainer, BaseText, BaseImage, BaseTitle } from "atoms";
import { Rate } from "rsuite";
export const Wrapper = styled(BaseFlexContainer)`
  margin: 6px 0;
  background: ${(props) => props.theme.colors.secondary[5]};
  border-radius: 10px;
  padding: 10px;
`;
export const Title = styled(BaseTitle)``;
export const Content = styled(BaseText)`
  display: flex;
`;
export const WrapperText = styled.div`
  width: 100%;
  display: flex;
  align-content: center;
  justify-content: space-between;
  align-items: center;
  gap: 15px;
  @media (max-width: 768px) {
    align-items: flex-start;
    flex-direction: column;
  }
  .rs-rate-md .rs-rate-character {
    width: 18px !important;
    height: 14px !important;
  }
  .rs-rate-character {
    padding: 0 !important;
  }
`;
export const BaseRate = styled(Rate)`
  display: flex;
  padding: 5px 10px;
`;
export const Avatar = styled(BaseImage)`
  width: 50px;
  // height:50px
  border-radius: 50%;
  @media (max-width: 768px) {
    width: 35px;
    // height:35px;
  }
`;
export const Col = styled.div``;
export const Row = styled.div`
  display: flex;
  gap: 10px;
`;
export const ColText = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 5px;
`;
