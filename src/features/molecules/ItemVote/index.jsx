import React from "react";
import PropTypes from "prop-types";
import {
  Wrapper,
  WrapperText,
  Title,
  Content,
  BaseRate,
  Avatar,
  Col,
  Row,
  ColText,
} from "./styled";
import FrownIcon from "@rsuite/icons/legacy/FrownO";
import MehIcon from "@rsuite/icons/legacy/MehO";
import SmileIcon from "@rsuite/icons/legacy/SmileO";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
const ItemVote = ({ content, star, avatar, userName, date }) => {
  dayjs.extend(relativeTime);
  const renderCharacter = (value, index) => {
    if (value < index + 1) {
      return <MehIcon style={{ fontSize: 18, width: 18, height: 18 }} />;
    }
    if (value < 3) {
      return (
        <FrownIcon
          style={{ color: "#99A9BF", fontSize: 18, width: 18, height: 18 }}
        />
      );
    }
    if (value < 4) {
      return (
        <MehIcon
          style={{ color: "#fda082", fontSize: 18, width: 18, height: 18 }}
        />
      );
    }
    return (
      <SmileIcon
        style={{ color: "#e9633b", fontSize: 18, width: 18, height: 18 }}
      />
    );
  };
  return (
    <Wrapper alignitems="flex-start">
      <WrapperText>
        <Col>
          <Row>
            <Avatar source={`${process.env.BASE_URL_IMAGE}${avatar}`} circle />
            <ColText>
              <Title H4 bold black>
                {userName}
              </Title>
              <Title H6 light bold>
                {dayjs(date).format("MMMM D, YYYY h:mm A")}
              </Title>
            </ColText>
          </Row>
        </Col>
        <Col>
          <BaseRate
            readOnly
            defaultValue={star}
            renderCharacter={renderCharacter}
          />
        </Col>
      </WrapperText>
      <Content>{content}</Content>
    </Wrapper>
  );
};

ItemVote.propTypes = {
  content: PropTypes.string,
  star: PropTypes.number,
  userName: PropTypes.string,
  avatar: PropTypes.string,
  date: PropTypes.string,
};

export default ItemVote;
