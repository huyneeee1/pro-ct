import styled, { css } from "styled-components";
import { BaseIcon } from "atoms";
import { Input, InputGroup } from "rsuite";

export const WrapperInputGroup = styled(InputGroup)`
  width: 300px !important;
  ${(props) =>
    props.border &&
    css`
      border: ${props.border};
    `}
`;
export const WrapperInput = styled(Input)`
  background-color: ${(props) => props.theme.colors.secondary[6]};
`;
export const Button = styled(InputGroup.Button)``;
export const Icon = styled(BaseIcon)``;
