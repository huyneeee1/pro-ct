import React from "react";
import PropTypes from "prop-types";
import { WrapperInputGroup, WrapperInput, Button, Icon } from "./styled";
const SearchInput = ({ props, placeholder, border }) => {
  return (
    <WrapperInputGroup {...props} inside border={border}>
      <WrapperInput placeholder={placeholder} />
      <Button>
        <Icon name="feather-search" />
      </Button>
    </WrapperInputGroup>
  );
};

SearchInput.propTypes = {
  props: PropTypes.any,
  placeholder: PropTypes.string
};

export default SearchInput;
