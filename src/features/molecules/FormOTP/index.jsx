import React, { useState, useEffect, useCallback } from "react";
import {
  Image,
  WrapperRowInput,
  OTPInput,
  SendVerify,
  Icon,
  Title,
  WrapperOTP,
} from "./styled";
import { IMAGES } from "assets";

const FormOTP = ({ value, handleChangeOTP, handleVerifyOTP }) => {
  const [val, setVal] = useState();
  const handleChange = useCallback(
    (e) => {
      setVal(e);
      handleChangeOTP(e);
    },
    [value]
  );
  useEffect(() => {
    setVal(value);
  }, [value]);
  return (
    <WrapperOTP>
      <Image source={IMAGES.VERIRYPHONE} />
      <WrapperRowInput $center={true}>
        <Title H4 bold black>
          Chúng tôi sẽ gửi tới mã OTP về điện thoại của bạn. Vui lòng kiểm tra
          điện thoại !
        </Title>
      </WrapperRowInput>
      <OTPInput
        shouldAutoFocus={true}
        value={val}
        onChange={(_value) => {
          handleChange(_value);
        }}
        numInputs={6}
        inputStyle={{
          width: "40px",
          height: "40px",
          borderRadius: "10px",
          border: "1.5px solid gray",
          margin: "0 5px",
          color: "black",
        }}
      />
      <SendVerify
        circle
        icon={<Icon name="feather-arrow-right" size={24} />}
        disabled={val?.length === 6 ? false : true}
        onClick={() => {
          handleVerifyOTP();
        }}
      />
    </WrapperOTP>
  );
};

export default FormOTP;
