import { BaseImage, BaseIcon, BaseTitle, BaseFlexContainer } from "atoms";
import OtpInput from "react-otp-input";
import { IconButton } from "rsuite";
import styled, { css } from "styled-components";
export const SendVerify = styled(IconButton)`
  display: flex;
  background: ${(props) => props.theme.colors.primary};
  color: ${(props) => props.theme.colors.white};
`;
export const OTPInput = styled(OtpInput)``;
export const Image = styled(BaseImage)`
  width: 200px;
  height: 80px;
`;
export const WrapperRowInput = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  & > .rs-form-group {
    width: 48%;
  }
  ${(props) =>
    props.$center &&
    css`
      text-align: center;
      justify-content: center;
    `}
`;
export const Icon = styled(BaseIcon)``;
export const Title = styled(BaseTitle)`
  margin: 10px 0;
`;
export const WrapperOTP = styled(BaseFlexContainer)`
  min-height: 300px;
  justify-content: space-evenly;
`;
