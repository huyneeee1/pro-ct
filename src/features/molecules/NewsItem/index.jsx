import { BaseTitle } from "atoms";
import { withEmpty } from "exp-value";
import React from "react";
import { useHistory } from "react-router-dom";
import { Routers } from "utils";
import { Column, Image, Link, Text, Wrapper, WrapperText } from "./styled";
const NewsItem = ({ data }) => {
  const history = useHistory();

  return (
    <Wrapper>
      <Column $width>
        <Image
          source={`${process.env.BASE_URL_IMAGE}${withEmpty("image", data)}`}
        />
      </Column>
      <Column>
        <WrapperText>
          <BaseTitle H2 bold black>
            {withEmpty("name", data)}
          </BaseTitle>
          <Text>{withEmpty("short_content", data)}</Text>
          <Link
            onClick={() => {
              history.push({
                pathname: Routers.NEWS.CHILD[0].URL,
                search: `?${data?.slug}`,
              });
            }}
          >
            Xem thêm
          </Link>
        </WrapperText>
      </Column>
    </Wrapper>
  );
};

NewsItem.propTypes = {};

export default NewsItem;
