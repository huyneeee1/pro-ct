import styled, { css } from "styled-components";
import {
  BaseFlexContainer,
  BaseWrapper,
  BaseImage,
  BaseText,
  BaseLink,
} from "atoms";
export const Wrapper = styled(BaseFlexContainer)`
  flex-direction: row;
  height: 200px;
  margin: 15px 0;
  padding: 10px;
  border-radius: 10px;
  background: ${(props) => props.theme.colors.bgBooking};
`;

export const Column = styled.div`
  display: flex;
  width: 65%;
  height: 100%;
  border-radius: 10px;
  overflow: hidden;
  ${(props) =>
    props.$width &&
    css`
      width: 35%;
    `}
`;
export const Image = styled(BaseImage)`
  width: 100%;
  height: 100%;
`;
export const Text = styled(BaseText)`
  height: 60%;
  overflow: hidden;
  padding: 10px 0;
`;
export const WrapperText = styled.div`
  padding: 0 0 10px 30px;
`;
export const Link = styled(BaseLink)``;
