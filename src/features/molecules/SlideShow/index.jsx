import React from 'react'
import { WrapperCarousel } from './styled'
import { BaseImage } from 'atoms'
import { IMAGES } from 'assets'
const SlideShow = () => {
    return (
        <WrapperCarousel autoplay>
            <BaseImage source={IMAGES.SLIDE1}  />
            <BaseImage source={IMAGES.SLIDE2} />
            <BaseImage source={IMAGES.SLIDE3} />
        </WrapperCarousel>
    )
}

export default SlideShow
