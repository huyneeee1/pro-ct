import styled from "styled-components";
import { Carousel } from "rsuite";
export const WrapperCarousel = styled(Carousel)`
  height: 500px;
`;
