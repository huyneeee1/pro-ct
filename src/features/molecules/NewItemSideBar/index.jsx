import React from "react";
import { useHistory } from "react-router-dom";
import { Column, Image, Link, Text, Wrapper } from "./styled";
import { Routers } from "utils";
const NewItemSideBar = ({ data }) => {
  const history = useHistory();

  return (
    <Wrapper>
      <Column $left={true}>
        <Image source={`${process.env.BASE_URL_IMAGE}${data?.image}`} />
      </Column>
      <Column>
        <Text>{data?.name}</Text>
        <Link
          onClick={() => {
            history.push({
              pathname: Routers.NEWS.CHILD[0].URL,
              search: `?${data?.slug}`,
            });
          }}
        >
          Xem thêm
        </Link>
      </Column>
    </Wrapper>
  );
};

NewItemSideBar.propTypes = {};

export default React.memo(NewItemSideBar);
