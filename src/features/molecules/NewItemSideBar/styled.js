import styled, { css } from "styled-components";
import { BaseFlexContainer, BaseImage, BaseText, BaseLink } from "atoms";
export const Wrapper = styled(BaseFlexContainer)`
  flex-direction: row;
  align-items: flex-start;
  margin: 10px 0;
`;
export const Column = styled.div`
  display: flex;
  width: 75%;
  padding: 8px 5px 8px 8px;
  line-height: 13px;
  word-break: break-all;
  flex-direction: column;
  ${(props) =>
    props.$left &&
    css`
      width: 35%;
      padding: 0;
    `};
`;
export const Image = styled(BaseImage)`
  width: 100%;
`;
export const Text = styled(BaseText)`
  margin-left: 3px;
`;
export const Link = styled(BaseLink)`
  font-size: 13px;
`;
