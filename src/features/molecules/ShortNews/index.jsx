import { IMAGES } from "assets";
import React from "react";
import { Routers } from "utils";
import {
  Wrapper,
  WapperContent,
  Content,
  ImgContent,
  Img,
  TitleContent,
  Button,
  TextContent,
} from "./styled";
const ShortNews = () => {
  return (
    <Wrapper>
      <WapperContent>
        <WapperContent>
          <ImgContent>
            <Img src={IMAGES.XELI} />
          </ImgContent>
          <Content>
            <TitleContent>
              Nhà xe Quang Huy - xe đi Hưng Yên uy tín, chất lượng
            </TitleContent>
            <TextContent>
              Đã được thành lập từ năm 2021, với mong muốn nâng cao hơn nữa chất
              lượng dịch vụ vận chuyển hành khách. Nhằm đem đến cho khách hàng
              những dịch vụ vận chuyển thuận tiện với những giá trị tối ưu nhất.
              Nhà xe Quang Huy Limousine được hành khách tin cậy và đánh giá rất
              cao. Tại Hà Nội và Hưng Yên nhà xe sẽ đón khách tận nơi và trả
              khách tại một số tỉnh ...
            </TextContent>
          </Content>
        </WapperContent>
        <WapperContent>
          <ImgContent>
            <Img src={IMAGES.XEDEMO} />
          </ImgContent>
          <Content>
            <TitleContent>
              Nhà xe Quang Huy - xe 9 chỗ cao cấp, hiện đại. Bạn nên lựa chọn
            </TitleContent>
            <TextContent>
              Quang Huy Limousine trang bị nhiều loại xe, trong đó có xe 9 chỗ
              ngồi, khoang hành khách có 4 ghế VIP và 3 ghế tiêu chuẩn. 4 ghế
              VIP có thể xoay lại phía sau để dễ dàng thảo luận với bạn đồng
              hành. Mỗi chiếc ghế đều được bọc da sang trọng, tựa như ghế hảng
              thương gia trên máy may. Có trang bị dây an toàn, thiết kế chống
              sốc và cơ chế bật ra sau cho ...
            </TextContent>
          </Content>
        </WapperContent>
      </WapperContent>
      <Button to={Routers.NAV[3].URL}>Xem thêm</Button>
    </Wrapper>
  );
};

export default ShortNews;
