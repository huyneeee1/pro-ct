import { NavLink } from "react-router-dom";
import styled from "styled-components";
export const Wrapper = styled.div`
  height: auto;
  width: 80%;
  display: flex;
  flex-direction: column;
  cursor: pointer;
  margin: 50px 0 35px;
  @media screen and (max-width: 768px) {
    width: 90%;
  }
`;
export const Title = styled.div`
  color: ${(props) => props.theme.colors.black[0]};
  font-size: 32px;
  font-weight: bold;
  border-bottom: 1px solid ${(props) => props.theme.colors.black[4]};
  padding-bottom: 5px;
  margin-bottom: 20px;
`;
export const WapperContent = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  gap: 20px;
  @media screen and (max-width: 768px) {
    flex-direction: column;
  }
`
export const Content = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`
export const TitleContent = styled.p`
  color: ${(props) => props.theme.colors.primary};
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 10px;
`;
export const ImgContent = styled.div`
  width: 100%;
`
export const Img = styled.img`
  width: 100%;
  border-radius: 20px;
  height: 100%;
`
export const TextContent = styled.p`
color: ${(props) => props.theme.colors.black[4]};
font-size: 13px;  
margin: 0;
`
export const Button = styled(NavLink)`
  border: none;
  display: flex;
  padding: 0.5rem 2.5rem;
  justify-content: center;
  font-size: 18px;
  margin: 1.2rem auto;
  background: ${(props) => props.theme.colors.orangePastel[1]};
  border-radius: 12px;
  color: #fff;
  cursor: pointer;
  &: hover, &: active, &: focus {
    color: #F1F3F4;
    background: ${(props) => props.theme.colors.orangePastel[0]};
  }
`