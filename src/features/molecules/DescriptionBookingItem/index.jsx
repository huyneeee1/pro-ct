import { BaseTitle, BaseLink } from "atoms";
import { withEmpty } from "exp-value";
import { ItemVote, PointStation, Slide, TotalRankReview } from "molecules";
import React, { useCallback, useState, useEffect } from "react";
import { Constant } from "utils";
import {
  CateCar,
  Column,
  ColumnWrapper,
  ContenTab,
  ImageItem,
  Tab,
  TabItem,
  WrapperContentTab,
  WrapperProgess,
  WrapperTopPoint,
  WrapperVote,
  WrappperText,
} from "./styled";
import { commentDepAPI } from "config/api";
const DescriptionBookingItem = ({
  dataImage,
  dataPolicies,
  dataDriver,
  idDeparture,
  dataDepature,
  toggle,
}) => {
  const [tab, setTab] = useState(0);
  const ImageTab = () => {
    return <Slide data={dataImage} toggle={toggle} />;
  };
  const [comments, setComments] = useState([]);
  const [search, setSearch] = useState({
    hasMore: false,
    page: 1,
  });
  const getComments = useCallback(
    (_page) => {
      async function execute() {
        try {
          const { data: response } = await commentDepAPI.getAll(
            idDeparture,
            _page
          );
          if (response) {
            setComments(response.data);
            setSearch({
              hasMore: response.next_page_url ? true : false,
              page: search.page + 1,
            });
            if (search.page > 1) {
              setComments([...comments, ...response.data]);
              return;
            }
          }
        } catch (error) {
          console.log("error");
        }
      }
      execute();
    },
    [search, comments]
  );
  const Utilities = () => {
    return (
      <WrapperContentTab direction="column">
        {dataPolicies &&
          dataPolicies.map((ele, index) => {
            return (
              <PointStation key={index} conscious={ele.detail} hasIcon={true} />
            );
          })}
      </WrapperContentTab>
    );
  };
  const Policy = () => {
    return (
      <WrapperContentTab direction="column">
        {Constant.policies.map((item) => {
          return (
            <PointStation
              conscious={item.label}
              district={item.content}
              hasIcon={true}
            />
          );
        })}
      </WrapperContentTab>
    );
  };
  const TopPoint = () => {
    return (
      <WrapperContentTab direction="column">
        <WrapperTopPoint>
          <BaseTitle H3={true} bold={true} black>
            Lưu ý
          </BaseTitle>
          <CateCar>
            Quý khách vui lòng hãy đến điểm đón gần nhất để bắt xe.
          </CateCar>
        </WrapperTopPoint>
        <WrapperTopPoint $row={true}>
          <Column>
            <BaseTitle H3={true} bold={true} mr="10px 0" black>
              Điểm đón
            </BaseTitle>
            <PointStation
              conscious={withEmpty("go_location_city", dataDepature)}
              district={`${withEmpty(
                "go_location_district",
                dataDepature
              )} - ${withEmpty("go_location_wards", dataDepature)}`}
            />
          </Column>
          <Column>
            <BaseTitle H3={true} bold={true} mr="10px 0" black={true}>
              Điểm trả
            </BaseTitle>
            <PointStation
              conscious={withEmpty("come_location_city", dataDepature)}
              district={`${withEmpty(
                "come_location_district",
                dataDepature
              )} - ${withEmpty("come_location_wards", dataDepature)}`}
            />
          </Column>
        </WrapperTopPoint>
      </WrapperContentTab>
    );
  };
  const VoteTab = useCallback(() => {
    return (
      <WrapperVote>
        {/* <TotalRankReview /> */}
        {comments?.length ? (
          comments.map((item, index) => {
            return (
              <ItemVote
                key={index}
                content={item.content}
                star={item.star}
                avatar={item?.customer?.image}
                userName={`${item?.customer?.first_name} ${item?.customer?.last_name}`}
                date={item?.created_at}
              />
            );
          })
        ) : (
          <span>Chuyến xe chưa có bình luận !</span>
        )}
        {search.hasMore && (
          <BaseLink
            onClick={() => {
              getComments(search.page);
            }}
          >
            Xem thêm
          </BaseLink>
        )}
      </WrapperVote>
    );
  }, [comments, search]);
  const CarUserInfo = () => {
    return (
      <>
        <WrapperVote $column={true} $cardUser>
          <ColumnWrapper>
            <ImageItem
              source={`${process.env.BASE_URL_IMAGE}${withEmpty(
                "image",
                dataDriver
              )}`}
            />
            <WrappperText>
              <BaseTitle H3 bold black>
                Tài xế: {withEmpty("name", dataDriver)}
              </BaseTitle>
              <BaseTitle black>
                📍
                {withEmpty("address", dataDriver)}
              </BaseTitle>
            </WrappperText>
          </ColumnWrapper>
          <ColumnWrapper>
            <WrappperText $width={true}>
              <BaseTitle bold H5 mr="0 10px" black>
                {withEmpty("experience", dataDriver)} năm kinh nghiệm
              </BaseTitle>
              <WrapperProgess.Line
                percent={100}
                status="active"
                showInfo={false}
              />
            </WrappperText>
          </ColumnWrapper>
        </WrapperVote>
        <WrappperText>{withEmpty("description", dataDriver)}</WrappperText>
        <WrappperText $row={true}>
          <BaseTitle bold H4 black>
            Số điện thoại:
          </BaseTitle>
          <BaseTitle bold H4 black>
            {withEmpty("phone", dataDriver)}
          </BaseTitle>
        </WrappperText>
        <WrappperText $row={true} black>
          <BaseTitle bold H4 black>
            Email:
          </BaseTitle>
          <BaseTitle bold H4 black>
            {withEmpty("email", dataDriver)}
          </BaseTitle>
        </WrappperText>
      </>
    );
  };
  const ContentTab = useCallback(() => {
    switch (tab) {
      case 0:
        return ImageTab();
      case 1:
        return CarUserInfo();
      case 2:
        return Utilities();
      case 3:
        return TopPoint();
      case 4:
        return Policy();
      case 5:
        return VoteTab();
      default:
        break;
    }
  }, [tab, comments, toggle, search]);
  useEffect(() => {
    if (tab === 5) {
      getComments(1);
    }
  }, [tab]);
  return (
    <>
      <Tab>
        {Constant.DESCRIPTION.map((item, index) => {
          return (
            <TabItem
              actived={tab === index ? true : false}
              key={index}
              onClick={() => setTab(index)}
            >
              {item.title}
            </TabItem>
          );
        })}
      </Tab>
      <ContenTab>{ContentTab()}</ContenTab>
    </>
  );
};

export default DescriptionBookingItem;
