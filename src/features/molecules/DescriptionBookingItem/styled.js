import styled, { css } from "styled-components";
import { BaseImage, BaseText, BaseInput, BaseFlexContainer } from "atoms";
import { Progress } from "rsuite";
export const WrapperDropDown = styled.div`
  max-height: 450px;
  padding-top: 20px;
`;

export const Image = styled(BaseImage)``;

export const Tab = styled.div`
  min-height: 60px;
  width: 100%;
  display: flex;
  justify-content: center;
  border-bottom: 1px solid ${(props) => props.theme.colors.secondary[4]};
`;
export const TabItem = styled.div`
  height: 100%;
  padding: 20px 10px;
  text-align: center;
  cursor: pointer;
  font-weight: bold;
  ${(props) =>
    props.actived &&
    css`
      border-bottom: 2px solid ${(props) => props.theme.colors.primary};
      color: ${(props) => props.theme.colors.primary};
    `}
`;
export const ContenTab = styled.div`
  max-height: 380px;
  overflow: scroll;
  overflow-x: scroll;
  &::-webkit-scrollbar {
    width: 0;
  }
  scrollbar-width: none;
`;
export const WrapperContentTab = styled.div`
  height: 100%;
  width: 100%;
  padding: 20px 0px;
  display: flex;
  flex-wrap: wrap;
  align-content: flex-start;
  ${(props) =>
    props.justify &&
    css`
      justify-content: ${props.justify};
    `}
  ${(props) =>
    props.direction &&
    css`
      flex-direction: ${props.direction};
    `}
`;
export const ItemUtilities = styled.div`
  border: 1px solid gray;
  width: 100%;
  height: 100px;
  display: flex;
  overflow: hidden;
  justify-content: center;
`;
export const ImageItem = styled(BaseImage)`
  display: flex;
  border-radius: 50%;
  width: 120px;
  height: 120px;
  margin-right: 15px;
  margin-bottom: 5px;
  ${(props) =>
    props.$small &&
    css`
      width: 60px;
      height: 50px;
      @media (max-width: 768px) {
        width: 50px;
        height: 40px;
        margin-right: 0;
      }
    `}
`;
export const WrapperTopPoint = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  ${(props) =>
    props.$row &&
    css`
      flex-direction: row;
    `}
`;
export const Column = styled.div`
  flex: 1;
  height: auto;
`;
export const WrapperGrid = styled.div`
  margin: 10px 0;
  display: grid;
  grid-template-columns: auto auto auto auto auto;
`;
export const CateCar = styled(BaseText)`
  margin-top: 1rem;
`;
export const WrapperVote = styled(BaseFlexContainer)`
  padding: 10px 0;
  ${(props) =>
    props.$column &&
    css`
      flex-direction: row;
      justify-content: space-between;
      align-items: center;
    `}
  ${(props) =>
    props.$cardUser &&
    css`
      @media (max-width: 768px) {
        flex-direction: column;
      }
    `}
  
  ${(props) =>
    props.$justify &&
    css`
      justify-content: ${props.$justify};
    `}
`;
export const WrappperText = styled.div`
  ${(props) =>
    props.$width &&
    css`
      width: 200px;
      @media (max-width: 768px) {
        width: 100%;
      }
    `}
  ${(props) =>
    props.$row &&
    css`
      display: flex;
      margin: 5px 0;
    `}
`;
export const WrapperProgess = styled(Progress)`
  width: 40px;
`;
export const ColumnWrapper = styled.div`
  display: flex;
  align-items: center;
  ${(props) =>
    props.$width &&
    css`
      width: 100%;
      flex-direction: column;
    `}
  @media (max-width: 768px) {
    flex-direction: column;
    width: 100%;
  }
`;
export const ContentVote = styled(BaseInput)`
  display: flex;
  width: 100%;
  border: none;
  border-radius: 0;
  margin-top: 5px;
  border-bottom: 1px solid gray;
  &:focus {
    box-shadow: none !important;
  }
  background: unset;
`;
export const WrapperRate = styled.div`
  display: flex;
  align-content: center;
  justify-content: center;
  flex-direction: column;
  width: 100%;
  margin-left: 5px;
  .rs-rate-md .rs-rate-character {
    width: 28px !important;
    height: 24px !important;
    @media (max-width: 768px) {
      margin: 0 !important;
    }
  }
  .rs-rate-character {
    padding: 0 !important;
  }
  .rs-icon {
    @media (max-width: 768px) {
      width: 25px !important;
      height: 25px !important;
    }
  }
`;
