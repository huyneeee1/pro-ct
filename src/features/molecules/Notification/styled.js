import { BaseBadge, BaseText, BaseTitle } from "atoms";
import styled from "styled-components";
export const Notification = styled(BaseBadge)`
  cursor: pointer;
`;
export const WrapperItemNoti = styled.div`
  width: 300px;
  border-radius: 4px;
  padding: 8px 10px;
  ${(props) =>
    props.$new &&
    `
    background: ${props.theme.colors.kemPastel[2]};
  
  `}
  position: relative;
  margin: 3px 0;
  cursor: pointer;
`;
export const Time = styled(BaseText)`
  font-size: 10px;
  color: ${(props) =>
    props.$new ? props.theme.colors.black[1] : props.theme.colors.black[1]};
`;
export const Title = styled(BaseTitle)`
  color: ${(props) =>
    props.$new ? props.theme.colors.primary : props.theme.colors.black[5]};
`;
export const Content = styled(BaseText)`
  font-size: 13px;
  color: ${(props) =>
    props.$new ? props.theme.colors.black[1] : props.theme.colors.black[5]};
  font-weight: 500;
  white-space: nowrap;
  text-overflow: ellipsis;
  width: 100%;
  overflow: hidden;
  ${(props) =>
    props.$expand &&
    `
    white-space: normal;
  `}
`;
export const Dot = styled.div`
  width: 8px;
  height: 8px;
  border-radius: 50%;
  background: ${(props) => props.theme.colors.error};
  position: absolute;
  top: 8px;
  right: 10px;
`;
export const WrapperNotification = styled.div`
  display: flex;
  flex-direction: column;
  max-height: 300px;
  min-width: 300px;
  padding: 0 5px;
  overflow: scroll;
  position: absolute;
  right: 10%;
  z-index: 999;
  background: ${(props) => props.theme.colors.bgBooking};
  border-radius: 10px;
  overflow-x: hidden;
  &::-webkit-scrollbar {
    width: 0;
  }
  ${(props) =>
    props.$hidden &&
    `
    display:none;
  `}
  box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
`;
export const RelativeNotification = styled.div`
  position: relative;
`;
export const HeaderNotification = styled.div`
  width: 100%;
  top: 0;
  background: ${(props) => props.theme.colors.bgBooking};
  position: sticky;
  padding: 10px;
  z-index: 2;
  font-weight: 600;
  font-size: 15px;
`;
export const Loading = styled.div`
  text-align: center;
  padding: 5px 0;
  color: ${(props) => props.theme.colors.primary};
`;
