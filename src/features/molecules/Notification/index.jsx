import SpinnerIcon from "@rsuite/icons/legacy/Spinner";
import { BaseIcon } from "atoms";
import { notificationAPI } from "config/api";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import { withEmpty } from "exp-value";
import React, { useCallback, useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { useDispatch, useSelector } from "react-redux";
import {
  loadMoreNotification,
  updateNotification,
} from "reduxFolder/notificationSlice";
import { useTheme } from "styled-components";
import {
  Content,
  Dot,
  HeaderNotification,
  Loading,
  Notification,
  RelativeNotification,
  Time,
  Title,
  WrapperItemNoti,
  WrapperNotification,
} from "./styled";

const NotificationModule = () => {
  dayjs.extend(relativeTime);
  const theme = useTheme();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const notifications = useSelector((state) => state.notification);
  const [toggleNoti, setToggleNoti] = useState(true);
  const [expand, setExpand] = useState("");
  const [page, setPage] = useState(1);
  const [hasMore, setHasMore] = useState(true);
  const handeUpdateNoti = useCallback(async (id, status, noti) => {
    if (status === "0") {
      const _notiupdate = { ...noti, status: "1" };
      setTimeout(() => {
        dispatch(updateNotification(_notiupdate));
      }, 3000);
      let _body = new FormData();
      _body.append("status", "1");
      await notificationAPI.updateStatusNotification(id, _body);
    }
  });
  const getNotification = useCallback(
    async (_page) => {
      if (!user?.id || _page <= 1) return;
      try {
        const { data: response } = await notificationAPI.listNotification(
          user?.id,
          { page: _page }
        );
        if (response) {
          setHasMore(response.next_page_url ? true : false);
          dispatch(loadMoreNotification(response.data));
        }
      } catch (error) {}
    },
    [page, user, hasMore]
  );
  const _renderItemNoti = useCallback(
    (noti, index) => {
      return (
        <WrapperItemNoti
          key={index}
          onClick={() => {
            handeUpdateNoti(noti.id, noti.status, noti);
            if (expand === noti.id) {
              return setExpand("");
            }
            setExpand(noti.id);
          }}
          $new={noti?.status == 0 ? true : false}
        >
          <Time $new={noti?.status == 0 ? true : false}>
            {dayjs(withEmpty("created_at", noti))?.fromNow()}
          </Time>
          <Title black $new={noti?.status == 0 ? true : false}>
            {withEmpty("title", noti)}
          </Title>
          <Content
            $new={noti?.status == 0 ? true : false}
            $expand={noti.id === expand}
          >
            {withEmpty("content", noti)}
          </Content>
          {noti?.status == 0 && <Dot />}
        </WrapperItemNoti>
      );
    },
    [expand]
  );
  useEffect(() => {
    if (page > 1) {
      getNotification(page);
    }
  }, [page]);
  return (
    <RelativeNotification>
      <Notification
        content={notifications?.filter((ele) => ele.status == 0)?.length}
        onClick={() => setToggleNoti(!toggleNoti)}
      >
        <BaseIcon
          name="feather-bell"
          size={20}
          style={{ color: theme.colors.white }}
        />
      </Notification>
      <WrapperNotification $hidden={toggleNoti} id="scrollableDiv">
        <HeaderNotification>Thông báo</HeaderNotification>
        <InfiniteScroll
          dataLength={notifications?.length}
          next={() => {
            setTimeout(() => {
              setPage(page + 1);
            }, 3000);
          }}
          hasMore={hasMore}
          loader={
            user && (
              <Loading>
                <SpinnerIcon pulse style={{ fontSize: "1.4em" }} />
              </Loading>
            )
          }
          scrollThreshold={1}
          scrollableTarget="scrollableDiv"
        >
          {notifications.length ? (
            notifications.map((noti, index) => {
              return _renderItemNoti(noti, index);
            })
          ) : (
            <Title H5 black mr={"10px;"}>
              Bạn không có thông báo!
            </Title>
          )}
        </InfiniteScroll>
      </WrapperNotification>
    </RelativeNotification>
  );
};
export default React.memo(NotificationModule);
