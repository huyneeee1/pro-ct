import styled from "styled-components";
import { Placeholder } from "rsuite";
export const WrapperConent = styled.div`
  display: flex;
  height: 250px;
  width: 100%;
  .rs-placeholder-paragraph-graph-image {
    border-radius: 8px;
    height: 100%;
    width: 26%;
  }
  .rs-placeholder-paragraph {
    height: 100%;
  }
`;

export const WrappeMiddle = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  flex-direction: column;
`;
export const Wrapper = styled.div`
  margin-bottom: 20px;
  width: 100%;
  border-radius: 10px;
  padding: 15px;
  display: flex;
  flex-direction: column;
  height: auto;
  overflow: hidden;
  background-color: ${(props) => props.theme.colors.bgBooking};
`;
export const SkeletonItem = styled(Placeholder.Paragraph)``;
