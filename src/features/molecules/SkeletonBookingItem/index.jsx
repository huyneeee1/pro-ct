import React from "react";
import {
  WrapperConent,
  WrapperImage,
  WrapperRight,
  WrappeMiddle,
  Wrapper,
  SkeletonItem,
} from "./styled";
const SkeletonBookingItem = () => {
  return (
    <Wrapper>
      <WrapperConent>
        <WrappeMiddle>
          <SkeletonItem graph="image" active rows={8} rowMargin={20} />
        </WrappeMiddle>
      </WrapperConent>
    </Wrapper>
  );
};

export default React.memo(SkeletonBookingItem);
