import styled from "styled-components";
import { BaseImage, BaseFlexContainer } from "atoms";
import CursorZoom from "react-cursor-zoom";
export const WrapperDemoImage = styled.div`
  display: flex;
  width: 100%;
  height: 250px;
  margin: 10px 0;
  @media (max-width: 768px) {
    height: 150px;
  }
  justify-content: center;
`;
export const ImageItem = styled(BaseImage)`
  object-fit: cover;
  width: 100%;
  height: 100%;
`;
export const ImagePagination = styled(BaseImage)``;
export const WrapperPagination = styled.div`
  overflow: scroll;
  width: 100%;
  display: -webkit-inline-box;
`;
export const Wrapper = styled.div`
  padding: 0 5px;
  width: 150px;
  cursor: pointer;
`;
export const Container = styled(BaseFlexContainer)``;
export const ZoomImage = styled(CursorZoom)``;
