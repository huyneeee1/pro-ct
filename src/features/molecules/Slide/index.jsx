import React, { useState } from "react";
import {
  Container,
  ImageItem,
  Wrapper,
  WrapperDemoImage,
  WrapperPagination,
  ZoomImage,
} from "./styled";
const Slide = ({ data, toggle }) => {
  const [imageDemo, setImageDemo] = useState(
    `${process.env.BASE_URL_IMAGE}${data[0]?.image_path}`
  );

  return (
    <Container>
      <WrapperDemoImage>
        {toggle && (
          <ZoomImage
            image={{
              src: imageDemo,
              width: 800,
              height: 250,
            }}
            zoomImage={{
              src: imageDemo,
              width: 1600,
              height: 1200,
            }}
            cursorOffset={{ x: 80, y: -80 }}
          />
        )}
      </WrapperDemoImage>
      <WrapperPagination>
        {data &&
          data.map((item, index) => {
            return (
              <Wrapper
                key={index}
                onClick={() => {
                  setImageDemo(
                    `${process.env.BASE_URL_IMAGE}${item?.image_path}`
                  );
                }}
              >
                <ImageItem
                  source={`${process.env.BASE_URL_IMAGE}${item?.image_path}`}
                />
              </Wrapper>
            );
          })}
      </WrapperPagination>
    </Container>
  );
};

Slide.propTypes = {};

export default Slide;
