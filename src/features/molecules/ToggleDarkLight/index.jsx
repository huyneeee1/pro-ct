import React from "react";
import { WrapperToggle } from "./styled";
import { useDispatch, useSelector } from "react-redux";
import { changeTheme } from "reduxFolder/themeSlice";
const ToggleDarkLight = ({ display }) => {
  const dispatch = useDispatch();
  const theme = useSelector((state) => state.theme);
  return (
    <WrapperToggle $display={display}>
      <div className="wrapper">
        <input
          type="checkbox"
          name="checkbox"
          className="switch"
          onClick={() => {
            dispatch(changeTheme(!theme));
          }}
        />
      </div>
    </WrapperToggle>
  );
};

export default React.memo(ToggleDarkLight);
