import React, { useState, useCallback, useEffect } from "react";
import PropTypes from "prop-types";
import { PointStation } from "molecules";
import { WrapperRadio, Wrapper } from "./styled";
import { withEmpty } from "exp-value";
import { BaseRadio } from "atoms";
const CustomRadio = ({ data, onChange, value, type, ...others }) => {
  const [val, setVal] = useState();
  const handleChange = useCallback(
    (e) => {
      setVal(e);
      onChange(e);
    },
    [value]
  );
  useEffect(() => {
    setVal(value);
  }, [value]);
  return (
    <WrapperRadio value={val} onChange={handleChange} {...others}>
      {data.map((item, index) => {
        return (
          <Wrapper key={index}>
            <BaseRadio
              value={withEmpty("district", item)}
              label={`${type === "diemDon" ? "Điểm đón " : "Điểm trả"}${
                index + 1
              }`}
            />
            <PointStation
              conscious={withEmpty("conscious", item)}
              district={withEmpty("district", item)}
            />
          </Wrapper>
        );
      })}
    </WrapperRadio>
  );
};

CustomRadio.propTypes = {
  data: PropTypes.array,
  onChange: PropTypes.func,
  value: PropTypes.string,
  type: PropTypes.string,
  defaultValue: PropTypes.string,
};

export default React.memo(CustomRadio);
