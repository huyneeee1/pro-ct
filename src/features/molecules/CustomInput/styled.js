import styled from "styled-components";
import { Form, InputGroup } from "rsuite";
import { BaseText } from "atoms";

export const Wrapper = styled(Form.Group)``;

export const Label = styled(Form.ControlLabel)`
  font-weight: 500 !important;
  display: flex !important;
  color: ${(props) => props.theme.colors.black[0]};
  font-size: 1rem;
`;
export const Control = styled(Form.Control)`
  border-radius: 10px !important;
  height: max-content;
  align-item: center;
  // padding: 0px 10px;
  text-overflow: ellipsis; /* IE, Safari (WebKit) */
  overflow: visible;
  white-space: nowrap;
  outline: none;
  &.rs-input[disabled] {
    cursor: not-allowed;
    color: unset;
    background: unset;
  }
`;
export const Input = styled(InputGroup)`
  .rs-input-xs {
    height: 100%;
  }
  display: flex;
  align-items: center;
  border: 1px solid #e5e5ea;
  border-radius: 10px;
  outline: none;
  overflow: visible;
  width: 100% !important;
  overflow: visible;
`;

export const InputAddon = styled(InputGroup.Addon)`
  display: flex;
  align-items: center;
  width: max-content;
  margin: 0;
  border-radius: 0 10px 10px 0 !important;
  outline: none;
  ${(props) =>
    props.$left &&
    `
  border-radius: 10px 0 0 10px !important;
  
  `}
  background: ${(props) => props.theme.colors.grey[1]};
  color: ${(props) => props.theme.colors.black[0]};
  cursor: pointer;
`;
export const TextRequire = styled(BaseText)`
  color: ${(props) => props.theme.colors.error};
  margin-left: 10px;
`;
