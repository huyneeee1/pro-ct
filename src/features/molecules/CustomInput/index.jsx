import React from "react";
import {
  Wrapper,
  Label,
  Control,
  Input,
  InputAddon,
  TextRequire,
} from "./styled";
import PropTypes from "prop-types";

const CustomInput = ({
  label,
  bold,
  name,
  value,
  onChange,
  accepter,
  leftIcon,
  rightIcon,
  placeholder,
  require,
  nameGroup,
  styles,
  ...rest
}) => {
  return (
    <Wrapper className={nameGroup}>
      <Label className="label" bold={bold ? 1 : 0}>
        {label}
        {require ? <TextRequire>(*)</TextRequire> : null}
      </Label>
      <Input style={styles}>
        {leftIcon ? <InputAddon $left>{leftIcon}</InputAddon> : null}
        <Control
          name={name}
          value={value}
          onChange={onChange}
          accepter={accepter}
          placeholder={placeholder}
          {...rest}
        />
        {rightIcon ? (
          <InputAddon onClick={rightIcon.click}>{rightIcon.icon}</InputAddon>
        ) : null}
      </Input>
    </Wrapper>
  );
};

CustomInput.propTypes = {
  label: PropTypes.string,
  bold: PropTypes.bool,
  name: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  accepter: PropTypes.object,
  leftIcon: PropTypes.any,
  rightIcon: PropTypes.any,
  placeholder: PropTypes.string,
  require: PropTypes.bool,
  styles: PropTypes.object,
  nameGroup: PropTypes.string,
};

export default React.memo(CustomInput);
