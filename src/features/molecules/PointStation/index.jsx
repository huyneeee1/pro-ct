import React from "react";
import PropTypes from "prop-types";
import { Wrapper, Text, WrapperLi, WrapperText, Icon } from "./styled";
import { BaseTitle } from "atoms";
const PointStation = ({ conscious, district, hasIcon }) => {
  return (
    <Wrapper>
      <WrapperLi>
        <BaseTitle H4={true} black bold>
          {conscious}
        </BaseTitle>
        <WrapperText>
          {!hasIcon && <Icon name="feather-map-pin" size={14} />}
          <Text>{district}</Text>
        </WrapperText>
      </WrapperLi>
    </Wrapper>
  );
};

PointStation.propTypes = {
  conscious: PropTypes.string,
  district: PropTypes.string,
};

export default PointStation;
