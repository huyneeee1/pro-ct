import styled, { css } from "styled-components";
import { BaseText, BaseWrapper, BaseIcon } from "atoms";
export const Wrapper = styled.ul``;
export const Text = styled(BaseText)`
  color: ${(props) => props.theme.colors.black[3]};
  margin-left: 10px;
`;
export const WrapperLi = styled.li``;
export const WrapperText = styled(BaseWrapper)`
  align-items: center;
`;
export const Icon = styled(BaseIcon)`
  color: ${(props) => props.theme.colors.primary};
  font-weight: 500;
`;
