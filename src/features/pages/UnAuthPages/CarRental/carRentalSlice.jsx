import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import axios from "axios";

export const fetchUserById = createAsyncThunk(
    'carRental/fetchUserById',
    async (userId, thunkAPI) => {
        const { data } = await axios.get("https://hungpvph12160-pake-user.herokuapp.com/products")
        return data
    }
)

const carRendtal = createSlice({
    name: 'carRental',
    initialState: {
        list: [],
        loading: false,
    },
    reducers: {
        getAllCar: (state, action) => {
            return state = action.payload
        }
    },
    extraReducers: {
        [fetchUserById.pending]: (state, action) => {
            state.loading = true
        },

        [fetchUserById.fulfilled]: (state, action) => {
            state.list = action.payload;
            state.loading = false
        }
        ,
        [fetchUserById.rejected]: (state, action) => {
            console.log({ state, action })
        }
    }
})

const { reducer, actions } = carRendtal;
export const { getAllCar, } = actions;
export default reducer;