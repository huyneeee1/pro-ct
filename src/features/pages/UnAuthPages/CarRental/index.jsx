import { getAllCar } from './carRentalSlice'
import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { Wrapper } from './styled'
import { fetchUserById } from './carRentalSlice'
import { unwrapResult } from '@reduxjs/toolkit'
function CarRental() {
    const dispatch = useDispatch()
    useEffect(() => {
        (async () => {
            const user = await dispatch(fetchUserById())
            console.log(unwrapResult(user))
        })();
    }, [dispatch])
    return (
        <>
            <Wrapper>
                Car rental slice
            </Wrapper>
            <Wrapper background={true}>
                Car rental slice
            </Wrapper>
            <Wrapper>
                Car rental slice
            </Wrapper>
        </>
    )
}

export default CarRental
