import styled, { css } from 'styled-components'
import { BaseWrapper } from 'atoms'

export const Wrapper = styled.div`
    height:400px;
    width:300px;
    background:green;
    ${props => props.background && css`
        background:red;
    ` }
`