import { SchemaModel, StringType } from "schema-typed";
export const validationEmail = SchemaModel({
  email: StringType()
    .isEmail("Please enter the correct email")
    .isRequired("Email is required"),
  password: StringType().isRequired("Password is required"),
});
export const validationPhone = SchemaModel({
  phone_number: StringType()
    .isRequired("Phone is required")
    .addRule((value, data) => {
      return /^(84|0[3|5|7|8|9])+([0-9]{8})\b/.test(value);
    }, "Please enter the correct phone number"),
  password: StringType().isRequired("Password is required"),
});
