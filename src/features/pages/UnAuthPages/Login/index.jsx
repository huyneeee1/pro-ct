import { IMAGES } from 'assets';
import { BaseIcon } from 'atoms';
import { authAPI } from 'config/api';
import { withEmpty } from 'exp-value';
import { useAlert, useStorage } from 'hooks';
import { LayoutFormLogin } from 'organisms';
import React, { useCallback, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { setLoading } from 'reduxFolder/loadingSlice';
import { saveUser } from 'reduxFolder/userSlice';
import { Routers } from 'utils';
import {
  Button,
  ButtonGroup,
  Form,
  Image,
  InputGroup,
  Line,
  SocialMedia,
  TextLink,
  Title,
  WrapperCheckBox,
} from './styled';
import { validationEmail, validationPhone } from './validation';
const Login = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [toggle, setToggle] = useState(false);
  const [data, setData] = useState({
    email: '',
    password: '',
    phone_number: '',
  });
  const { showSuccess, showError } = useAlert();
  const { saveValue } = useStorage();
  const goToPage = useCallback((route) => {
    history.push(route);
  }, []);
  const [showPassword, setShowPassword] = useState(false);
  const [remember, setRemember] = useState(true);
  const handleInput = useCallback(
    (key, value) => {
      setData((prev) => {
        return {
          ...prev,
          [key]: value,
        };
      });
    },
    [data]
  );
  const handleFacebook = useCallback(() => {
    // signInWithPopup(auth, facebookProvider)
    //   .then((result) => {
    //     // The signed-in user info.
    //     const user = result.user;
    //     // This gives you a Facebook Access Token. You can use it to access the Facebook API.
    //     const credential = FacebookAuthProvider.credentialFromResult(result);
    //     const accessToken = credential.accessToken;
    //     console.log("user", user);
    //   })
    //   .catch((error) => {
    //     // Handle Errors here.
    //     const errorCode = error.code;
    //     const errorMessage = error.message;
    //     // The email of the user's account used.
    //     const email = error.email;
    //     // The AuthCredential type that was used.
    //     const credential = FacebookAuthProvider.credentialFromError(error);
    //   });
  });
  const handleGoogle = useCallback(() => {
    // signInWithPopup(auth, googleProvider)
    //   .then((result) => {
    //     // This gives you a Google Access Token. You can use it to access the Google API.
    //     const credential = GoogleAuthProvider.credentialFromResult(result);
    //     const token = credential.accessToken;
    //     // The signed-in user info.
    //     const { email, displayName, photoURL, phoneNumber, providerId } =
    //       result.user;
    //     // dispatch(
    //     //   createAccount({
    //     //     email: email,
    //     //     firstName: displayName,
    //     //     avatar: photoURL,
    //     //     phone: phoneNumber,
    //     //   })
    //     // );
    //   })
    //   .catch((error) => {
    //     // Handle Errors here.
    //     const errorCode = error.code;
    //     const errorMessage = error.message;
    //     // The email of the user's account used.
    //     const email = error.email;
    //     // The AuthCredential type that was used.
    //     const credential = GoogleAuthProvider.credentialFromError(error);
    //     // ...
    //   });
  });
  const validateFunc = toggle ? validationEmail : validationPhone;
  const handleSubmit = useCallback(() => {
    dispatch(setLoading(true));
    const { email, phone_number, password } = data;
    let formData = new FormData();
    formData.append(
      toggle ? 'email' : 'phone_number',
      toggle ? email : phone_number
    );
    formData.append('password', password);

    async function login() {
      try {
        const { data: _data } = await authAPI.login(formData);
        if (_data) {
          showSuccess(_data.message);
          dispatch(setLoading(false));
          dispatch(saveUser({ ..._data.data.customerlogin, remember }));
          saveValue('remember', remember, true);
          history.push(Routers.HOME);
          if (remember === true) {
            saveValue('token', _data.data.token, true);
            saveValue(
              'user',
              { ..._data.data.customerlogin, role: 'customer', remember },
              true
            );
            return;
          }
          saveValue('token', _data.data.token, false);
          saveValue(
            'user',
            { ..._data.data.customerlogin, role: 'customer', remember },
            false
          );
        }
      } catch (error) {
        showError(error.response.data?.message);
        dispatch(setLoading(false));
      }
    }
    login();
  }, [data, remember]);
  return (
    <LayoutFormLogin>
      <Form formValue={data} model={validateFunc} onSubmit={handleSubmit}>
        <Image source={IMAGES.LOGO_QH_PRIMARY} />
        <Title H1 primary bold>
          Đăng nhập
        </Title>
        <InputGroup
          leftIcon={
            <BaseIcon
              size={18}
              name={toggle ? 'feather-mail' : 'feather-phone'}
            />
          }
          label={toggle ? 'Email' : 'Điện thoại'}
          name={toggle ? 'email' : 'phone_number'}
          type={toggle ? 'text' : 'number'}
          placeholder={toggle ? 'Nhập email' : 'Nhập số điện thoại'}
          value={withEmpty(toggle ? 'email' : 'phone_number', data)}
          onChange={(value) =>
            handleInput(toggle ? 'email' : 'phone_number', value)
          }
          rightIcon={{
            click: () => setToggle(!toggle),
            icon: <>Thay đổi</>,
          }}
        />
        <InputGroup
          leftIcon={<BaseIcon size={18} name='feather-lock' />}
          label='Mật khẩu'
          placeholder='Nhập mật khẩu'
          type={showPassword ? 'text' : 'password'}
          name='password'
          value={withEmpty('password', data)}
          onChange={(value) => handleInput('password', value)}
          rightIcon={{
            click: () => setShowPassword(!showPassword),
            icon: (
              <BaseIcon
                size={18}
                name={showPassword ? 'feather-eye' : 'feather-eye-off'}
              />
            ),
          }}
        />
        <ButtonGroup justifycontent={'space-between'}>
          <WrapperCheckBox
            value={remember}
            checked={remember}
            onChange={() => {
              setRemember(!remember);
            }}
          >
            Nhớ mật khẩu
          </WrapperCheckBox>
          <TextLink onClick={() => goToPage(Routers.FORGOT)}>
            Quên mật khẩu?
          </TextLink>
        </ButtonGroup>
        <Button primary $mr={true} type='submit'>
          Đăng nhập
        </Button>
        <Button warning $mr={true} onClick={() => goToPage(Routers.REGISTER)}>
          Đăng ký
        </Button>

        <Line>hoặc</Line>
        <ButtonGroup>
          <SocialMedia circle src={IMAGES.FACEBOOK} onClick={handleFacebook} />
          <SocialMedia circle src={IMAGES.GOOGLE} onClick={handleGoogle} />
        </ButtonGroup>
      </Form>
    </LayoutFormLogin>
  );
};

export default Login;
