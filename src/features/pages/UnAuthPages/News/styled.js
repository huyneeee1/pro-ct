import { Divider } from "rsuite";
import styled from "styled-components";

export const Line = styled(Divider)`
  margin: 24px 20px;
`;

export const Wrapper = styled.div`
  display: flex;
  height: auto;
  width: 80%;
  margin: 20px 0px;
  @media (max-width: 768px) {
    flex-direction: column;
    width: 100%;
    align-items: center;
  }
`;
export const WrapperSideBar = styled.div`
  background-color: ${(props) => props.theme.colors.bgBooking};
  width: 300px;
  max-height: 600px;
  position: sticky;
  top: 20px;
  margin-right: 1.5%;
  border-radius: 10px;
  padding: 20px 15px;
  @media (max-width: 768px) {
    width: 90%;
    margin-top: 10px;
  }
`;
export const WrapperContent = styled.div`
  width: calc(100% - 300px);
  border-radius: 10px;
  margin-right: 20px;
  @media (max-width: 768px) {
    width: 90%;
    margin-right: 0px;
  }
`;
