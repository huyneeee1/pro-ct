import { newsAPI } from "config/api";
import { NewsItem } from "molecules";
import { SideBarNews } from "organisms";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { setLoading } from "reduxFolder/loadingSlice";
import { Wrapper, WrapperContent, WrapperSideBar } from "./styled";
const News = () => {
  const dispatch = useDispatch();
  const [data, setData] = useState([]);
  const [search, setSearch] = useState({
    hasMore: false,
    page: 1,
  });
  useEffect(() => {
    dispatch(setLoading(true));

    async function execute() {
      const { page } = search;
      try {
        const { data: response } = await newsAPI.getAll(page);
        if (response) {
          setData(response.data);
          dispatch(setLoading(false));
        }
      } catch (error) {
        dispatch(setLoading(false));
      }
    }
    execute();
  }, [search]);
  return (
    <Wrapper>
      <WrapperContent>
        {data?.length > 0 &&
          data.map((ele, index) => {
            return <NewsItem data={ele} key={index} />;
          })}
      </WrapperContent>
      <WrapperSideBar>
        <SideBarNews />
      </WrapperSideBar>
    </Wrapper>
  );
};

export default News;
