import styled from "styled-components";
import {
  BaseWrapper,
  BaseForm,
  BaseText,
  BaseImage,
  BaseInputPicker,
  BaseDatePicker,
  BaseButton,
} from "atoms";
import { CustomInput } from "molecules";
import { Divider, RangeSlider } from "rsuite";
export const Wrapper = styled.div`
  display: flex;
  height: auto;
  width: 90%;
  margin: 20px 0px;
  @media (max-width: 768px) {
    flex-direction: column;
    width: 90%;
  }
`;
export const WrapperSideBar = styled.div`
  background-color: ${(props) => props.theme.colors.bgBooking};
  width: 300px;
  max-height: 600px;
  position: sticky;
  top: 20px;
  margin-right: 1.5%;
  border-radius: 10px;
  padding: 20px 15px;
  @media (max-width: 768px) {
    width: 100%;
    position: inherit;
    margin: 20px 0;
  }
`;
export const WrapperContent = styled.div`
  width: calc(100% - 300px);
  border-radius: 10px;
  @media (max-width: 768px) {
    width: 100%;
  }
`;

export const InputSearch = styled(CustomInput)``;
export const FormSearch = styled(BaseForm)``;
export const Line = styled(Divider)``;
export const PriceRange = styled(RangeSlider)``;
export const WrapperText = styled(BaseWrapper)`
  justify-content: space-between;
  margin: 15px 0;
  ${(props) =>
    props.$center &&
    `
    justify-content: center;
    align-items: center;
    flex-direction: column;
  `}
`;
export const Text = styled(BaseText)`
  font-weight: bold;
`;
export const WrapperForm = styled.div`
  width: 100vw;
  height: 40vw;
  position: relative;
  overflow: hidden;
  box-shadow: 0 1px 8px rgba(0, 0, 0, 0.15);
  &:after {
    content: "";
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    opacity: 0.25;
    background-image: linear-gradient(180deg, #28d6e4, #04f44f);
    opacity: 0.8;
  }
`;
export const Image = styled(BaseImage)`
  width: 100%;
  height: 90%;
`;
export const Form = styled(BaseForm)`
  display: flex;
  width: fit-content;
  align-items: center;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;
export const Input = styled(BaseInputPicker)`
  height: 50px;
  width: 220px;
  // border: none;
  border-radius: 0;
  &:nth-child(1) {
    border-radius: 5px 0 0 5px;
    @media (max-width: 768px) {
      border-radius: 0;
    }
  }
  .rs-btn-default {
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  @media (max-width: 768px) {
    border-radius: 0;
  }
`;
export const WrapperFilterCar = styled.div`
  width: 100%;
  height: 600px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-image:url('https://cdn.anvui.vn/uploadv2/web/35/3567/slide/2019/04/03/04/05/1554264326_banner-viet-trung-1-01.jpg');
  flex-direction: column;
  background-size: cover;
  background-repeat: no-repeat;
  background-position-y: center;
  background-attachment: fixed;
  backdrop-filter: blur(1px);
`;
export const DataPicker = styled(BaseDatePicker)`
  height: 50px;
  width: 220px;
  .rs-btn-default {
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 0;
  }
`;
export const Button = styled(BaseButton)`
  border-radius: 0;
  height: 50px;
  width: 220px;
  border-radius: 0 5px 5px 0;
  @media (max-width: 768px) {
    border-radius: 0;
  }
`;
export const WrapperSearchCarNotFound = styled.div`
  height: 400px;
  background: ${(props) => props.theme.colors.bgBooking};
  border-radius: 10px;
  padding: 20px;
  text-align: center;
`;
export const WrapperFormStep1 = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  justify-content: center;
  align-items: center;
`;
export const WrapperRecentDepature = styled.div`
  width: 70%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-top: 10px;
  @media (max-width: 768px) {
    width: 95%;
  }
`;
