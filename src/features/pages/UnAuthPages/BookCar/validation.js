import { SchemaModel, StringType } from "schema-typed";
export const validation = SchemaModel({
  go_location_city: StringType().isRequired("go_location_city is required"),
  come_location_city: StringType().isRequired("come_location_city is required"),
  start_time: StringType().isRequired("start_time is required"),
});
