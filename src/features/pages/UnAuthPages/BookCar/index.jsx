import { BaseButton, BaseIcon, BaseTitle, BaseLink } from 'atoms';
import { departureAPI } from 'config/api';
import { SkeletonBookingItem } from 'molecules';
import { BookingItem } from 'organisms';
import React, { useEffect, useState, useCallback } from 'react';
import {
  FormSearch,
  InputSearch,
  Line,
  PriceRange,
  Text,
  Wrapper,
  WrapperContent,
  WrapperSideBar,
  WrapperText,
  Form,
  Input,
  WrapperFilterCar,
  DataPicker,
  Button,
  WrapperSearchCarNotFound,
  Image,
  WrapperFormStep1,
  WrapperRecentDepature,
} from './styled';
import dayjs from 'dayjs';
import { withEmpty } from 'exp-value';
import InfiniteScroll from 'react-infinite-scroll-component';
import { IMAGES } from 'assets';
import { Constant } from 'utils';
import { useAlert } from 'hooks';
const BookCar = (props) => {
  const { showError } = useAlert();
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([]);
  const [value, setValue] = useState([200000, 1000000]);
  const numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };
  const [step, setStep] = useState(true);
  const [hasMore, setHasMore] = useState(false);
  const [listBank, setListBank] = useState([]);
  const [search, setSearch] = useState({
    go_location_city: '',
    come_location_city: '',
    start_time: dayjs().format('YYYY-MM-DD'),
    page: 1,
    name: '',
    price_to: 200000,
    price_from: 1000000,
  });
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }, []);
  useEffect(() => {
    getData(search);
  }, [search.page]);
  useEffect(() => {
    fetch('https://api.vietqr.io/v2/banks')
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        const listData = data?.data.map((item) => {
          return {
            label: item.name,
            value: item.short_name,
            logo: item.logo,
          };
        });
        setListBank(listData);
      });
  }, []);
  const getData = async (params) => {
    setIsLoading(true);
    const { page } = params;
    try {
      const { data: response } = await departureAPI.getAll(params);
      setIsLoading(false);
      if (response) {
        setTimeout(() => {
          setHasMore(response?.next_page_url ? true : false);
        });
        if (page === 1) {
          setData(response.data);
          return;
        }
        setData([...data, ...response.data]);
      }
    } catch (error) {
      setIsLoading(false);
      showError(error.response.data.message);
    }
  };
  const handleSeach = useCallback(() => {
    const [price_form, price_to] = value;
    const _search = { ...search, price_form, price_to };
    setData([]);
    getData(_search);
  }, [search, value]);
  const _renderSideBar = () => {
    return (
      <WrapperSideBar>
        <FormSearch formValue={search} onSubmit={handleSeach}>
          <InputSearch
            label='Tìm kiếm'
            name='textSearch'
            value={withEmpty('name', search)}
            leftIcon={<BaseIcon size={18} name='feather-search' />}
            onChange={(_value) => {
              setSearch({ ...search, name: _value });
            }}
          />
          <Line />
          <BaseTitle H4 mr={'10px 0'}>
            Giá
          </BaseTitle>
          <PriceRange
            value={value}
            min={200000}
            max={1000000}
            step={10000}
            defaultValue={[200000, 500000]}
            progress
            onChange={(_value) => {
              setValue(_value);
            }}
            tooltip={true}
          />
          <WrapperText>
            <Text>{numberWithCommas(value[0])}VND</Text>
            <Text>{numberWithCommas(value[1])}VND</Text>
          </WrapperText>
          <BaseButton fluid primary={true} type='submit'>
            <BaseIcon size={18} name='feather-search' />
          </BaseButton>
        </FormSearch>
      </WrapperSideBar>
    );
  };
  const _renderDepature = useCallback(() => {
    return (
      <WrapperContent>
        <InfiniteScroll
          dataLength={data?.length}
          next={() => {
            setTimeout(() => {
              setSearch({ ...search, page: search?.page + 1 });
            }, 2000);
          }}
          hasMore={hasMore}
          loader={<SkeletonBookingItem />}
          scrollThreshold={0.8}
        >
          {isLoading ? (
            <>
              <SkeletonBookingItem />
              <SkeletonBookingItem />
            </>
          ) : data.length !== 0 ? (
            data.map((ele, index) => {
              return <BookingItem key={index} data={ele} listBank={listBank} />;
            })
          ) : (
            <WrapperSearchCarNotFound>
              <Image source={IMAGES.SEARCHCAR} />
              <Text>Không có dữ liệu. Vui lòng quay lại phần chọn chuyến</Text>
              <BaseLink
                onClick={() => {
                  setStep(true);
                }}
              >
                Tại đây
              </BaseLink>
            </WrapperSearchCarNotFound>
          )}
        </InfiniteScroll>
      </WrapperContent>
    );
  }, [data, isLoading, hasMore, search, listBank]);

  const handleInput = useCallback(
    (key, _value) => {
      setSearch((prev) => {
        return {
          ...prev,
          [key]: _value,
        };
      });
    },
    [search]
  );
  const handleSubmit = useCallback(async () => {
    if (!withEmpty('go_location_city', search)) {
      showError('Không được để trống điểm đến');
      return;
    }
    if (!withEmpty('come_location_city', search)) {
      showError('Không được để trống điểm đi');
      return;
    }
    getData(search);
    setStep(false);
  }, [search]);
  const _renderForm = useCallback(() => {
    return (
      <WrapperFormStep1>
        <WrapperFilterCar $img={IMAGES.GIOITHIEU1}>
          <BaseTitle H1 bold white style={{ textAlign: 'center' }}>
            ĐẶT XE DỄ DÀNG HƠN VỚI QUANG HUY LEMOUNSINE
          </BaseTitle>
          <Form onSubmit={handleSubmit}>
            <Input
              value={withEmpty('go_location_city', search)}
              name='go_location_city'
              placeholder={'Nhập nơi đi'}
              data={Constant.destination.filter(
                (ele) => ele.value !== withEmpty('come_location_city', search)
              )}
              valueKey={'value'}
              onChange={(_value) => {
                handleInput('go_location_city', _value);
              }}
            />
            <Input
              value={withEmpty('come_location_city', search)}
              name='come_location_city'
              placeholder={'Nhập nơi đến'}
              data={Constant.pointOfDeparture.filter(
                (ele) => ele.value !== withEmpty('go_location_city', search)
              )}
              onChange={(_value) => handleInput('come_location_city', _value)}
            />

            <DataPicker
              defaultValue={new Date()}
              name='start_time'
              style={{ width: 220 }}
              disabledDate={(date) =>
                !dayjs().isBefore(
                  dayjs(date).add(1, 'days').format('YYYY-MM-DD')
                )
              }
              onSelect={(_value) =>
                handleInput('start_time', dayjs(_value).format('YYYY-MM-DD'))
              }
            />
            <Button primary type='submit' onClick={() => handleSubmit()}>
              Tìm vé xe
            </Button>
          </Form>
        </WrapperFilterCar>
        <WrapperRecentDepature>
          {data?.length !== 0 &&
            data.map((ele, index) => {
              if (index < 3) {
                return (
                  <BookingItem key={index} data={ele} listBank={listBank} />
                );
              }
            })}
        </WrapperRecentDepature>
      </WrapperFormStep1>
    );
  }, [data, listBank, search]);
  return (
    <>
      {step ? (
        _renderForm()
      ) : (
        <Wrapper>
          {_renderSideBar()}
          {_renderDepature()}
        </Wrapper>
      )}
    </>
  );
};

export default React.memo(BookCar);
