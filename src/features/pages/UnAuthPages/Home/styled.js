import styled from "styled-components";
import { BaseTitle, BaseFlexContainer } from "atoms";
export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
export const Title = styled(BaseTitle)`
  font-size: 20px;
`;
export const WrapperService = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 10px 0px;
  position: absolute;
  bottom: 10px;
  right: 27%;
  @media (max-width: 768px) {
    display: none;
  }
`;
export const WrapperSlide = styled.div`
  position: relative;
  min-height: 600px;
  background: ${(props) => props.theme.colors.grey[2]};
`;
export const WrapperNew = styled(BaseFlexContainer)`

  background: ${(props) => props.theme.colors.grey[0]};
  `
export const WrapperOverService = styled(BaseFlexContainer)`
  background-image:url('https://cdn.dribbble.com/users/3392979/screenshots/6297144/90s-pattern-dribbble.gif');
`;
