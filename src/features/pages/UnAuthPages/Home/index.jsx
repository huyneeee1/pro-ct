import { ServiceWrapper, SlideShow, ShortNews } from "molecules";
import { OverviewService } from "organisms";
import React from "react";
import { Constant } from "utils";
import {
  Wrapper,
  WrapperOverService,
  WrapperService,
  WrapperSlide,
  WrapperNew,
} from "./styled";
const Home = () => {
  return (
    <Wrapper>
      <WrapperSlide>
        <SlideShow />
        <WrapperService>
          {Constant.SERVICE.map((item, index) => {
            return (
              <ServiceWrapper key={index} name={item.name} icon={item.icon} />
            );
          })}
        </WrapperService>
      </WrapperSlide>
      <WrapperNew>
        <ShortNews />
      </WrapperNew>
      <WrapperOverService>
        <OverviewService />
      </WrapperOverService>
    </Wrapper>
  );
};

export default React.memo(Home);
