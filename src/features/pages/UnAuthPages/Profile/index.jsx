import React, { useState, useCallback, useEffect } from 'react';
import {
  Container,
  Column,
  RowContent,
  Title,
  Text,
  Divide,
  Input,
  FormWrapper,
  Icon,
  SelectPicker,
  WrapperSelect,
  RowWrapper,
  Button,
  Image,
  WrapperUpload,
  TextArea,
  WrapperVote,
  Tags,
  WrapperModal,
  Row,
  ColTable,
  HeaderTable,
  Cell,
} from './styled';
import FrownIcon from '@rsuite/icons/legacy/FrownO';
import MehIcon from '@rsuite/icons/legacy/MehO';
import SmileIcon from '@rsuite/icons/legacy/SmileO';
import { Table, Rate } from 'rsuite';
import { validation, validateFormUpadte } from './validation';
import { withEmpty, withArray } from 'exp-value';
import { Constant } from 'utils';
import { useSelector, useDispatch } from 'react-redux';
import { authAPI, departureAPI } from 'config/api';
import { useAlert, useStorage } from 'hooks';
import { setLoading } from 'reduxFolder/loadingSlice';
import { saveUser } from 'reduxFolder/userSlice';
import { IMAGES } from 'assets';
import firebase from 'firebase';
const Profile = () => {
  const [data, setData] = useState({
    first_name: '',
    last_name: '',
    phone_number: '',
    email: '',
    address: '',
    provinces: '',
    districts: '',
    wards: '',
    image: '',
  });
  const [divisionVie, setDivisionVie] = useState({
    provinces: [],
    districts: [],
    wards: [],
  });
  const [tab, setTab] = useState(1);
  const [modalChangePassword, setModalChangePassword] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [newPassword, setNewPassword] = useState(false);
  const [confirmPassword, setConfirmPassword] = useState(false);
  const [changePassword, setChangePassword] = useState({
    oldPassword: '',
    newPassword: '',
    confirmPassword: '',
  });
  const userState = useSelector((state) => state.user);
  const [fileInfo, setFileInfo] = useState(null);
  const [user, setUser] = useState(null);
  const { saveValue } = useStorage();
  const [historyDeparture, setHistoryDeparture] = useState([]);
  const [vote, setVote] = useState({
    visibleModal: false,
    count: 3,
    content: [...Constant.TAGS.slice(0, 3)],
    feedback: '',
    id: '',
    name: '',
    image: '',
  });
  const getDivision = useCallback(({ code_id, d }) => {
    const BASE_URL = `${process.env.PROVINCES}/${d ? 'd' : 'p'}/${
      code_id ? `${code_id}/?depth=2` : ''
    }`;
    return fetch(BASE_URL)
      .then((response) => response.json())
      .then((_data) => {
        return _data;
      });
  });
  const handleInput = useCallback(
    (name, value) => {
      setData((prev) => ({ ...prev, [name]: value }));
    },
    [data]
  );
  const { showSuccess, showError } = useAlert();
  const dispatch = useDispatch();
  const handleInputChangePassword = useCallback(
    (key, value) => {
      setChangePassword((prev) => {
        return {
          ...prev,
          [key]: value,
        };
      });
    },
    [changePassword]
  );
  const handleSubmitChangePassword = useCallback(async () => {
    dispatch(setLoading(true));
    const { oldPassword, newPassword } = changePassword;
    let formData = new FormData();
    formData.append('oldPassword', oldPassword);
    formData.append('newPassword', newPassword);
    try {
      const { data: response } = await authAPI.changePassword(
        user.id,
        formData
      );
      if (response.data) {
        dispatch(setLoading(false));
        showSuccess(response.message);
      }
    } catch (error) {
      dispatch(setLoading(false));
      showError(error.response.data.message);
    }
  }, [changePassword]);
  const handleSubmit = useCallback(() => {
    dispatch(setLoading(true));
    let profileData = new FormData();
    const _address = [data.provinces, data.districts, data.wards, data.address];
    profileData.append('first_name', data.first_name);
    profileData.append('last_name', data.last_name);
    profileData.append('email', data.email);
    profileData.append('phone_number', data.phone_number);
    _address.map((item) => {
      profileData.append('address[]', item);
    });
    async function execute() {
      try {
        const {
          data: { data: response },
        } = await authAPI.updateProfile(user.id, profileData);
        if (response) {
          dispatch(saveUser(response.model));
          dispatch(setLoading(false));
          saveValue('user', response.model, true);
          showSuccess('Lưu thông tin người dùng thành công');
        }
      } catch (error) {
        dispatch(setLoading(false));
        showError('Lưu thông tin người dùng thất bại');
      }
    }
    if (fileInfo) {
      let storageRef = firebase.storage().ref(`images/${data.image.name}`);
      storageRef.put(data.image).then(function () {
        storageRef.getDownloadURL().then(async (url) => {
          profileData.append('image', url);
          return execute();
        });
      });
    }
    if (!fileInfo) {
      execute();
    }
  }, [data, tab, fileInfo]);
  const handleChangePicker = async (value, type) => {
    const _typeList = type === 'provinces' ? 'districts' : 'wards';
    const _typeAPI = type === 'provinces' ? false : true;

    const response = await getDivision({ code_id: value, d: _typeAPI });
    if (type === 'wards') return;
    if (response) {
      const list = withEmpty(_typeList, response).map((item) => {
        return {
          label: withEmpty('name', item),
          value: withEmpty('code', item),
        };
      });
      setDivisionVie((prev) => {
        return {
          ...prev,
          [_typeList]: list,
        };
      });
    }
  };
  const _renderGeography = useCallback(
    ({ label, type, defaultValue }) => {
      return (
        <WrapperSelect>
          <Text>{label}</Text>
          <SelectPicker
            $fluid={true}
            data={withArray(type, divisionVie)}
            onChange={(value) => handleChangePicker(value, type)}
            placeholder={withArray(type, data)}
            onSelect={(value, item) =>
              handleInput(type, withEmpty('label', item))
            }
          />
        </WrapperSelect>
      );
    },
    [divisionVie, data]
  );
  const _renderColumn1 = useCallback(() => {
    return (
      <Column>
        <RowContent $header={true} $borderb={true}>
          <Title H3 bold black>
            Cài đặt
          </Title>
          <Text>Chỉnh sửa thông tin</Text>
        </RowContent>
        <RowContent
          $pointer={true}
          $active={tab === 1}
          onClick={() => {
            setTab(1);
          }}
        >
          <Title H4 black>
            Thông tin cá nhân
          </Title>
        </RowContent>
        <RowContent
          $pointer={true}
          $active={tab === 2}
          onClick={() => {
            setTab(2);
          }}
        >
          <Title H4 black>
            Lịch sử đặt chuyến
          </Title>
        </RowContent>
      </Column>
    );
  });
  const _renderPerson = useCallback(() => {
    const previewFile = (file, callback) => {
      const reader = new FileReader();
      reader.onloadend = () => {
        callback(reader.result);
      };
      reader.readAsDataURL(file);
    };
    return (
      <Column $width={true}>
        <RowContent $header={true}>
          <Title H3 bold black>
            Cá nhân
          </Title>
          <Text>Thông tin chung</Text>
        </RowContent>
        <Divide>Thông tin chi tiết</Divide>
        <RowContent $center $lg_hidden>
          <WrapperUpload
            fileListVisible={false}
            action='//jsonplaceholder.typicode.com/posts/'
            onUpload={(file) => {
              handleInput('image', file.blobFile);
              previewFile(file.blobFile, (value) => {
                setFileInfo(value);
              });
            }}
          >
            {fileInfo ? (
              <img src={fileInfo} width='160px' height='150px' />
            ) : (
              <Button>
                <Image
                  circle
                  source={`${process.env.BASE_URL_IMAGE}${user?.image}`}
                  $avatar={true}
                />
              </Button>
            )}
          </WrapperUpload>
        </RowContent>
        <RowContent>
          <Input
            name='first_name'
            label='Tên'
            type='text'
            leftIcon={<Icon size={18} name='feather-user' />}
            value={withEmpty('first_name', data)}
            onChange={(value) => handleInput('first_name', value)}
          />
          <Input
            name='last_name'
            label='Họ'
            leftIcon={<Icon size={18} name='feather-user' />}
            value={withEmpty('last_name', data)}
            onChange={(value) => handleInput('last_name', value)}
          />
        </RowContent>
        <Input
          name='password'
          label='Mật khẩu'
          type='password'
          leftIcon={<Icon size={18} name='feather-lock' />}
          rightIcon={{
            click: () => setModalChangePassword(true),
            icon: (
              <Title H6 bold black>
                Đổi mật khẩu
              </Title>
            ),
          }}
        />
        <Input
          name='email'
          label='Email'
          leftIcon={<Icon size={18} name='feather-mail' />}
          value={withEmpty('email', data)}
          onChange={(value) => handleInput('email', value)}
        />
        <Input
          name='phone'
          label='Số điện thoại'
          leftIcon={<Icon size={18} name='feather-phone' />}
          value={withEmpty('phone_number', data)}
          onChange={(value) => handleInput('phone_number', value)}
        />
        <RowContent>
          {_renderGeography({
            label: 'Tỉnh',
            type: 'provinces',
            defaultValue: withEmpty('provinces', data),
          })}
          {_renderGeography({
            label: 'Huyện',
            type: 'districts',
            defaultValue: withEmpty('districts', data),
          })}
          {_renderGeography({
            label: 'Xã',
            type: 'wards',
            defaultValue: withEmpty('wards', data),
          })}
        </RowContent>
        <Input
          name='address'
          label='Address'
          leftIcon={<Icon size={18} name='feather-map-pin' />}
          value={withEmpty('address', data)}
          onChange={(value) => handleInput('address', value)}
        />
        {/* </FormWrapper> */}
      </Column>
    );
  }, [data, divisionVie, fileInfo, user, tab]);

  const _renderColumn3 = useCallback(() => {
    const previewFile = (file, callback) => {
      const reader = new FileReader();
      reader.onloadend = () => {
        callback(reader.result);
      };
      reader.readAsDataURL(file);
    };
    return (
      <Column $hidden>
        <RowContent $header={true} $borderb={true}>
          <Title H3 bold black>
            Ảnh cá nhân
          </Title>
          <Text>Nhấn vào avatar để thay ảnh</Text>
        </RowContent>
        <WrapperUpload
          fileListVisible={false}
          action='//jsonplaceholder.typicode.com/posts/'
          onUpload={(file) => {
            handleInput('image', file.blobFile);
            previewFile(file.blobFile, (value) => {
              setFileInfo(value);
            });
          }}
        >
          {fileInfo ? (
            <img src={fileInfo} width='160px' height='150px' />
          ) : (
            <Button>
              <Image
                circle
                source={`${process.env.BASE_URL_IMAGE}${user?.image}`}
                $avatar={true}
              />
            </Button>
          )}
        </WrapperUpload>
      </Column>
    );
  }, [fileInfo, user]);
  const _renderModalChangePassword = useCallback(() => {
    const body = () => {
      return (
        <FormWrapper
          formValue={changePassword}
          model={validation}
          onSubmit={handleSubmitChangePassword}
        >
          <Input
            label='Old Password'
            placeholder='Enter old password'
            type={showPassword ? 'text' : 'password'}
            value={withEmpty('oldPassword', changePassword)}
            name='oldPassword'
            leftIcon={<Icon size={18} name='feather-lock' />}
            onChange={(value) =>
              handleInputChangePassword('oldPassword', value)
            }
            rightIcon={{
              icon: (
                <Icon
                  size={18}
                  name={showPassword ? 'feather-eye' : 'feather-eye-off'}
                />
              ),
              click: () => setShowPassword(!showPassword),
            }}
          />
          <Input
            label='New Password'
            name='newPassword'
            placeholder='Enter new password'
            type={newPassword ? 'text' : 'password'}
            value={withEmpty('newPassword', changePassword)}
            onChange={(value) =>
              handleInputChangePassword('newPassword', value)
            }
            leftIcon={<Icon size={18} name='feather-lock' />}
            rightIcon={{
              icon: (
                <Icon
                  size={18}
                  name={newPassword ? 'feather-eye' : 'feather-eye-off'}
                />
              ),
              click: () => setNewPassword(!newPassword),
            }}
          />
          <Input
            label='Confirm Password'
            name='confirmPassword'
            placeholder='Enter confirm password'
            type={confirmPassword ? 'text' : 'password'}
            value={withEmpty('confirmPassword', changePassword)}
            onChange={(value) =>
              handleInputChangePassword('confirmPassword', value)
            }
            leftIcon={<Icon size={18} name='feather-lock' />}
            rightIcon={{
              icon: (
                <Icon
                  size={18}
                  name={confirmPassword ? 'feather-eye' : 'feather-eye-off'}
                />
              ),
              click: () => setConfirmPassword(!confirmPassword),
            }}
          />
          <Button primary type='submit'>
            Change
          </Button>
        </FormWrapper>
      );
    };
    return (
      <WrapperModal
        size={'xs'}
        show={modalChangePassword}
        header={<Text>Change Password</Text>}
        body={body()}
        onHide={() => setModalChangePassword(!modalChangePassword)}
      />
    );
  });
  const _renderHistoryInvoince = useCallback(() => {
    return (
      <Column $width={true}>
        <RowContent $header={true}>
          <Title H3 bold black>
            Lịch sử chuyến xe
          </Title>
          <Text>Thông tin chuyến xe của bạn</Text>
        </RowContent>
        <Divide>Danh sách chuyến xe</Divide>
        <Table wordWrap virtualized height={400} data={historyDeparture}>
          <ColTable width={80} align='center' fixed>
            <HeaderTable>Id</HeaderTable>
            <Cell dataKey='invoice_code' />
          </ColTable>

          <ColTable width={100}>
            <HeaderTable>Tên chuyến</HeaderTable>
            <Cell>{(rowData) => <p>{rowData.departure?.name}</p>}</Cell>
          </ColTable>

          <ColTable width={70}>
            <HeaderTable>Số vé</HeaderTable>
            <Cell dataKey='quantity' />
          </ColTable>

          <ColTable width={80}>
            <HeaderTable>Tổng tiền</HeaderTable>
            <Cell dataKey='total_price' />
          </ColTable>

          <ColTable width={155}>
            <HeaderTable>Phương thức thanh toán</HeaderTable>
            <Cell>
              {(rowData) => (
                <p>
                  {rowData.form_payment === 0
                    ? 'Thanh toán khi lên xe'
                    : 'Thanh toán online'}
                </p>
              )}
            </Cell>
          </ColTable>

          <ColTable width={100}>
            <HeaderTable>Ngày đặt xe</HeaderTable>
            <Cell dataKey='date' />
          </ColTable>
          <ColTable width={160}>
            <HeaderTable>Hành động</HeaderTable>
            <Cell>
              {(rowData) => (
                <Button
                  primary={true}
                  onClick={() => {
                    setVote({
                      ...vote,
                      id: rowData.id,
                      visibleModal: true,
                      name: rowData.departure?.name,
                      image: rowData.departure?.image,
                    });
                  }}
                >
                  Đánh giá
                </Button>
              )}
            </Cell>
          </ColTable>
        </Table>
      </Column>
    );
  }, [historyDeparture]);
  const onFeedBack = useCallback(async () => {
    const { content, feedback, count, id } = vote;
    const contentBody = content.toString() + feedback;
    let bodyFeedback = new FormData();
    bodyFeedback.append('content', contentBody);
    bodyFeedback.append('star', count);
    try {
      const {
        data: { data: response },
      } = await departureAPI.commentDeparture(id, bodyFeedback);
      if (response) {
        showSuccess(
          'Cảm ơn bạn đã tin tưởng và sử dụng dịch vụ chúng tôi. Chúng tôi sẽ tiếp thu ý kiến và cải thiện thêm !'
        );
        setVote({ ...vote, visibleModal: false });
      }
    } catch (error) {
      console.log('error');
    }
  }, [vote]);
  const _renderModalVote = useCallback(() => {
    const renderCharacter = (value, index) => {
      // unselected character
      if (value < index + 1) {
        return <MehIcon style={{ fontSize: 30, width: 30, height: 30 }} />;
      }
      if (value < 3) {
        return (
          <FrownIcon
            style={{ color: '#99A9BF', fontSize: 30, width: 30, height: 30 }}
          />
        );
      }
      if (value < 4) {
        return (
          <MehIcon
            style={{ color: '#F4CA1D', fontSize: 30, width: 30, height: 30 }}
          />
        );
      }
      return (
        <SmileIcon
          style={{ color: '#ff9800', fontSize: 30, width: 30, height: 30 }}
        />
      );
    };
    const _renderBody = () => {
      return (
        <FormWrapper>
          <WrapperVote>
            <Row>
              <Image source={IMAGES.LOGO} circle />
            </Row>
            <Row>
              <Title bold H3 black mr='10px 0'>
                Bạn thấy chuyến đi của bạn thế nào ?
              </Title>
              <Text>
                Hãy giúp chúng tôi cải thiện trải nghiệm Quang Huy Lemounsine
                bằng cách đánh giá chuyến đi này.
              </Text>
            </Row>
            <Row>
              <Rate
                defaultValue={withEmpty('count', vote)}
                renderCharacter={renderCharacter}
                onChange={(value) => {
                  setVote({ ...vote, count: value });
                }}
              />
            </Row>
            <Row>
              {Constant.TAGS.map((ele, index) => {
                return (
                  <Tags
                    key={index}
                    onClick={() => {
                      if (vote.content.find((e) => e === ele)) {
                        const _content = vote.content.filter((e) => e !== ele);
                        setVote({ ...vote, content: [..._content] });
                        return;
                      }
                      setVote({ ...vote, content: [...vote?.content, ele] });
                    }}
                    $checked={vote.content.find((item) => item === ele)}
                  >
                    {ele}
                  </Tags>
                );
              })}
            </Row>
            <Row>
              <TextArea
                as={'textarea'}
                rows={3}
                value={withEmpty('feedback', vote)}
                onChange={(_value) => {
                  setVote({ ...vote, feedback: _value.target.value });
                }}
              />
            </Row>
            <Button primary fluid onClick={() => onFeedBack()}>
              Gửi đánh giá
            </Button>
          </WrapperVote>
        </FormWrapper>
      );
    };
    return (
      <WrapperModal
        show={vote.visibleModal}
        size={'sm'}
        body={_renderBody()}
        onHide={() => setVote({ ...vote, visibleModal: false })}
      />
    );
  }, [vote]);
  useEffect(() => {
    dispatch(setLoading(true));
    async function getProvince() {
      const listProvice = await getDivision({ code_id: null, d: false });
      const {
        data: { data: dataUser },
      } = await authAPI.showProfile(userState?.id);
      if (dataUser) {
        setUser(dataUser);
      }
      if (listProvice.length) {
        const dataProvince = listProvice.map((ele) => {
          return {
            label: withEmpty('name', ele),
            value: withEmpty('code', ele),
          };
        });
        setDivisionVie({
          ...divisionVie,
          provinces: dataProvince,
        });
        dispatch(setLoading(false));
      }
    }
    getProvince();
  }, []);
  useEffect(() => {
    const address = withEmpty('address', user);
    setData({
      first_name: withEmpty('first_name', user),
      last_name: withEmpty('last_name', user),
      email: withEmpty('email', user),
      phone_number: withEmpty('phone_number', user),
      provinces: address?.[0],
      districts: address?.[1],
      wards: address?.[2],
      address: address?.[3],
      image: withEmpty('image', user),
    });
  }, [user]);
  useEffect(() => {
    dispatch(setLoading(true));
    async function execute() {
      try {
        const {
          data: { data: response },
        } = await departureAPI.historyDeparture();
        if (response) {
          dispatch(setLoading(false));
          const _history = response?.filter((ele) => ele.status === 1);
          setHistoryDeparture(_history);
        }
      } catch (error) {
        dispatch(setLoading(false));
      }
    }
    execute();
  }, []);
  return (
    <Container>
      <FormWrapper
        formValue={data}
        onSubmit={handleSubmit}
        model={validateFormUpadte}
      >
        <RowWrapper>
          {_renderColumn1()}
          {tab === 1 ? _renderPerson() : _renderHistoryInvoince()}
          {_renderColumn3()}
        </RowWrapper>
        {_renderModalChangePassword()}
        {_renderModalVote()}
        <RowWrapper $flexEnd={true}>
          <Button warning>Huỷ</Button>
          <Button primary type='submit'>
            Lưu thông tin
          </Button>
        </RowWrapper>
      </FormWrapper>
    </Container>
  );
};

export default React.memo(Profile);
