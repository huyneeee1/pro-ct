import styled, { css } from "styled-components";
import { Divider, Table, Uploader } from "rsuite";
import {
  BaseFlexContainer,
  BaseWrapper,
  BaseText,
  BaseTitle,
  BaseForm,
  BaseIcon,
  BaseInputPicker,
  BaseButton,
  BaseImage,
  BaseModal,
  BaseInput,
} from "atoms";
import { CustomInput } from "molecules";

export const Container = styled(BaseFlexContainer)`
  width: 95%;
  justify-content: space-between;
  margin: 20px 0;
  align-items: flex-start;
  .rs-uploader-text .rs-uploader-trigger-btn {
    border-radius: 50%;
  }
`;
export const Column = styled.div`
  width: 19%;
  height: fit-content;
  padding: 20px;
  background: ${(props) => props.theme.colors.bgBooking};
  box-shadow: ${(props) => props.theme.shadow.shadow_2};
  border-radius: 8px;
  ${(props) =>
    props.$width &&
    css`
      width: 58%;
      height: 100%;
      @media (max-width: 768px) {
        width: 78%;
      }
    `}
  ${(props) =>
    props.$hidden &&
    css`
      @media (max-width: 768px) {
        display: none;
      }
    `}
`;
export const RowContent = styled(BaseWrapper)`
  width: 100%;
  justify-content: space-between;
  width: 100%;
  & > .rs-form-group {
    width: 48%;
  }
  padding: 10px 0;
  ${(props) =>
    props.$header &&
    css`
      flex-direction: column;
    `}
  ${(props) =>
    props.$borderb &&
    css`
      border-bottom: 1px solid ${props.theme.colors.secondary[4]};
    `}
  ${(props) =>
    props.$active &&
    css`
      border-left: 2px solid ${props.theme.colors.primary};
      padding-left: 10px;
    `}
  ${(props) =>
    props.$center &&
    css`
      justify-content: center;
      align-items: center;
    `}
      ${(props) =>
    props.$flexLeft &&
    css`
      justify-content: flex-start;
    `}
      ${(props) =>
    props.$pointer &&
    css`
      cursor: pointer;
    `}
    ${(props) =>
    props.$lg_hidden &&
    css`
      @media screen and (min-width: 768px) {
        display: none;
      }
    `}
`;
export const Title = styled(BaseTitle)`
  ${(props) =>
    props.$mr &&
    css`
      margin: 0 10px;
    `}
`;
export const Text = styled(BaseText)`
  font-weight: 500 !important;
  display: flex !important;
  color: ${(props) => props.theme.colors.black[0]};
  font-size: 1rem;
  margin-bottom: 4px;
`;
export const Divide = styled(Divider)`
  color: ${(props) => props.theme.colors.black[0]};
`;
export const Input = styled(CustomInput)`
  width: 45%;
  display: flex;
`;
export const FormWrapper = styled(BaseForm)``;
export const Icon = styled(BaseIcon)``;
export const WrapperSelect = styled.div`
  margin-bottom: 24px;
  width: 32%;
`;
export const SelectPicker = styled(BaseInputPicker)`
  ${(props) =>
    props.$fluid &&
    css`
      width: 100% !important;
    `}
  height: 40px;
`;
export const RowWrapper = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
  ${(props) =>
    props.$flexEnd &&
    css`
      justify-content: flex-end;
      margin-top: 20px;
    `}
`;
export const Button = styled(BaseButton)`
  margin: 0 10px;
  ${(props) =>
    props.$upload &&
    css`
      width: 30px;
      height: 30px;
    `}
`;
export const Image = styled(BaseImage)`
  display: flex;
  width: 100px;
  ${(props) =>
    props.$avatar &&
    css`
      width: 120px;
      height: 120px;
      transform: scale3d(1.5, 1.5, 1.5);
    `}
`;

export const WrapperUpload = styled(Uploader)`
  display: flex;
  margin-top: 10px;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  position: relative;
  flex-direction: column;
`;
export const WrapperVote = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  padding: 0 20px;
  align-items: center;
`;
export const Row = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  text-align: center;
`;
export const WrapperCreatePayment = styled.div`
  width: 30%;
  height: 90px;
  display: flex;
  align-items: center;
  justify-content: space-around;
  padding: 20px;
  border: 1px dashed ${(props) => props.theme.colors.secondary[3]};
  border-radius: 10px;
`;
export const WrapperModal = styled(BaseModal)``;
export const ColTable = styled(Table.Column)``;
export const HeaderTable = styled(Table.HeaderCell)``;
export const Cell = styled(Table.Cell)``;
export const Tags = styled.div`
  padding: 8px 13px;
  border-radius: 20px;
  border: 1px solid ${(props) => props.theme.colors.grey[2]};
  cursor: pointer;
  margin: 5px 3px;
  ${(props) =>
    props.$checked &&
    css`
      background: ${(props) => props.theme.colors.primary};
      color: ${(props) => props.theme.colors.grey[0]};
    `}
`;
export const TextArea = styled(BaseInput)`
  width: 100%;
  border: 1px solid ${(props) => props.theme.colors.grey[2]};
  border-radius: 10px;
  padding: 10px;
  margin: 10px 0;
`;
