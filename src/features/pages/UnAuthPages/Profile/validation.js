import { SchemaModel, StringType } from "schema-typed";
const regexPassword = /^([a-zA-Z0-9])(?=.*[a-z])(?=.*\d)[a-zA-Z\d]{7,}$/;
export const validation = SchemaModel({
  oldPassword: StringType().isRequired("Old password is required"),
  newPassword: StringType()
    .isRequired("New Password is required")
    .pattern(
      regexPassword,
      "New Password must be at least 8 character and combination of alphabets numbers and letters"
    ),
  confirmPassword: StringType()
    .isRequired("Confirm Password is required")
    .addRule((value, data) => {
      if (value != data.newPassword) return false;
      return true;
    }, "Confirm password is not match password"),
});
export const validateFormUpadte = SchemaModel({
  first_name: StringType().isRequired("First name is required"),
  last_name: StringType().isRequired("Last Name is required"),
  phone_number: StringType().isRequired("Phone is required"),
  email: StringType().isRequired("Email is required"),
});
