import { IMAGES } from "assets";
import React, { useState } from "react";
import {
  GoogleMap,
  Marker,
  Polyline,
  withGoogleMap,
  withScriptjs,
} from "react-google-maps";
import InfoBox from "react-google-maps/lib/components/addons/InfoBox";
import { Wrapper } from "./styled";
const options = { closeBoxURL: "", enableEventPropagation: true };
const positions = [
  {
    lat: 20.852571,
    lng: 106.016998,
    label: "Tp Hưng yên",
    time1: "7h",
    time2: "15h",
  },
  {
    lat: 20.839893,
    lng: 106.008995,
    label: "Khoái Châu",
    time1: "8h",
    time2: "14h30",
  },
  {
    lat: 20.946339,
    lng: 105.924283,
    label: "Văn Giang",
    time1: "8h50",
    time2: "14h",
  },
  {
    lat: 20.99645,
    lng: 105.905198,
    label: "Cầu Thanh Trì",
    time1: "9h",
    time2: "13h40",
  },
  {
    lat: 20.990526,
    lng: 105.804219,
    label: "Thanh Xuân",
    time1: "9h30",
    time2: "13h",
  },
  {
    lat: 21.03968,
    lng: 105.767189,
    label: "Bến xe Mỹ Đình",
    time1: "10h",
    time2: "12h30",
  },
];
const optionsPolyline = {
  strokeColor: "#EB5757",
  strokeOpacity: 0.8,
  strokeWeight: 6,
  fillColor: "#EB5757",
  fillOpacity: 0.35,
  clickable: false,
  draggable: false,
  editable: false,
  visible: true,
  radius: 30000,
  zIndex: 1,
};
function Map() {
  const myLocation = { lat: 20.99645, lng: 105.905198 };
  return (
    <GoogleMap defaultZoom={12} defaultCenter={myLocation}>
      {positions &&
        positions.map((position, index) => (
          <Marker
            position={new window.google.maps.LatLng(position)}
            key={index}
            icon={{
              url: IMAGES.CARLOCATION,
              scaledSize: new window.google.maps.Size(40, 40),
            }}
          >
            <InfoBox options={options}>
              <>
                <div
                  style={{
                    backgroundColor: "#2b663d",
                    color: "white",
                    borderRadius: "1em",
                    padding: "0.5em 0.8em",
                  }}
                >
                  <h6>{position.label}</h6>
                  <b>Chiều đi:⏱{position.time1}</b> <br />
                  <b>Chiều về:⏱{position.time2}</b>
                </div>
              </>
            </InfoBox>
          </Marker>
        ))}
      <Polyline
        // path={positions}
        options={optionsPolyline}
      />
    </GoogleMap>
  );
}

const WrapperMap = withScriptjs(withGoogleMap(Map));

const RoutePage = () => {
  const key = "AIzaSyCr8i8iYrxjD5gUia6h5poWt6D_67jDHuA";
  const [directions, setDirections] = useState("");
  // const DirectionsService = new google.maps.DirectionsService();
  const origin = { lat: 20.852571, lng: 106.016998 };
  const destination = { lat: 20.811581, lng: 105.335312 };
  // DirectionsService.route(
  //   {
  //     origin: origin,
  //     destination: destination,
  //     travelMode: google.maps.TravelMode.DRIVING,
  //   },
  //   (result, status) => {
  //     if (status === google.maps.DirectionsStatus.OK) {
  //       setDirections(result);
  //     } else {
  //       console.error(`error fetching directions ${result}`);
  //     }
  //   }
  // );

  return (
    <Wrapper>
      <WrapperMap
        googleMapURL={`https://maps.googleapis.com/maps/api/js?libraries=geometry,drawing,places&key=${key}`}
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={
          <div
            style={{
              height: `100vh`,
              margin: `auto`,
            }}
          />
        }
        mapElement={<div style={{ height: `100%` }} />}
      ></WrapperMap>
    </Wrapper>
  );
};

export default React.memo(RoutePage);
