import styled from "styled-components";
import {
  BaseWrapper,
  BaseFlexContainer,
  BaseImage,
  BaseText,
  BaseForm,
  BaseTextArea,
} from "atoms";
import { Avatar } from "rsuite";
export const Container = styled(BaseFlexContainer)`
  background: ${(props) => props.theme.colors.secondary[5]};
  display: flex;
  width: 100%;
`;
export const Wrapper = styled(BaseFlexContainer)`
  width: 90%;
  min-height: 550px;
  box-shadow: ${(props) => props.theme.shadow.shadow_1};
  margin: 20px 0;
  border-radius: 8px;
  background: ${(props) => props.theme.colors.bgBooking};
  flex-direction: row;
  @media (max-width: 768px) {
    flex-direction: column;
    padding: 10px 0;
  }
`;
export const Column = styled(BaseWrapper)`
  align-items: center;
  flex-direction: column;
  padding: 0 20px;
  @media (max-width: 768px) {
    width: 100%;
  }
`;
export const Image = styled(BaseImage)`
  width: 100%;
`;
export const Icon = styled(Avatar)`
  background: ${(props) => props.theme.colors.primary};
  color: ${(props) => props.theme.colors.white};
`;
export const Text = styled(BaseText)`
  margin-left: 5px;
  display: flex;
  width: 80%;
`;
export const WrapperRowText = styled(BaseWrapper)`
  align-items: center;
  margin: 10px 0;
  width: 100%;
`;
export const Form = styled(BaseForm)`
  padding: 15px 20px;
  background: ${(props) => props.theme.colors.textPurple[1]};
  border-radius: 8px;
  margin: 20px;
`;
export const TextAreaWrapper = styled(BaseTextArea)`
  width: 100%;
`;
export const ColumnContact = styled(BaseWrapper)`
  width: 100%;
`;
