import { SchemaModel, StringType } from "schema-typed";
export const validation = SchemaModel({
  email: StringType()
    .isEmail("Please enter the correct email")
    .isRequired("Email is required"),
  phone: StringType().isRequired("Phone is required"),
  name: StringType().isRequired("Name is required"),
});
