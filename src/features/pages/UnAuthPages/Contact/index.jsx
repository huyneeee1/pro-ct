import React, { useState, useCallback } from "react";
import { BaseTitle, BaseIcon, BaseButton } from "atoms";
import {
  Wrapper,
  Container,
  Column,
  Image,
  Icon,
  Text,
  WrapperRowText,
  Form,
  TextAreaWrapper,
  ColumnContact,
} from "./styled";
import { CustomInput } from "molecules";
import { IMAGES } from "assets";
import { Constant } from "utils";
import { withEmpty } from "exp-value";
import { commentDepAPI } from "config/api";
import { useAlert } from "hooks";
import { validation } from "./validation";
const Contact = () => {
  const [data, setData] = useState({
    name: "",
    phone: "",
    email: "",
    content: "",
  });
  const { showError, showSuccess } = useAlert();
  const handleInput = useCallback(
    (key, value) => {
      setData((prev) => {
        return {
          ...prev,
          [key]: value,
        };
      });
    },
    [data]
  );
  const handleSubmit = useCallback(async () => {
    let body = new FormData();
    body.append("name", data.name);
    body.append("email", data.email);
    body.append("phone", data.phone);
    body.append("content", data.content);
    try {
      const {
        data: { data: response },
      } = await commentDepAPI.contact(body);
      if (response) {
        showSuccess(
          "Chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất. Cảm ơn bạn đã gửi thông tin đến cho chúng tôi."
        );
      }
    } catch (error) {
      showError("Đã xảy ra lỗi.Vui lòng thử lại");
    }
  }, [data]);
  return (
    <Container>
      <Wrapper>
        <Column column={true}>
          <Image source={IMAGES.CONTACT} />
          <ColumnContact>
            <WrapperRowText>
              <Icon circle>
                <BaseIcon name="feather-mail" size={30} />
              </Icon>
              <Text>{Constant.PROFILE.EMAIL}</Text>
            </WrapperRowText>
            <WrapperRowText>
              <Icon circle>
                <BaseIcon name="feather-phone" size={30} />
              </Icon>
              <Text>{Constant.PROFILE.PHONE}</Text>
            </WrapperRowText>
          </ColumnContact>

          <WrapperRowText>
            <Icon circle>
              <BaseIcon name="feather-map-pin" size={30} />
            </Icon>
            <Text>{Constant.PROFILE.OFFICE}</Text>
          </WrapperRowText>
        </Column>

        <Column>
          <BaseTitle H1 bold black style={{ marginTop: "20px" }}>
            Liên hệ với chúng tôi
          </BaseTitle>

          <Form formValue={data} onSubmit={handleSubmit} model={validation}>
            <CustomInput
              label="Họ và tên"
              name="name"
              placeholder="Vui lòng điền họ và tên"
              require
              value={withEmpty("name", data)}
              onChange={(value) => handleInput("name", value)}
            />
            <CustomInput
              label="Email"
              name="email"
              placeholder="Vui lòng email"
              require
              value={withEmpty("email", data)}
              onChange={(value) => handleInput("email", value)}
            />
            <CustomInput
              label="Phone"
              name="phone"
              placeholder="Vui lòng sdt"
              require
              value={withEmpty("phone", data)}
              onChange={(value) => handleInput("phone", value)}
            />

            <TextAreaWrapper
              rows={3}
              name="content"
              label="Nội dung"
              defaultValue={withEmpty("content", data)}
              onChange={(value) => handleInput("content", value)}
            />
            <BaseButton primary type="submit">
              Submit
            </BaseButton>
          </Form>
        </Column>
      </Wrapper>
    </Container>
  );
};

export default Contact;
