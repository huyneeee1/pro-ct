import styled, { css } from "styled-components";
import { BaseWrapper, BaseTitle, BaseText, BaseImage } from "atoms";
export const Wrapper = styled.div`
  display: flex;
  height: auto;
  width: 80%;
  margin: 20px 0px;
  @media (max-width: 768px) {
    flex-direction: column;
    width: 100%;
    align-items: center;
  }
`;
export const WrapperSideBar = styled.div`
  background-color: ${(props) => props.theme.colors.bgBooking};
  width: 300px;
  max-height: 600px;
  position: sticky;
  top: 20px;
  margin-right: 1.5%;
  border-radius: 10px;
  padding: 20px 15px;
  @media (max-width: 768px) {
    width: 90%;
    margin-top: 10px;
  }
`;
export const WrapperContent = styled.div`
  width: calc(100% - 300px);
  border-radius: 10px;
  margin-right: 20px;
  background: ${(props) => props.theme.colors.bgBooking};
  padding: 20px 50px;
  @media (max-width: 768px) {
    width: 90%;
    padding: 20px 10px;
    margin-right: 0px;
  }
`;

export const Title = styled(BaseTitle)``;
export const Text = styled(BaseText)`
  ${(props) =>
    props.$news &&
    css`
      margin: 10px 0;
      color: ${(props) => props.theme.colors.secondary[1]};
    `}
`;

export const WrapperText = styled.div`
  ${(props) =>
    props.$center &&
    css`
      text-align: center;
    `}
`;
export const Image = styled(BaseImage)`
  width: 100%;
  height: auto;
  margin: 10px 0;
`;
