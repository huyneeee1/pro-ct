import React from "react";
import {
  Wrapper,
  WrapperContent,
  WrapperSideBar,
  Title,
  Text,
  WrapperText,
  Image,
} from "./styled";
import { SideBarNews } from "organisms";
import { IMAGES } from "assets";
const IntroducePage = () => {
  return (
    <Wrapper>
      <WrapperContent>
        <WrapperText $center={true}>
          <Title H1 bold primary mr={"25px 0px"} uppercase>
            Tổng quan về Quang Huy Limousine.
          </Title>
          <Text>
            Chúng tôi là tổng đài hỗ trợ đặt xe trực tuyến qua điện thoại, Chúng
            tôi không mạo danh hay kinh doanh dịch vụ của bất kể một cá nhân hay
            một đơn vị cụ thể nào.
          </Text>
        </WrapperText>
        <WrapperText>
          <Title H3 bold primary mr={"25px 0px"} uppercase>
            1.Chất Lượng
          </Title>
          <Text>
            khách rộng rãi hơn, Trang bị loại ghế đệm dày khiến cho người nằm có
            cảm giác êm ái, thoải mái. Các chuyến xe dù xa hay gần, thời gian
            ngồi ghế lâu cũng sẽ làm hành khách mệt mỏi. Nhưng với xe hạng
            thương gia, hành khách hoàn toàn có thể thư giãn và nghỉ ngơi trên
            xe dễ dàng.
          </Text>
          <Image source={IMAGES.GIOITHIEU1} />
        </WrapperText>
        <WrapperText>
          <Title H3 bold primary mr={"25px 0px"} uppercase>
            2.Tiện Ích
          </Title>
          <Text>
            Được trang bị đầy đủ các tiện ích thông thường như máy lạnh, chăn
            đắp, nước uống. Điểm cộng cho dòng xe Vip là có thêm tủ lạnh, ti vi
            led 19 inch, hệ thống đèn chiếu sáng đọc sách, bục gác chân, v.v..
            Nhằm đáp ứng mọi nhu cầu và mang lại sự thoải mái nhất cho hành
            khách.
          </Text>
          <Image source={IMAGES.GIOITHIEU2} />
        </WrapperText>
        <WrapperText $center={true}>
          <Title H2 bold primary mr={"25px 0px"} uppercase>
            Cảm Ơn Quý Khách Đã ghé Quang Huy Limousine!
          </Title>
        </WrapperText>
      </WrapperContent>
      <WrapperSideBar>
        <WrapperText>
          <Title H3 bold primary>
            Bài viết mới
          </Title>
          <SideBarNews />
        </WrapperText>
        <WrapperText>
          <Title H3 bold primary>
            Siêu ưu đãi
          </Title>
          <Text $news={true}>
            Giảm giá 30% tiền vé xe cho Giảm giá 30% tiền vé xe cho 5 suất vé
            đầu tiên5 suất vé đầu tiên
          </Text>
        </WrapperText>
      </WrapperSideBar>
    </Wrapper>
  );
};

export default IntroducePage;
