import { BaseButton, BaseForm, BaseIcon, BaseText, BaseTitle } from "atoms";
import styled from "styled-components";

export const Title = styled(BaseTitle)`
  font-size: 2.5rem;
  font-weight: bold;
  @media (max-width: 768px) {
    font-size: 1.7rem;
  }
`;
export const Text = styled(BaseText)`
  margin: 5px 0;
  font-size: 15px;
  ${(props) =>
    props.$mr &&
    `
    margin-right:3px;
  `}
`;
export const Button = styled(BaseButton)``;
export const Icon = styled(BaseIcon)`
  margin-right: 3px;
  color: ${(props) => props.theme.colors.primary};
`;
export const Form = styled(BaseForm)``;
export const WrapperRowText = styled.div`
  display: flex;
  align-items: center;
`;
export const Content = styled.div`
  width: 100%;
  margin-top: 15px;
`;
export const Column = styled.div`
  @media (min-width: 768px) {
    display: flex;
    justify-content: space-between;
  }
`;
