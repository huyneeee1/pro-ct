import { IMAGES } from "assets";
import { CustomInput } from "molecules";
import { LayoutColumnPage } from "organisms";
import React, { useCallback, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import {
  Content,
  Form,
  Icon,
  Text,
  Title,
  WrapperRowText,
  Column,
} from "./styled";
import { invoiceAPI } from "config/api";
import { useDispatch } from "react-redux";
import { setLoading } from "reduxFolder/loadingSlice";
import { useAlert } from "hooks";
import { withEmpty } from "exp-value";
const CheckBooking = () => {
  const [value, setValue] = useState("");
  const { search } = useLocation();
  const [data, setData] = useState("");
  const dispatch = useDispatch();
  const { showError, showSuccess } = useAlert();
  useEffect(() => {
    const arrUrl = search.split("?");
    const searchBooking = new URLSearchParams(arrUrl[1]).get("search");
    if (searchBooking) {
      getData(searchBooking);
    }
  }, []);
  const getData = async (_value) => {
    dispatch(setLoading(true));
    try {
      const {
        data: { data: response },
      } = await invoiceAPI.checkingInvoince(_value);
      if (response) {
        dispatch(setLoading(false));
        showSuccess("Tìm thông tin vé thành công!");
        setData(response[0]);
      }
    } catch (error) {
      dispatch(setLoading(false));
      showError("Không tìm thấy vé !");
    }
  };
  const handleSubmit = useCallback(async () => {
    getData(value);
  }, [value]);
  const _renderRowText = useCallback(
    (label, _value, icon) => {
      return (
        <WrapperRowText>
          <Icon size={18} name={`feather-${icon}`} />
          <Text $mr>{label}</Text>
          <Text black bold>
            {_value !== "total_price"
              ? withEmpty(_value, data)
              : withEmpty(_value, data)
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " VND"}
          </Text>
        </WrapperRowText>
      );
    },
    [data]
  );
  return (
    <LayoutColumnPage urlImage={IMAGES.SEARCHSVG}>
      <Title black>Kiểm tra vé xe</Title>
      <Form formData={value} onSubmit={handleSubmit}>
        <CustomInput
          name="textSearch"
          placeholder="Nhập mã vé bạn đã đặt"
          value={value}
          onChange={(_value) => setValue(_value)}
          rightIcon={{
            click: () => handleSubmit(),
            icon: <Icon size={18} name="feather-search" />,
          }}
        />
      </Form>
      {data ? (
        <Content>
          {_renderRowText("Người đặt:", "name", "user")}
          {_renderRowText("Số điện thoại:", "phone", "phone")}
          {_renderRowText("Giờ xuất phát:", "departure.start_time", "clock")}
          <Column>
            {_renderRowText("Điểm đón:", "go_point", "map-pin")}
            {_renderRowText("Điểm đi:", "come_point", "map-pin")}
          </Column>
          <Column>
            {_renderRowText("Số vé:", "quantity", "inbox")}
            {_renderRowText(
              "Phương thức thanh toán:",
              "form_payment",
              "credit-card"
            )}
          </Column>
          {_renderRowText("Ghi chú:", "note", "file-text")}
          {_renderRowText("Tổng tiền", "total_price", "dollar-sign")}
        </Content>
      ) : (
        <Text>Không có thông tin về vé xe !</Text>
      )}
    </LayoutColumnPage>
  );
};

export default React.memo(CheckBooking);
