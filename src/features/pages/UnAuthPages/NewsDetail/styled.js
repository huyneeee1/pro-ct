import styled, { css } from "styled-components";
import {
  BaseWrapper,
  BaseText,
  BaseTitle,
  BaseFlexContainer,
  BaseImage,
} from "atoms";
import { Divider, Breadcrumb } from "rsuite";
export const Wrapper = styled.div`
  display: flex;
  height: auto;
  width: 80%;
  margin: 20px 0px;
`;
export const WrapperSideBar = styled.div`
  background-color: ${(props) => props.theme.colors.white};
  width: 300px;
  max-height: 600px;
  position: sticky;
  top: 20px;
  margin-right: 1.5%;
  border-radius: 10px;
  padding: 10px;
`;
export const WrapperContent = styled.div`
  width: calc(100% - 300px);
  border-radius: 10px;
  margin-right: 20px;
  background: ${(props) => props.theme.colors.white};
  padding: 20px;
`;
export const Line = styled(Divider)`
  margin: 24px 20px;
`;
export const Title = styled(BaseTitle)`
  ${(props) =>
    props.$title &&
    css`
      font-size: 28px;
      color: ${(props) => props.theme.colors.primary};
      bold: semibold;
      font-weight: 700;
    `}
`;
export const Text = styled(BaseText)`
  margin: 0 5px;
`;
export const WrapperRow = styled.div`
  display: flex;
  flex-direction: row;
  ${(props) =>
    props.$title &&
    css`
      margin-top: 30px;
    `}
  ${(props) =>
    props.$content &&
    css`
      margin-top: 15px;
    `}
`;
export const Image = styled(BaseImage)`
  width: 100%;
  margin: 5px 0;
`;
export const WrapperBreakCumb = styled(Breadcrumb)``;
export const BreakItem = styled(Breadcrumb.Item)`
  font-size: 16px;
  color: ${(props) => props.theme.colors.primary};
  font-weight: 400;
`;
