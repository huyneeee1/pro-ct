import { BaseIcon } from 'atoms';
import { newsAPI } from 'config/api';
import dayjs from 'dayjs';
import { withEmpty } from 'exp-value';
import parse from 'html-react-parser';
import { SideBarNews } from 'organisms';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
import { setLoading } from 'reduxFolder/loadingSlice';
import {
  BreakItem,
  Text,
  Title,
  Wrapper,
  WrapperBreakCumb,
  WrapperContent,
  WrapperRow,
  WrapperSideBar,
} from './styled';

const NewsDetail = () => {
  const { search } = useLocation();
  const [data, setData] = useState();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(setLoading(true));
    async function execute() {
      try {
        const { data: response } = await newsAPI.detail(search.slice(1));
        if (response) {
          setData(response.data);
          dispatch(setLoading(false));
        }
      } catch (error) {
        dispatch(setLoading(false));
      }
    }
    execute();
  }, [search]);
  return (
    <Wrapper>
      <WrapperContent>
        <WrapperBreakCumb>
          <BreakItem href='/'>Trang chủ</BreakItem>
          <BreakItem>Tin tức</BreakItem>
        </WrapperBreakCumb>
        <Title $title={true}>{withEmpty('name', data)}</Title>
        <WrapperRow>
          <BaseIcon name='feather-user' size={18} />
          <Text>Nguyen Quang Huy</Text>•
          <Text>
            {dayjs(withEmpty('created_at', data)).format('MMMM D, YYYY h:mm A')}
          </Text>
        </WrapperRow>
        {parse(withEmpty('content', data))}
      </WrapperContent>
      <WrapperSideBar>
        <SideBarNews />
      </WrapperSideBar>
    </Wrapper>
  );
};

NewsDetail.propTypes = {};

export default React.memo(NewsDetail);
