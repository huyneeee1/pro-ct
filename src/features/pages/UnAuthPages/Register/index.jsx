import firebase from '@firebase/app';
import { IMAGES } from 'assets';
import { authAPI } from 'config/api';
import { auth } from 'config/firebase';
import { withEmpty } from 'exp-value';
import { useAlert } from 'hooks';
import { LayoutFormLogin } from 'organisms';
import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { setLoading } from 'reduxFolder/loadingSlice';
import { Routers } from 'utils';
import {
  BodyModal,
  Button,
  ButtonGroup,
  Form,
  Icon,
  Image,
  Input,
  OTPInput,
  SendVerify,
  Title,
  WrapperModal,
  WrapperRowInput,
} from './styled';
import { notificationAPI } from 'config/api';
import { validation } from './validation';
const Register = () => {
  const { showError, showSuccess } = useAlert();
  const dispatch = useDispatch();
  const [verify, setVerify] = useState({
    status: false,
    otp: '',
  });
  const [result, setResult] = useState('');
  const [data, setData] = useState({
    first_name: '',
    last_name: '',
    email: '',
    phone_number: '',
    password: '',
    passwordAgain: '',
  });
  const [showPassword, setShowPassword] = useState(false);
  const [showPasswordCf, setShowPasswordCf] = useState(false);
  const history = useHistory();
  const [divisionVie, setDivisionVie] = useState({
    provinces: [],
    districts: [],
    wards: [],
  });
  const goToPage = useCallback((route) => {
    history.push(route);
  }, []);
  const getDivision = useCallback(({ code_id, d }) => {
    const BASE_URL = `${process.env.PROVINCES}/${d ? 'd' : 'p'}/${
      code_id ? `${code_id}/?depth=2` : ''
    }`;
    return fetch(BASE_URL)
      .then((response) => response.json())
      .then((data) => {
        return data;
      });
  });

  useEffect(() => {
    async function getProvince() {
      const listProvice = await getDivision({ code_id: null, d: false });
      if (listProvice.length) {
        const dataProvince = listProvice.map((ele) => {
          return {
            label: withEmpty('name', ele),
            value: withEmpty('code', ele),
          };
        });
        setDivisionVie({
          ...divisionVie,
          provinces: dataProvince,
        });
      }
    }
    getProvince();
  }, []);
  const handleInput = useCallback(
    (name, value) => {
      setData((prev) => ({ ...prev, [name]: value }));
    },
    [data]
  );

  const handleSubmit = () => {
    const { phone_number } = data;
    let _verify = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
      size: 'invisible',
    });
    auth
      .signInWithPhoneNumber(`+84${phone_number}`, _verify)
      .then((_result) => {
        setResult(_result);
        setVerify({ ...verify, status: true });
      })
      .catch((err) => {
        grecaptcha.reset(window.recaptchaWidgetId);
        showError('Error');
      });
  };
  const handleVerifyAccount = useCallback(() => {
    dispatch(setLoading(true));
    const { otp } = verify;
    const {
      first_name,
      last_name,
      phone_number,
      password,
      passwordAgain,
      email,
    } = data;
    let formData = new FormData();
    formData.append('email', email);
    formData.append('first_name', first_name);
    formData.append('last_name', last_name);
    formData.append('phone_number', phone_number);
    formData.append('password', password);
    formData.append('passwordAgain', passwordAgain);
    formData.append('status', '1');
    if (otp === null) return;

    result
      .confirm(otp)
      .then(async (_result) => {
        try {
          const {
            data: { data: response },
          } = await authAPI.register(formData);

          if (response) {
            const title = 'Chào mừng bạn đến với Quang Huy Lemounsine!';
            const content = `Quang Huy Limounsine xin kính chào ${response?.model?.first_name} ${response?.model?.last_name}. Cảm ơn quý khách đã tin tưởng và sử dụng dịch vụ của chúng tôi. Chúc quý khách có thật nhiều những chuyến đi an toàn và vui vẻ cùng với Quang Huy Limounsine!`;
            let bodySendDBNotifi = new FormData();
            bodySendDBNotifi.append('token_device', 'none');
            bodySendDBNotifi.append('invoice_id', 0);
            bodySendDBNotifi.append('user_id', response?.model?.id || 'null');
            bodySendDBNotifi.append('title', title);
            bodySendDBNotifi.append('content', content);
            bodySendDBNotifi.append('status', 0);
            bodySendDBNotifi.append('is_send', 1);
            bodySendDBNotifi.append('role_id', 1);
            bodySendDBNotifi.append(
              'avatar_notification',
              'https://gpihefp.com/wp-content/uploads/2021/05/male_boy_person_people_avatar_icon_159358.png'
            );
            await notificationAPI.registerPushNotification(
              bodySendDBNotifi,
              response?.token
            );
            showSuccess('Đăng kí tài khoản thành công !');
            dispatch(setLoading(false));
            setVerify({ ...verify, status: false });
            setData('');
            goToPage(Routers.LOGIN);
          }
        } catch (error) {
          dispatch(setLoading(false));
          setVerify({ ...verify, status: false });
          showError('Register is error');
        }
      })
      .catch((err) => {
        setVerify({ ...verify, status: false });
        showError(err);
      });
  }, [verify, data]);
  const renderFormRegister = useCallback(() => {
    return (
      <LayoutFormLogin>
        <Form formValue={data} model={validation} onSubmit={handleSubmit}>
          <Image source={IMAGES.LOGO_QH_PRIMARY} />
          <Title H1 primary bold>
            Đăng ký
          </Title>
          <WrapperRowInput>
            <Input
              label='Họ'
              name='first_name'
              placehoder='Nhập họ'
              value={withEmpty('first_name', data)}
              onChange={(value) => handleInput('first_name', value)}
              require
            />
            <Input
              label='Tên'
              name='last_name'
              placehoder='Nhập tên'
              value={withEmpty('last_name', data)}
              onChange={(value) => handleInput('last_name', value)}
              require
            />
          </WrapperRowInput>
          <Input
            label='Email'
            name='email'
            placehoder='Nhập email'
            value={withEmpty('email', data)}
            onChange={(value) => handleInput('email', value)}
            leftIcon={<Icon size={18} name='feather-mail' />}
            require
          />
          <Input
            label='Số điện thoại'
            name='phone_number'
            placehoder='Nhập số điện thoại'
            value={withEmpty('phone_number', data)}
            onChange={(value) => handleInput('phone_number', value)}
            leftIcon={<Icon size={18} name='feather-smartphone' />}
            type='number'
            require
          />
          <WrapperRowInput>
            <Input
              label='Mật khẩu'
              name='password'
              placehoder='Nhập mật khẩu'
              value={withEmpty('password', data)}
              type={showPassword ? 'text' : 'password'}
              onChange={(value) => handleInput('password', value)}
              leftIcon={<Icon size={18} name='feather-lock' />}
              rightIcon={{
                click: () => setShowPassword(!showPassword),
                icon: (
                  <Icon
                    size={18}
                    name={showPassword ? 'feather-eye' : 'feather-eye-off'}
                  />
                ),
              }}
              require
            />
            <Input
              label='Nhập lại Mật khẩu'
              name='passwordAgain'
              placehoder='Nhập lại Mật khẩu'
              value={withEmpty('passwordAgain', data)}
              onChange={(value) => handleInput('passwordAgain', value)}
              type={showPasswordCf ? 'text' : 'password'}
              leftIcon={<Icon size={18} name='feather-lock' />}
              rightIcon={{
                click: () => setShowPasswordCf(!showPasswordCf),
                icon: (
                  <Icon
                    size={18}
                    name={showPasswordCf ? 'feather-eye' : 'feather-eye-off'}
                  />
                ),
              }}
              require
            />
          </WrapperRowInput>
          <ButtonGroup>
            <Button onClick={() => goToPage(Routers.LOGIN)}>Huỷ</Button>
            <Button primary type='submit'>
              Đăng ký
            </Button>
          </ButtonGroup>
        </Form>
      </LayoutFormLogin>
    );
  }, [data, showPasswordCf, showPassword]);
  const modalVerify = useCallback(() => {
    const _body = () => {
      return (
        <BodyModal>
          <Image source={IMAGES.VERIRYPHONE} />
          <WrapperRowInput $center={true}>
            <Title H4 bold black>
              Chúng tôi sẽ gửi mã OTP về số điện thoại của bạn !
            </Title>
          </WrapperRowInput>
          <OTPInput
            shouldAutoFocus={true}
            value={withEmpty('otp', verify)}
            onChange={(value) => {
              setVerify({ ...verify, otp: value });
            }}
            numInputs={6}
            inputStyle={{
              width: '40px',
              height: '40px',
              borderRadius: '10px',
              border: '1.5px solid gray',
              margin: '0 5px',
              color: 'black',
            }}
          />
          <SendVerify
            circle
            icon={<Icon name='feather-arrow-right' size={24} />}
            disabled={withEmpty('otp', verify)?.length === 6 ? false : true}
            onClick={() => {
              handleVerifyAccount();
            }}
          />
        </BodyModal>
      );
    };
    return (
      <WrapperModal
        role='alertdialog'
        backdrop='static'
        size={'xs'}
        show={verify.status}
        body={_body()}
        onHide={() => {
          setVerify(false);
        }}
      />
    );
  }, [verify]);
  return (
    <>
      <div id='recaptcha-container'></div>
      {renderFormRegister()}
      {modalVerify()}
    </>
  );
};

export default Register;
