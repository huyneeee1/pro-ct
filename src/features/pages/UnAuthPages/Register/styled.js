import styled, { css } from "styled-components";
import {
  BaseForm,
  BaseItemGrid,
  BaseWrapper,
  BaseInputPicker,
  BaseIcon,
  BaseText,
  BaseButton,
  BaseTitle,
  BaseImage,
  BaseModal,
  BaseFlexContainer,
} from "atoms";
import { CustomInput } from "molecules";
import { FlexboxGrid, IconButton } from "rsuite";
import OtpInput from "react-otp-input";
export const Form = styled(BaseForm)`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 15px 0;
  width: 400px;
  .rs-form-group {
    width: 100%;
  }
`;
export const Input = styled(CustomInput)`
  width: 100%;
`;

export const WrapperRowInput = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  & > .rs-form-group {
    width: 48%;
  }
  ${(props) =>
    props.$center &&
    css`
      text-align: center;
      justify-content: center;
    `}
`;
export const Icon = styled(BaseIcon)``;

export const ButtonGroup = styled(BaseWrapper)`
  width: 100%;
  margin-top: 20px;
  justify-content: center;
`;
export const Button = styled(BaseButton)`
  width: 50%;
  &:nth-child(1) {
    margin-right: 10px;
  }
`;
export const Title = styled(BaseTitle)`
  margin: 10px 0;
`;
export const Image = styled(BaseImage)`
  width: 200px;
  height: 80px;
`;

export const WrapperModal = styled(BaseModal)``;
export const BodyModal = styled(BaseFlexContainer)`
  min-height: 300px;
  justify-content: space-evenly;
`;

export const SendVerify = styled(IconButton)`
  display: flex;
  background: ${(props) => props.theme.colors.primary};
  color: ${(props) => props.theme.colors.white};
`;
export const OTPInput = styled(OtpInput)``;
