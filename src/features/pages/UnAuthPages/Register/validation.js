import { SchemaModel, StringType } from "schema-typed";
const regexPassword = /^([a-zA-Z0-9])(?=.*[a-z])(?=.*\d)[a-zA-Z\d]{7,}$/;
export const validation = SchemaModel({
  first_name: StringType().isRequired("First name is required"),
  last_name: StringType().isRequired("Last name is required"),
  email: StringType()
    .isRequired("Email is required")
    .isEmail("Please enter the correct email"),
  phone_number: StringType()
    .isRequired("Phone is required")
    .addRule((value, data) => {
      return /^(84|0[3|5|7|8|9])+([0-9]{8})\b/.test(value);
    }, "Please enter the correct phone number"),
  password: StringType()
    .isRequired("Password is required")
    .pattern(
      regexPassword,
      "Password must be at least 8 character and combination of alphabets numbers and letters"
    ),
  passwordAgain: StringType()
    .isRequired("Confirm Password is required")
    .addRule((value, data) => {
      if (value != data.password) return false;
      return true;
    }, "Confirm password is not match password"),
});
