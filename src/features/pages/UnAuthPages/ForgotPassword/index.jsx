import React, { useState, useCallback } from 'react';
import { LayoutFormLogin } from 'organisms';
import { Form, Image, Title, Button, InputGroup, WrapperText } from './styled';
import { IMAGES } from 'assets';
import { BaseIcon, BaseText } from 'atoms';
import { validation } from './validation';
import { useHistory } from 'react-router-dom';
import { Routers } from 'utils';
import { withEmpty } from 'exp-value';
import { authAPI } from 'config/api';
import { useAlert } from 'hooks';
import { setLoading } from 'reduxFolder/loadingSlice';
import { useDispatch } from 'react-redux';
const ForgotPassword = () => {
  const [data, setData] = useState({
    email: '',
  });
  const { showError, showSuccess } = useAlert();
  const history = useHistory();
  const dispatch = useDispatch();
  const handleSubmit = useCallback(async () => {
    dispatch(setLoading(true));
    let _data = new FormData();
    _data.append('email', data.email);
    try {
      const response = await authAPI.forgotPasword(_data);
      if (response) {
        dispatch(setLoading(false));
        showSuccess('Forgot Password Success');
      }
    } catch (error) {
      dispatch(setLoading(false));
      showError('Forgot Password Failed');
    }
  }, [data]);
  const handleInput = useCallback(
    (name, value) => {
      setData((prev) => ({ ...prev, [name]: value }));
    },
    [data]
  );
  return (
    <LayoutFormLogin>
      <Form formValue={data} model={validation} onSubmit={handleSubmit}>
        <Image source={IMAGES.LOGO_QH_PRIMARY} />
        <WrapperText>
          <Title H1 primary bold>
            Forgot Password
          </Title>
          <BaseText>
            Please enter your registered email address we will get back to you
            with the reset password link
          </BaseText>
        </WrapperText>

        <InputGroup
          leftIcon={<BaseIcon size={18} name='feather-mail' />}
          name='email'
          placeholder='Email'
          value={withEmpty('email', data)}
          onChange={(value) => handleInput('email', value)}
        />
        <Button primary $mr={true} type='submit'>
          Send
        </Button>
        <Button
          secondary
          $mr={true}
          onClick={() => history.push(Routers.LOGIN)}
        >
          Go to login page
        </Button>
      </Form>
    </LayoutFormLogin>
  );
};

export default ForgotPassword;
