import { SchemaModel, StringType } from "schema-typed";
export const validation = SchemaModel({
  email: StringType()
    .isEmail("Please enter the correct email")
    .isRequired("Email is required"),
});
