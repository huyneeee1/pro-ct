import { SchemaModel, StringType } from "schema-typed";
const regexPassword = /^([a-zA-Z0-9])(?=.*[a-z])(?=.*\d)[a-zA-Z\d]{7,}$/;
export const validation = SchemaModel({
  password: StringType()
    .isRequired("New Password is required")
    .pattern(
      regexPassword,
      "New Password must be at least 8 character and combination of alphabets numbers and letters"
    ),
  passwordAgain: StringType()
    .isRequired("Confirm Password is required")
    .addRule((value, data) => {
      if (value != data.password) return false;
      return true;
    }, "Confirm password is not match password"),
});
