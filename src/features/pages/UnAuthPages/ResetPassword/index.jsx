import React, { useState, useCallback } from 'react';
import { LayoutFormLogin } from 'organisms';
import { Form, Image, Title, Button, InputGroup, WrapperText } from './styled';
import { IMAGES } from 'assets';
import { BaseIcon, BaseText } from 'atoms';
import { validation } from './validation';
import { useHistory, useLocation } from 'react-router-dom';
import { Routers } from 'utils';
import { withEmpty } from 'exp-value';
import { authAPI } from 'config/api';
import { useAlert } from 'hooks';
import { useDispatch } from 'react-redux';
import { setLoading } from 'reduxFolder/loadingSlice';
const ForgotPassword = () => {
  const [data, setData] = useState({
    password: '',
    passwordAgain: '',
  });
  const dispatch = useDispatch();
  const [showPassword, setShowPassword] = useState(false);
  const [showPasswordAgain, setShowPasswordAgain] = useState(false);
  const { search } = useLocation();
  const { showError, showSuccess } = useAlert();
  const history = useHistory();
  const handleSubmit = useCallback(async () => {
    dispatch(setLoading(true));
    const token = new URLSearchParams(search).get('token');
    const _data = { ...data, token };
    try {
      const response = await authAPI.resetPasword(_data);
      if (response) {
        showSuccess('Forgot Password Success');
        dispatch(setLoading(false));
      }
    } catch (error) {
      showError('Forgot Password Failed');
      dispatch(setLoading(false));
    }
  }, [data]);
  const handleInput = useCallback(
    (name, value) => {
      setData((prev) => ({ ...prev, [name]: value }));
    },
    [data]
  );
  return (
    <LayoutFormLogin>
      <Form formValue={data} model={validation} onSubmit={handleSubmit}>
        <Image source={IMAGES.LOGO_QH_PRIMARY} />
        <WrapperText>
          <Title H1 primary bold>
            Reset Password
          </Title>
          <BaseText>
            Please enter your registered email address we will get back to you
            with the reset password link
          </BaseText>
        </WrapperText>

        <InputGroup
          leftIcon={<BaseIcon size={18} name='feather-lock' />}
          name='password'
          type={showPassword ? 'text' : 'password'}
          placeholder='Password'
          value={withEmpty('password', data)}
          onChange={(value) => handleInput('password', value)}
          rightIcon={{
            click: () => setShowPassword(!showPassword),
            icon: (
              <BaseIcon
                size={18}
                name={showPassword ? 'feather-eye' : 'feather-eye-off'}
              />
            ),
          }}
        />
        <InputGroup
          leftIcon={<BaseIcon size={18} name='feather-lock' />}
          name='passwordAgain'
          type={showPasswordAgain ? 'text' : 'password'}
          placeholder='Confirm Password'
          value={withEmpty('passwordAgain', data)}
          onChange={(value) => handleInput('passwordAgain', value)}
          rightIcon={{
            click: () => setShowPasswordAgain(!showPasswordAgain),
            icon: (
              <BaseIcon
                size={18}
                name={showPasswordAgain ? 'feather-eye' : 'feather-eye-off'}
              />
            ),
          }}
        />
        <Button primary $mr={true} type='submit'>
          Send
        </Button>
        <Button
          secondary
          $mr={true}
          onClick={() => history.push(Routers.LOGIN)}
        >
          Go to login page
        </Button>
      </Form>
    </LayoutFormLogin>
  );
};

export default ForgotPassword;
