import styled, { css } from "styled-components";
import {
  BaseForm,
  BaseTitle,
  BaseImage,
  BaseButton,
  BaseWrapper,
  BaseLink,
  BaseFlexContainer,
} from "atoms";
import { CustomInput } from "molecules";
import { Divider, IconButton, Avatar, Checkbox } from "rsuite";
export const Form = styled(BaseForm)`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 15px 0;
  width: 400px;
  .rs-form-group {
    width: 100%;
  }
`;
export const InputGroup = styled(CustomInput)``;
export const Title = styled(BaseTitle)`
  text-align: left;
`;
export const Image = styled(BaseImage)`
  width: 200px;
  height: 80px;
`;
export const Button = styled(BaseButton)`
  ${(props) =>
    props.$mr &&
    css`
      margin: 5px;
    `}
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  ${(props) =>
    props.$blue &&
    css`
      background: ${(props) => props.theme.colors.tertiary};
      color: ${(props) => props.theme.colors.white};
    `}
  ${(props) =>
    props.$red &&
    css`
      background: ${(props) => props.theme.colors.error};
      color: ${(props) => props.theme.colors.white};
    `}
`;
export const WrapperText = styled.div`
  margin-bottom: 20px;
`;
