import { IMAGES } from "assets";
import { LayoutColumnPage } from "organisms";
import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { Text, Title } from "./styled";

const InvoiceStatus = () => {
  const { search } = useLocation();
  const [isSuccess, setIsSuccess] = useState(true);

  useEffect(() => {
    const arrUrl = search.split("?");
    const success = arrUrl[1];
    if (success === "success") {
      setIsSuccess(true);
      return;
    }
    setIsSuccess(false);
  }, [isSuccess]);
  return (
    <LayoutColumnPage urlImage={IMAGES.PAYMENT}>
      <Title primary>
        {isSuccess ? "Thanh toán thành công" : "Thanh toán thất bại"}
      </Title>
      <Text>
        {isSuccess
          ? "Chúc mừng bạn đã đặt vé thành công tại QuangHuyLemounsine. Quý khách vui lòng sắp xếp thời gian hợp lí để lên xe đúng giờ, tránh tình trạng xe dừng đợi khách quá lâu nhé!"
          : "Quý khách vui lòng liên hệ với hotline: 098999999 để được tư vấn và hỗ trợ quý khách đặt vé, để không làm gián đoạn lịch trình của quý khách!"}
      </Text>
    </LayoutColumnPage>
  );
};
export default React.memo(InvoiceStatus);
