import { IMAGES } from "assets";
import { LayoutColumnPage } from "organisms";
import PropTypes from "prop-types";
import React, { useCallback } from "react";
import { useHistory } from "react-router-dom";
import { Button, Text, Title } from "./styled";

const EmptyPage = ({ title, message, autoNavigate, buttonTitle }) => {
  const history = useHistory();
  const goNavigation = useCallback((route) => {
    history.push(route);
  });
  return (
    <LayoutColumnPage urlImage={IMAGES.ICON404}>
      <Title black>{title}</Title>
      <Text>{message}</Text>
      <Button primary={true} onClick={() => goNavigation(autoNavigate)}>
        {buttonTitle}
      </Button>
    </LayoutColumnPage>
  );
};
EmptyPage.propTypes = {
  message: PropTypes.string,
  title: PropTypes.string,
  buttonTitle: PropTypes.string,
  autoNavigate: PropTypes.string,
};
export default React.memo(EmptyPage);
