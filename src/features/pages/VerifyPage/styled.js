import { BaseButton, BaseText, BaseTitle } from "atoms";
import styled from "styled-components";

export const Title = styled(BaseTitle)`
  font-size: 2.5rem;
  font-weight: bold;
  @media (max-width: 768px) {
    font-size: 1.7rem;
  }
`;
export const Text = styled(BaseText)`
  margin: 20px 0;
`;
export const Button = styled(BaseButton)``;
