import { IMAGES } from "assets";
import { authAPI } from "config/api";
import { LayoutColumnPage } from "organisms";
import React, { useCallback, useEffect, useState } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { Routers } from "utils";
import { Button, Text, Title } from "./styled";

const VerifyPage = () => {
  const history = useHistory();
  const { search } = useLocation();
  const goNavigation = useCallback((route) => {
    history.push(route);
  });
  const [messageTitle, setMessageTitle] = useState({
    status: true,
    message: "Verify success",
  });

  useEffect(() => {
    const arrUrl = search.split("?");
    const id = new URLSearchParams(arrUrl[1]).get("id");
    const hash = new URLSearchParams(arrUrl[1]).get("hash");
    const token = localStorage.getItem("tokenVerify");
    async function verify() {
      try {
        const response = await authAPI.verification(id, hash, arrUrl[2], token);
        if (response) {
          setMessageTitle({
            status: true,
            message: "Verify success",
          });
          localStorage.removeItem("tokenVerify");
        }
      } catch (error) {
        setMessageTitle({
          status: false,
          message: "Verify failed",
        });
      }
    }
    verify();
  }, []);
  return (
    <LayoutColumnPage urlImage={IMAGES.VERIFY}>
      <Title black>{messageTitle.message}</Title>
      <Text>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta nam eaque
        laudantium inventore recusandae molestiae minus unde, mollitia voluptas
        aliquid!
      </Text>
      {messageTitle.status && (
        <Button primary={true} onClick={() => goNavigation(Routers.LOGIN)}>
          Login
        </Button>
      )}
    </LayoutColumnPage>
  );
};
export default React.memo(VerifyPage);
