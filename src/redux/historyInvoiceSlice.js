import { createSlice } from "@reduxjs/toolkit";
const historyInvoince = createSlice({
  name: "historyInvoince",
  initialState: null,
  reducers: {
    saveHistoryInvoince: (state, action) => {
      return (state = action.payload);
    },
    resetHistoryInvoince: (state, action) => {
      return (state = null);
    },
  },
});

const { reducer, actions } = historyInvoince;
export const { saveHistoryInvoince, resetHistoryInvoince } = actions;
export default reducer;
