import carRentalReducer from "pages/UnAuthPages/CarRental/carRentalSlice";
import userReducer from "./userSlice";
import loadingReducer from "./loadingSlice";
import departureReducer from "./departureSlice";
import notificationReducer from "./notificationSlice";
import historyInvoince from "./historyInvoiceSlice";
import themeSlice from "./themeSlice";
const rootReducer = {
  carRental: carRentalReducer,
  user: userReducer,
  loading: loadingReducer,
  departure: departureReducer,
  notification: notificationReducer,
  history: historyInvoince,
  theme: themeSlice,
};

export default rootReducer;
