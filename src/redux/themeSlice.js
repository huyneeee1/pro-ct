import { createSlice } from "@reduxjs/toolkit";
const theme = createSlice({
  name: "theme",
  initialState: true,
  reducers: {
    changeTheme: (state, action) => {
      return (state = action.payload);
    },
  },
});

const { reducer, actions } = theme;
export const { changeTheme } = actions;
export default reducer;
