import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { authAPI } from "config/api";

const userSlice = createSlice({
  name: "user",
  initialState: null,
  reducers: {
    saveUser: (state, action) => {
      return (state = action.payload);
    },
    logout: (state, action) => {
      const remember = JSON.parse(localStorage.getItem("remember"));
      const actionLogout = remember ? localStorage : sessionStorage;
      actionLogout.clear();
      localStorage.removeItem("remember");
      return (state = null);
    },
  },
});

const { reducer, actions } = userSlice;
export const { saveUser, logout } = actions;
export default reducer;
