import { createSlice } from "@reduxjs/toolkit";
const notification = createSlice({
  name: "notification",
  initialState: [],
  reducers: {
    saveNotificationRedux: (state, action) => {
      return (state = action.payload);
    },
    addNotification: (state, action) => {
      const findNoti = state.findIndex(ele => ele?.id === action.payload?.id);
      if (findNoti === -1) {
        return (state = [action.payload, ...state]);
      }
    },
    updateNotification: (state, action) => {
      const findIndex = state.findIndex((ele) => ele.id === action.payload.id);
      state.splice(findIndex, 1, action.payload);
    },
    loadMoreNotification: (state, action) => {
      return (state = [...state, ...action.payload]);
    },
    resetNotification: (state, action) => {
      return (state = []);
    },
  },
});

const { reducer, actions } = notification;
export const {
  saveNotificationRedux,
  addNotification,
  updateNotification,
  loadMoreNotification,
  resetNotification,
} = actions;
export default reducer;
