import React, { useEffect } from 'react';
import Routes from 'config/routes';
import { BrowserRouter } from 'react-router-dom';
import { Loading } from 'atoms';
import { useDispatch, useSelector } from 'react-redux';
import { ToastProvider } from 'react-toast-notifications';
import { useUser } from 'hooks';
import { saveUser } from './redux/userSlice';
import {
  saveNotificationRedux,
  addNotification,
} from './redux/notificationSlice';
import { saveHistoryInvoince } from './redux/historyInvoiceSlice';
import { notificationAPI, departureAPI } from 'config/api';
import io from 'socket.io-client';
const App = ({ changeTheme }) => {
  const isLoading = useSelector((state) => state.loading);
  const { user: userStorage } = useUser();
  const theme = useSelector((state) => state.theme);
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const socket = io.connect(process.env.WEBSOCKET);
  useEffect(() => {
    let app = document.querySelector('html');
    if (isLoading) {
      app.style.overflow = 'hidden';
      return;
    }
    app.style.overflow = 'auto';
  }, [isLoading]);
  useEffect(() => {
    dispatch(saveUser(userStorage));
  }, [userStorage]);

  useEffect(() => {
    async function execute() {
      try {
        const {
          data: { data: response },
        } = await notificationAPI.listNotification(user.id);
        dispatch(saveNotificationRedux(response));
        const {
          data: { data: responseHistory },
        } = await departureAPI.historyDeparture();
        const history = responseHistory?.filter((ele) => ele.status === 0);
        dispatch(saveHistoryInvoince(history.length ? history[0] : null));
      } catch (error) { }
    }
    setTimeout(() => {
      if (user) execute();
    }, 1000);
  }, [user]);
  useEffect(() => {
    changeTheme(theme);
  }, [theme]);
  useEffect(() => {
    socket.on('receive_message', (data) => {
      const { user_id, is_send } = data;
      if (user_id === user?.id && is_send === 1) {
        console.log('data', data)
        dispatch(addNotification(data));
      }
    });
  }, [socket]);
  return (
    <ToastProvider autoDismiss autoDismissTimeout={6000} placement='top-right'>
      <BrowserRouter>
        {isLoading ? <Loading /> : null}
        <Routes />
      </BrowserRouter>
    </ToastProvider>
  );
};

export default App;
