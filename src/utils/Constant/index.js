const Constant = {
  SERVICE: [
    {
      name: "Xe đi Hà Nội",
      icon: "feather-check-circle",
    },
    {
      name: "Thuê xe",
      icon: "feather-truck",
    },
    {
      name: "Đảm bảo có vé",
      icon: "feather-award",
    },
    {
      name: "Nhiều ưu đãi",
      icon: "feather-gift",
    },
    {
      name: "Đặt vé dễ dàng",
      icon: "feather-calendar",
    },
  ],
  OVERVIEW: [
    {
      title: "Đặt vé xe tại quanghuylemounsine.vn",
      content:
        "Website quanghuylemounsine.vn của chúng tôi đã phát triển hệ thống đặt vé online để hành khách có thể dễ dàng sử dụng trên tất cả các thiết bị có kết nối internet như máy tính, máy tính bảng, điện thoại di động... Tùy chỉnh trang web trên máy tính bảng và thiết bị di động giúp hành khách có thể đặt vé xe ở bất cứ nơi đâu và ở bất kỳ thời điểm nào mà họ mong muốn.",
    },
    {
      title: "Combo gia đình",
      content:
        "Combo Du Lịch trọn gói khuyến mãi từ  Sàn vé Xe đi  Sapa xuất phát từ các tỉnh thành phố như: Hải Phòng, Hà Nội Thái Bình, Hải Dương, Hưng Yên, Quang Ninh , Thái Nguyên, Tuyên Quang, Hà Giang, Hà Tĩnh, Vinh, Nghệ An, Thanh Hóa, Ninh Bình, Nam Định, Phủ Lý, Hà Nam khởi hành hàng ngày bằng xe giường nằm chất lượng cao.",
    },
    {
      title: "Các dịch vụ du lịch khác cung cấp bới quanghuylemounsine.vn",
      content:
        "Phù hợp với những chuyến đi đường dài. Nếu bạn ưa thích sự thoải mái, tiện lợi và chi phí rẻ hay đơn giản là đi cùng gia đình hoặc bạn bè, hãy lựa chọn dịch vụ này để gia tăng sự tiện lợi và tối ưu trải nghiệm chuyến đi. Bạn sẽ được bao hoàn toàn chuyến mà bạn đặt, tài xế có trách nhiệm đón bạn tận nơi đưa về tận điểm. (Bạn có thể sẽ phải trả thêm phí cầu đường nếu như bạn yêu cầu tài xế của bạn di chuyển bằng cao tốc hoặc tuyến đường của bạn buộc phải đi qua đường cao tốc.)",
    },
    {
      title: "Setup combo du lịch",
      content:
        "Tại Sapatrip.vn, khách hàng có thể tìm kiếm và lựa chọn cho mình những gói combo du lịch Sapa trọn gói được đỗi ngũ nhân sự của Sapatrip setup sẵn, với nhiều mức giá thỏa mãn nhu cầu của mình. Điểm cộng tại Sapatrip là khách hàng có thể tự tay Setup cho mình các gói Combo Sapa thỏa mãn nhu cầu, mục đích và kinh phí.",
    },
    {
      title: "Các dịch vụ du lịch khác cung cấp bới x.vn",
      content:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim ratione quibusdam reprehenderit, facere cupiditate minima accusantium officia saepe dignissimos pariatur delectus? Odit, a, numquam molestias vel aliquam necessitatibus sint inventore porro voluptatem sequi delectus quisquam assumenda",
    },
  ],
  DESCRIPTION: [
    {
      title: "Hình ảnh",
    },
    {
      title: "Tài xế",
    },
    {
      title: "Tiện ích",
    },
    {
      title: "Điểm đón trả",
    },
    {
      title: "Chính sách",
    },
    {
      title: "Đánh giá",
    },
  ],
  PROFILE: {
    OFFICE: "Khả Chính,xã Hợp Hưng, huyện Vụ Bản, tỉnh Nam Định",
    EMAIL: "quanghuylemounsine@gmail.com",
    PHONE: "0989999999",
    ADDRESS: "Khả Chính,xã Hợp Hưng, huyện Vụ Bản, tỉnh Nam Định",
    FACEBOOK: "facebook.com",
    INSTAGRAM: "instagram.com",
    YOUTUBE: "youtube.com",
  },
  NUMBER_9SEAT: [
    {
      label: "1 ghế",
      value: 1,
    },
    {
      label: "2 ghế",
      value: 2,
    },
    {
      label: "3 ghế",
      value: 3,
    },
    {
      label: "4 ghế",
      value: 4,
    },
    {
      label: "5 ghế",
      value: 5,
    },
    {
      label: "6 ghế",
      value: 6,
    },
    {
      label: "7 ghế",
      value: 7,
    },
    {
      label: "8 ghế",
      value: 8,
    },
    {
      label: "9 ghế",
      value: 9,
    },
  ],
  privateRouter: [
    {
      URL: "/dashboard",
      NAME: "Admin",
    },
    {
      URL: "/admin-car",
      NAME: "Quan li xe",
    },
  ],
  publicRouter: [
    {
      URL: "/",
      NAME: "",
    },
    {
      URL: "/dat-xe",
      NAME: "Đặt xe",
    },
    {
      URL: "/check-ve",
      NAME: "Check Ve",
    },
    {
      URL: "/lo-trinh",
      NAME: "Lộ trình",
    },
    {
      URL: "/gioi-thieu",
      NAME: "Giớ thiệu",
    },
    {
      URL: "/tin-tuc",
      NAME: "Tin tức",
    },
    {
      URL: "/lien-he",
      NAME: "Liên hệ",
    },
    {
      URL: "/login",
      NAME: "Login",
    },
    {
      URL: "/register",
      NAME: "Register",
    },
    {
      URL: "/forgot-password",
      NAME: "Forgot Password",
    },
    {
      URL: "/reset-password",
      NAME: "Reset Password",
    },
    {
      URL: "/profile",
      NAME: "Profile",
    },
    {
      URL: "/tin-tuc-chi-tiet",
      NAME: "Tin tức chi tiết",
    },
    {
      URL: "/verify",
      NAME: "Xác thực tài khoản",
    },
    {
      URL: "/payment",
      NAME: "PAYMENt",
    },
  ],
  destination: [
    {
      label: "Hà Nội",
      value: "Hà Nội",
    },
    {
      label: "Hải Phòng",
      value: "Hải Phòng",
    },
    {
      label: "Nam Định",
      value: "Nam Định",
    },
    {
      label: "Hưng Yên",
      value: "Hưng Yên",
    },
    {
      label: "Hoà Bình",
      value: "Hoà Bình",
    },
  ],
  pointOfDeparture: [
    {
      label: "Hà Nội",
      value: "Hà Nội",
    },
    {
      label: "Hải Phòng",
      value: "Hải Phòng",
    },
    {
      label: "Nam Định",
      value: "Nam Định",
    },
    {
      label: "Hưng Yên",
      value: "Hưng Yên",
    },
    {
      label: "Hoà Bình",
      value: "Hoà Bình",
    },
  ],
  policies: [
    {
      label: "Người mua chủ động trả/hủy vé",
      content:
        "Sau khi hoàn tất việc thanh toán tiền vé, vé đặt của bạn được xác nhận là Vé Có Hiệu Lực, nhưng bạn vẫn có thể hủy bỏ việc đặt vé đó. ",
    },
    {
      label: "Hủy vé do thông tin sai lệch trên website",
      content:
        "Trường hợp sản phẩm đã hết hàng hoặc giá sản phẩm không hiển thị chính xác trên website , tùy theo từng trường hợp nhà xe sẽ liên hệ để điều chỉnh hoặc thông báo hủy đơn hàng đó của người mua. Trường hợp đơn hàng đã được thanh toán và xác nhận thành công, người mua sẽ được hoàn lại 100% tiền vé.",
    },
    {
      label: "Nhà xe hủy chuyến ",
      content:
        "Trường hợp hãng xe hủy bỏ một chuyến xe hay dịch vụ, quanghuylemounsine.vn sẽ cố gắng thông báo cho Người mua sớm nhất và nỗ lực hỗ trợ Người mua tìm dịch vụ thay thế tương đồng nhất. Nếu Người mua không đồng ý sử dụng dịch vụ thay thế, Quang Huy Lemounsine sẽ hoàn lại 100% tiền vé cho Người mua.",
    },
    {
      label: "Người mua lỡ chuyến ",
      content:
        "Người mua phải có mặt trước Thời Gian Xuất Phát tối thiểu 30 phút để sắp xếp hành lý và chỗ ngồi. Quang Huy Lemounsine  và đối tác vận tải sẽ không chịu trách nhiệm nếu Người mua không thực hiện yêu cầu này. ",
    },
    {
      label: "Quy định về ghế ngồi, giường nằm và các tiện ích khác trên xe",
      content:
        " Nếu vé chưa bị hủy trước Thời Gian Khởi Hành, Quang Huy Lemounsine có nghĩa vụ đảm bảo đủ số lượng giường trống (ghế trống) trên xe tương ứng số Vé Có Hiệu Lực Người mua đã đặt. Trường hợp, xe không đủ số lượng giường trống (ghế trống) tương ứng số Vé Có Hiệu Lực và Người mua từ chối sử dụng dịch vụ, Quang Huy Lemounsine sẽ hoàn lại 150% tiền vé đã thanh toán của Người mua.",
    },
  ],
  TAGS: [
    "Ghê ngồi thoải mái",
    "Giá cả hợp lí",
    "Chuyến đi an toàn",
    "Tài xê thân thiện, nhiệt tình, kĩ năng chuyên môn cao ",
    "Nội thất sang trọng, điều kiện không gian xe sạch sẽ",
  ],
};

export default Constant;
