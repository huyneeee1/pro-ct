const Routers = {
  HOME: "/",
  LOGIN: "/login",
  REGISTER: "/register",
  PROFILE: "/profile",
  FORGOT: "/forgot-password",
  RESET: "/reset-password",
  NAV: [
    {
      URL: "/dat-xe",
      NAME: "Đặt xe",
    },
    {
      URL: "/check-ve",
      NAME: "Check vé",
    },
    {
      URL: "/lo-trinh",
      NAME: "Lộ trình",
    },
    {
      URL: "/gioi-thieu",
      NAME: "Giới thiệu",
    },
    {
      URL: "/tin-tuc",
      NAME: "Tin tức",
    },
    {
      URL: "/lien-he",
      NAME: "Liên hệ",
    },
  ],
  NEWS: {
    URL: "/tin-tuc",
    NAME: "Tin tức",
    CHILD: [
      {
        URL: "/tin-tuc-chi-tiet",
        NAME: "Tin tức chi tiết",
      },
    ],
  },
  VERIFYPAGE: {
    URL: "/verify",
    NAME: "Xác thực tài khoản",
  },
  CHECK_INVOICE: {
    URL: "/check-ve",
    NAME: "Check Invoince",
  },
  PAYMENT: {
    URL: "/payment",
    NAME: "PAYMENt",
  },
};

export default Routers;
