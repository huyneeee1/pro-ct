import ICON404 from "./404.svg";
import CARLOCATION from "./carlocation.svg";
import CONTACT from "./contact.png";
import DEFAULT_IMAGE from "./default.jpeg";
import ENDOW1 from "./endow1.svg";
import ENDOW2 from "./endow2.svg";
import ENDOW3 from "./endow3.svg";
import FACEBOOK from "./facebook.png";
import GIOITHIEU1 from "./gioithieu1.jpeg";
import GIOITHIEU2 from "./gioithieu2.jpeg";
import GOOGLE from "./google.png";
import INSTAGRAM from "./instagram.png";
import LEMOUNSINE from "./lemounsine.png";
import { default as LOGO, default as LOGOWHITE } from "./LOGO_QH.svg";
import LOGO_QH_PRIMARY from './LOGO_QH_PRIMARY.svg';
import PAYMENT from "./payment.png";
import SEARCHSVG from "./search.svg";
import SEARCHCAR from "./searchCar.svg";
import SLIDE1 from "./silde1.jpg";
import SLIDE2 from "./silde2.jpg";
import SLIDE3 from "./silde3.jpg";
import TELEGRAM from "./telegram.png";
import TRAVEL from "./travel.svg";
import VERIFY from "./verify.svg";
import VERIRYPHONE from "./verifyPhone.svg";
import WHATSAPP from "./whatsapp.png";
import XEDEMO from "./xedemo.jpeg";
import XELI from "./xeLimousine.jpg";

export default {
  DEFAULT: DEFAULT_IMAGE,
  LOGO: LOGO,
  SLIDE1: SLIDE1,
  SLIDE2: SLIDE2,
  SLIDE3: SLIDE3,
  FACEBOOK: FACEBOOK,
  GOOGLE: GOOGLE,
  ENDOW1: ENDOW1,
  ENDOW2: ENDOW2,
  ENDOW3: ENDOW3,
  VERIRYPHONE: VERIRYPHONE,
  ICON404: ICON404,
  CARLOCATION: CARLOCATION,
  VERIFY: VERIFY,
  GIOITHIEU1: GIOITHIEU1,
  GIOITHIEU2: GIOITHIEU2,
  LOGOWHITE: LOGOWHITE,
  INSTAGRAM: INSTAGRAM,
  TELEGRAM: TELEGRAM,
  WHATSAPP: WHATSAPP,
  SEARCHSVG: SEARCHSVG,
  XEDEMO: XEDEMO,
  SEARCHCAR: SEARCHCAR,
  PAYMENT: PAYMENT,
  LEMOUNSINE: LEMOUNSINE,
  TRAVEL: TRAVEL,
  CONTACT: CONTACT,
  XELI: XELI,
  LOGO_QH_PRIMARY: LOGO_QH_PRIMARY
};
