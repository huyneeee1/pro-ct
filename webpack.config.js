const HtmlPlugin = require("html-webpack-plugin");
const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const SvgStore = require("webpack-svgstore-plugin");
const webpack = require("webpack");
const Dotenv = require("dotenv-webpack");
module.exports = {
  entry: "./src/index.jsx",
  output: {
    publicPath: "/",
    path: path.resolve(__dirname, "build"),
    filename: "bundle.js",
  },
  resolve: {
    extensions: [".js", ".jsx", "*"],
    alias: {
      atoms: path.resolve(__dirname, "src/features/atoms"),
      molecules: path.resolve(__dirname, "src/features/molecules"),
      config: path.resolve(__dirname, "src/config"),
      routes: path.resolve(__dirname, "src/config/routes"),
      pages: path.resolve(__dirname, "src/features/pages"),
      templates: path.resolve(__dirname, "src/features/templates"),
      organisms: path.resolve(__dirname, "src/features/organisms"),
      assets: path.resolve(__dirname, "src/assets"),
      utils: path.resolve(__dirname, "src/utils"),
      reduxFolder: path.resolve(__dirname, "src/redux"),
      hooks: path.resolve(__dirname, "src/core/hooks"),
    },
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
          },
        ],
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
          },
        ],
      },
      {
        test: /\.(ttf|woff|woff2|eot|svg|png|jpg|jpeg|gif|ico)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "file-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: "svg-url-loader",
            options: {
              limit: 10000,
            },
          },
          {
            loader: "svg-sprite-loader",
            options: {
              symbolId: "icon-[name]",
            },
          },
        ],
        exclude: /assets/,
      },
    ],
  },

  plugins: [
    new HtmlPlugin({
      filename: "index.html",
      template: "./public/index.html",
    }),
    new MiniCssExtractPlugin(),
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      },
    }),
    new Dotenv({
      path: ".env",
    }),
    new SvgStore({
      svgoOptions: {
        plugins: [{ removeTitle: true }],
      },
    }),
  ],

  devServer: {
    historyApiFallback: true,
    port: 3000,
  },
}
